# Session 14 - Abschluss

> _Die begleitenden Slides gibt es hier: [slides/session14.html](https://tantemalkah.at/feministische-technikforschung/2024/slides/session14.html)_

## Stille Diskussion
> geplante Zeit: 20min

Zum Ankommen starten wir mit einer stillen Diskussion mit vier Plakaten und folgenden Themen/Fragen:

* Was hat mir in dieser LV gefehlt, was hätte ich noch erwartet?
* Welche der Inhalte/Methoden in dieser LV möchte ich auch noch mehr in anderen LVs sehen?
* Feministische Technikforschung ist ...
* Für mein weiteres Studium wünsche ich mir ...

## Soziogramm
> geplante Zeit: 20min

Mit kurzem Input davor zur Dunning-Kruger Kurve (siehe Slides). Die Fragen für das Soziogramm:

* Wo auf der Dunning-Kruger Kurve würdest du dich in Bezug auf feministische Technikforschung jetzt gerade gefühlt verorten.
* Wie viel (un)wohl(er) fühlst du dich nach dieser LV mit in-person-Interaktionen in der Gruppe?
* Wie hoch ist deine Motivation dich nun außerhalb des Kernstudiums für Technik für Alle Menschen einzusetzen?
* Überlegst du dir noch andere LVs aus dem Katalog des Zusatzzertifikats [Zusatzzertifikat Gender- und Diversitätskompetenz](https://www.tuwien.at/tu-wien/organisation/zentrale-bereiche/genderkompetenz/gender-in-der-lehre/zusatzzertifikat-gender-und-diversitaetskompetenz) zu besuchen?
* Gibt es noch Fragen aus der Gruppe die ihr der Gruppe stellen wollt?
* Wenn es ein Follow-Up Seminar gäbe zu dieser LV geben würde (Seminar := weniger Leute, mehr Lesen und Texte und Methoden besprechen),
  wie wahrscheinlich würdest du dieses auch besuchen wollen?
* Andere Fragen die ihr noch an die Gruppe habt und visualisieren wollt?

## Talking by Walking
> geplante Zeit: 20min

In 2er- oder 3er-Gruppen, je 7.5 bzw. 5 min pro Person. Leitfrage:

* Wie hat mir der Besuch dieser LV in meinem eigenen Entwicklungsprozess geholfen?

## Evaluation
> geplante Zeit: 10min

Zeit für das Ausfüllen der offizielle Evalutation / Kursbewertung im TISS:
[Link zur Evaluation](https://tiss.tuwien.ac.at/survey/surveyForm.xhtml?courseNumber=034016&semesterCode=2024S)

## Review & organisatorischer Abschluss
> geplante Zeit: 10min

Kurze Abklärung zum Stand der Projektgruppen, Hinweis auf die Gruppenzuordnung im TUWEL, Ausloten weiterer
Gruppenoptionen für alle, die noch keine Gruppe haben.

Kurze Review der LV, einige weitere Gedanken zu offenen Themen und ein Ausblick, was und wie ihr nach
der LV weitertun könntet.

Hier lohnt sich eventuell auch noch ein gesonderter Blick in die oben verlinkten Slides.

### Bonus Challenge 5

In dieser abschließenden Bonuschallenge geht es um Dissemination. Dabei gibt es Punkte für einen kurzen
veröffentlichten Bericht über diese LV (z.B. kurzer Blogbeitrag, Mastodon- oder Twitter-Thread,
Tiktok-Video, etc.). Das muss dann auch nicht sehr ausführlich sein, aber auch nicht einfach nur
ein Toot "ich war da und das war nice" oder so. Als Minimalkriterium gilt: es wird erwähnt worum
es in der LV geht, und wieso das relevant für euch und euer Studium selbst ist (also über "ich brauch
ECTS" hinausgehend). Positive side effect: das kann dann auch gleich als Basis für den Reflexionsbericht
oder auch das Dissemination-Concept in der Gruppenarbeit verwendet werden.

## Abschlussrunde
> geplante Zeit: 15min

Zum Ende gibt es noch eine Blitzlichtrunde: was nehme ich mit, und wie gehe ich jetzt aus dieser LV und in den Sommer?
