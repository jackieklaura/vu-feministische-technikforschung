# Session 4 - Feministische Technikforschung

> _Die begleitenden Slides gibt es hier: [slides/session4.html](https://tantemalkah.at/feministische-technikforschung/2024/slides/session4.html)_

## Aktiver Teilnahmebeitrag

Im TUWEL (Moodle) steht ein Forum mit folgender Aufgabe bereit:

Gedanken zu einem der vier Vorbereitungstexte. Bitte formuliert zumindest zu einem der vier Texte einen Gedanken den ihr hattet. Das kann entweder etwas sein, das euch bislang noch nicht so bewusst war, oder auch etwas was ihr eh schon wusstet, aber in der Form noch nicht gelesen habt. Das kann auch Querbezüge zu den anderen Texten haben, muss also nicht ausschließlich zu einem Text sein. Und ihr könnt gern auch noch zusätzliche Gedanken zu dem, was andere geschrieben haben fomulieren. Minimalkriterium ist wie immer eben zwei, drei Sätze, die ihr euch zu einem Text denkt.

Ich stelle im Forum zu jedem der Texte einen Ausgangspost, auf den ihr antworten könnt. Sollte es mit dem Forum und der Möglichkeit zu Antworten irgendwelche issues geben (bin selber grad erst noch am Ausprobieren der unterschiedlichen Forenmodi), schreibt mir bitte einfach gleich eine Nachricht. Ich versuch im Laufe des Dienstags meine Mailbox entsprechend im Auge zu behalten.

Die vier Themen:
- We don’t need ‘miracle’ technologies to fix the climate. We have the tools now.
- Klimakrise: Das überholte Mantra von einer politisch indifferenten Wissenschaft
- Can Snow-Clearing be Sexist?
- Gender Neutral With Urinals

## Einstiegs-Sequenz

Zum Start:
* 5min 2/3er Murmelgruppen
	* Wie lief die Vorbereitung?
	* Was ist eure Erwartung an die heutige Session?
	* Was hat mir nach der letzten Session am meisten gefehlt?

## Input-Sequenz

In den letzten beiden Einheiten haben wir uns mit der Wissenschafts- & Technikforschung, ihrer Genese und ihren groben Inhalten und Zugängen befasst. In dieser Session soll es um eine spezifischere (oder vielleicht auch nur nah verwandte) Forschungsrichtung gehen: die feministische Technikforschung.

Bevor wir aber in diesen Themenstrang einsteigen, möchte ich wieder ein Stück zurück zu Ludwik Fleck gehen, weil meines Erachtens hier viele gemeinsame Kristallisationspunkte sowohl der Wissenschaftsforschung als auch der Ausprägungen der feministischen Technikforschung vorweggenommen sind, noch lange bevor diese konkretere Formen angenommen haben. Und um hier auch noch einen weiteren Blick einzuflechten, möchte ich mit mit Wojciech Sady's englischsprachiger Beschreibung davon starten wie Fleck seine Denkkollektive und Denkstile gedacht hat:

> "If a group of people participate in a research program there occurs a peculiar process. Each member of the group reads different texts (both popular and professional), participates in different experiments, and belongs to more or less different thought collectives (both scientific and non-scientific). So, when they start speaking to each other and reading each other’s papers, a series of misunderstandings arises. Ideas circulate within a collective and are enriched by new associations, and therefore the words that are used change their meanings. Sometimes nothing is left of the original meanings intended by those participating in the exchange of ideas. Sometimes the changes stem from a clash of various proto-ideas. And that clash itself is a result of various social factors. Having conducted countless studies and conversations and embarked upon a long journey, the scholars finally create a thought style which nobody intended. And after this has happened, nobody _post factum_ knows when and how that style started to operate and who, specifically, created it." (Sady 2021)

Für die weiteren Betrachtungen ist dann vor allem auch spannend, wie sich Denkkollektive ändern und wie neue Gedanken in diesen entstehen können:

> "Before new sensual and thought forms arise, a certain “specific intellectual unrest must arise and a change of the moods of the thought collective” (1935b). If a person makes an important discovery but social moods do not change, she will not find people who take up her ideas and continue research." (ibid)

Und entscheidend dabei ist, dass es nicht den einen entscheidenden Faktor dafür gibt ob neue Gedanken und Ansätze aufgenommen oder verworfen werden:

>"In this process there is nothing necessary; various accidental circumstances decide which ideas become a basis for investigation and are disseminated within a collective; which meetings between scientists happen; which research projects acquire social support; which experiments are conducted first etc. [...] Summing up this idea Fleck claims that most—and maybe even the whole—content of scientific knowledge is conditioned by historical, psychological and sociological factors and one has to consider them when attempting to explain that content (1935a, II.1)." (ibid)

Vor allem für den wissenschaftlichen Bereich, dem sehr oft ein rationaler und möglichst emotionsfreier Zugang zugeschrieben wird, ist die kollektive Stimmung besonders relevant, die Fleck als das Substrat für das Florieren von Denkkollektiven versteht. Und diese kollektive Stimmung spiegelt sich auch in Stimmungen und Gefühlen der Individuen innerhalb des Denkkollektives wieder. Diese sind dabei nicht einfach wegzurationalisierende Komponenten der eigene wissenschaftlichen Praxis. Vielmehr gibt es einige mit der kollektiven Stimmung kongruente Gefühle, die dadurch in der Alltagserfahrung unsichtbar bzw. nicht mehr wahrgenommen werden:

> "Fleck stresses that there is no thinking which is free from emotions: “There is only agreement or difference between feelings, and the uniform agreement in the emotions of a society is, in its content, called freedom from emotions” (1935a, II.4)" (ibid)

Bzw. hier im deutschen Original:

> "Gefühlsfreies Denken kann nur ein solches bedeuten, das vom momentanen, persönlichen Stimmungszustande unabhängig ist, aber aus einer durch­schnittlichen Kollektivstimmung erfließt. Der Begriff eines überhaupt gefühlsfreien Denkens hat keinen Sinn. Es gibt keine Gefühlsfreiheit an sich oder reine Verstandesmäßig­keit an sich - wie wären sie nur festzustellen? Es gibt nur Gefühlsübereinstimmung oder Gefühlsdifferenz, und die gleichmäßige Gefühlsübereinstimmung einer Gesellschaft heißt in ihrem Bereiche Gefühlsfreiheit." (Fleck 1935, p. 67)

Während also in einem populären Verständnis von Wissenschaft und Technik nach wie vor Logik und Rationalität einen, wenn nicht den entscheidenden Faktor für wissenschaftliche Tätigkeit spielen, sagt Fleck nicht nur, dass neben verschiedene sozialen Faktoren auch die Emotionalität der Forschenden eine entscheidende Rolle spielt. Er sagt viel mehr auch, dass es für die Teilhabe an einem Denkkollektiv nur darauf ankommt, ob die eigene psycho-soziale Konfiguration mit der dominanten Konfiguration kongruent ist.

Es kommt also auch nicht nur darauf an wer Wissenschaft und Technik betreibt, sondern auch mit welcher Haltung Wissenschaft und Technik betrieben wird. Und dies sind zwei sehr zentrale Ansatzpunkte verschiedener Varianten feministischer Technikforschung.

Versuchen wir aber analog zur STS auch hier erst einmal eine allgemeines Verständnis des Begriffes zu fassen.

### Feminismus

Nachdem wir also schon versucht haben den Begriff Technikforschung zu umreißen, indem wir die Entwicklung der STS, also der Wissenschafts- und Technikforschung, nachgezeichnet haben, sollten wir vielleicht zuerst versuchen den Begriff Feminismus grob zu umreißen. Das ist jetzt leider keine leichte Aufgabe, die in den nächsten 5 Minuten einfach lösbar wäre. Daher orientiere ich mich an Sue V. Rosser's Beitrag im Gender and Science Reader, in dem sie unterschiedliche feministische Strömungen auf ihre methodologischen Auswirkungen in den Naturwissenschaft bespricht (Rosser 2001). Für eine kompakte, aber doch ausführlichere Beschreibung der Entwicklung feministischer Bewegungen und ihrer unterschiedlichen Zugänge möchte ich auf das erste Kapitel in Susanne Schröter's "FeMale. Über Grenzverläufe zwischen den Geschlechtern" (Schröter 2002 , pp. 15-59), das im Sinne des digitalen Handapparats auch im TUWEL zur Verfügung steht.

Auch wenn wir oft vom "Feminismus" sprechen, so sind in den meisten Fällen spezifische Feminismen gemeint, da es unter dem Dach des Feminismus in unterschiedlichen Punkten durchaus widersprüchlich Zugänge und Haltungen gibt. Gemein ist allen Richtungen aber wohl, wie auch Rosser schreibt, dass sie Geschlecht als wirkmächtige Kategorie menschlichen Handelns verstehen, die wir in der Untersuchung sozialer (und damit auch technosozialer) Phänomene nicht ignorieren können. Wie genau Geschlecht dabei allerdings verstanden wird kann sich wiederum in den Zugängen sehr unterscheiden:

> "all feminist theory posits gender as a significant characteristic that interacts with other characteristics, such as race and class, to structure relationships between individuals, within groups, and within society as a whole. However, using the lens of gender to view the world results in diverse images or theories" (Rosser 2001, p. 126)

Hier klingt auch bereits die Wichtigkeit einer intersektionalen Betrachtung an, die nicht Gender alleine - oder gar nur ein binäres Geschlecht - als isolierten Faktor betrachtet, sondern die eine Geschlechterkonfiguration immer in Verknüpfung mit anderen sozialen Dimensionen versteht, um monokausalen Erklärungsmustern vorzubeugen und auch nicht am Tellerrand der eigenen sozialen Verortung hängen zu bleiben (z.B.: männlich, weiß, cis, hetero, not-yet-disabled, ...).

In ihrem Beitrag zum Gender & Science Reader beschreibt Rosser die größeren feministischen Strömungen der letzten beiden Jahrhunderte und analysiert welche epistemologischen und methodologischen Ableitungen sich daraus für Wissenschaft und Technik ergeben - wobei ihre Arbeit spezifische auf den Bereich der Biologie fokussiert. Dabei bespricht sich vor allem folgende Feminismen:
* "Liberal feminism"
* "Marxist-feminism"
* "Socialist feminism" und "African-American feminism"
* "Essentialist feminism"
* "Existentialist feminism"
* "Psychoanalytic feminism"
* "Radical feminism"
* "Lesbian separatism"

Während es einerseits noch eine Vielzahl weiterer Feminismen gibt, und sich insbesondere seit der Jahrtausendwende auch weitere feministische Bewegungen und Strömungen ausgeprägt haben, können wir doch die meisten in der einen oder anderen Weise auch auf die oben genannten zurückführen oder mit diesen umschreiben. Bevor wir in unseren Diskursteil einsteigen, versuche ich Rosser's Beschreibung dieser Feminismen schnell zusammenzufassen.

#### Liberal feminism

Der _liberal feminism_, den wir im deutschen vielleicht noch eher mit "bürgerlicher Feminismus" als "Liberaler Feminismus" umschreiben können, nimmt seinen Ausgang in der bürgerlichen Frauenbewegung des 19. Jahrhunderts und ist vom klassischen Liberalismus geprägt, der gleiche Rechte für Alle einfordert und so ein bürgerliches Gesetz den davor bestehenden monarchistisch geprägten Herrschaftsformen entgegensetzte.

Nun haben sich hier über ca. 200 Jahre freilich auch unter diesem Dache viele unterschiedliche Ausprägungen entwickelt, von libertären Feminismen hin zum klassischen Gleichheitsfeminismus, oder wie Rosser schreibt:

> "[N]umerous complexities exist among definitions of liberal feminists today. However, a general definition of liberal feminism is the belief that women are suppressed in contemporary society because they suffer unjust discrimination [...]. Liberal feminists seek no special privileges for women and simply demand that everyone receive equal consideration without discrimination on the basis of sex." (ibid)

Auf einer oberflächlichen Ebene könnte mensch nun meinen, dass sich der liberal feminism auf Wissenschaft primär dahingehend auswirkt, dass die vielen - teils krassen und inzwischen sehr ausführliche dokumentierten - Barrieren für Frauen am Weg in die Wissenschaft und Diskriminierungen von Frauen in der Wissenschaft abgebaut werden, und dass sich so vielleicht leichte inhaltliche Verschiebungen darin ergeben was erforscht wird. Rosser sieht aber Ableitungen, die darüber hinaus gehen, und die damit zu tun haben, dass der liberal feminism zwei fundamentale Annahmen mit der klassischen scientific method teilt:

1. "Both assume that human beings are highly individualistic and obtain knowledge in a rational manner that may be separated from their social conditions" (ibid, p. 127)
2. "Both accept positivism as the theory of knowledge." (ibid)

Daraus leitet sich die Grundannahme ab, dass Wissen objektiv und wertfrei generiert werden kann. Die Objektivität der Wissenschaft ist demnach "contingent upon value neutrality or freedom from values, interests, and emotions associated with a particular class, race, or sex." (ibid)

Jetzt wissen wir nicht nur aus vielen feministischen Auseinandersetzungen, sondern bereits seit Ludwik Fleck und heute von der STS als Forschungsfeld, dass es individuellen Wissenschafter:innen nicht vollständig möglich ist neutral oder wert-frei zu sein, auch wenn das eine wissenschaftliche Norm (vgl. z.B. Merton aus der letzten Einheit) ist, der die einzelnen Wissenschafter:innen auch versuchen möglichst nachzukommen.

Rosser geht an dieser Stelle noch ausführlicher auf Beispiele von "flawed research" in der Verhaltensbiologie, der Primatologie und den Neurosciences ein. Zusammenfassend stellt sie fest, dass liberal feminists grundsätzlich davon ausgehen, dass eine "good science", die auch "gender-free" ist, möglich ist indem nach und nach bias und unzureichende Forschungsansätze hinterfragt und aufgedeckt und so korrektive Maßnahmen gesetzt werden, die hier der subjektiven Verortung einzelner Wissenschafter:innen entgegenwirken.

So hat der Ansatz des "liberal feminism" folgende zwei signifikante Implikationen für Wissenschaft:
1. "It does not question the integrity of the scientific method itself or of its supporting corollaries of objectivity and value neutrality. Liberal feminism reaffirms the idea that it is possible to find a perspective from which to observe that is truly impartial, rational, and detached. Lack of objectivity and presence of bias occur because of human failure to properly follow the scientific method and avoid bias due to situation or condition." (Rosser 2001, p. 129)
2. "Liberal feminism also implies that good scientific research is not conducted differently by men and women and that in principle men can be just as good feminists as women." (ibid)

Im Gegensatz dazu, stellen alle anderen nachfolgend vorgestellten feministischen Ansätze diese Vorannahmen grundlegend in Frage.

#### Marxist-feminism

Ausgehend von einer marxistischen Kritik an Wissenschaft, die jegliches Wissen als sozial konstruiert sieht, weil durch die konkrete menschliche Erfahrung in kollektiven bzw. kooperativen Produktionsprozessen geformt, steht auch der marxistische Feminismus in starkem Kontrast zum liberal feminism. "Marxism proposes that the form of knowledge is determined by the prevailing mode of production" (ibid, p. 130) Das generierte Wissen entspricht demnach in der Regel den Interessen der herrschenden Klasse. Als Beispiel wird hier z.b. der Sozialdarwinismus angeführt, der sich mit Bezug auf die Arbeiten Darwins in der viktorianischen englischen Gesellschaft besonders gut etablieren konnte, weil sie so ihre Normen als biologisch fundiert deuten konnten. Auch heute kann demnach die permanente Suche nach Hormonen oder Genen die z.B. Homosexualität determinieren sollen oder nach Geschlechterunterschieden in der Hirnforschung als Ausdruck einer patriarchalen Gesellschaft gedeutet werden, die so ihre soziale Norm als natürlich gegeben begründen möchte. Insbesondere auch weil:

> "One can imagine that a society free from inequality between the sexes and lacking homophobia would not view sex differences and sexual preferences differences research as valid scientific endeavours. The fact that our society supports such research indicates the extent to which the values of the society and the scientists influence scientific inequity and 'objectivity'." (ibid, p. 131)

Marxistischer Feminismus stellt gängige Dichotomien wie Natur/Kultur und Subjekt/Objekt zur Kategorisierung von Wissen und Wirklichkeit in Frage, und gibt zu bedenken, dass der "neutrale" bzw. "objektive" Standpunkt einer:s wissenschaftlichen Beobachter:in, der sich in der scientific method aus der Distanzierung vom Forschungsobjekt ergibt, nur dann als wissenschaftlicher als andere Ansätze erscheint, wenn wir die soziale Konstruktion von Wissen ignorieren.

#### Socialist feminism & African-American feminism

In vielen Punkten sind der "socialist feminism" und "African-American feminism" wie sie Sue V. Rosser beschreibt dem marxistischen Feminismus sehr nahe oder überlappend. Allerdings entwickelten sich in den ersteren intersektionalere Ansätze, die vor allem die Kategorien sex/gender und race als mindestes gleich relevant wie class sehen. Für diese Strömungen ist also nicht allein der "prevailing mode of production" ausschlaggebend, und damit Klasse auch nicht als die dominante Kategorie für Erklärung von Unterdrückungsmechanismen heranzuziehen, wie das aber in einem streng ausgelegten marxistischen Feminismus der Fall ist.

Hier werden unter anderem marxistisch Standpunkttheorien erweitert und so der Weg für feminist standpoint epistemologies geebnet, auf die wir in der nächsten Einheit noch genauer zu sprechen kommen werden. Zentral dabei ist, dass es eben nicht den "neutral, disinterested observer of liberal feminism" (ibid, p. 132) gibt, sondern aufgrund unterschiedlicher Herrschafts- und Unterdrückungsmechanismen einzelne Subjekte aufgrund ihrer sozialen Verortung unterschiedliche Standpunkte einnehmen, von denen aus unterschiedliche Erkenntnisse möglich sind:

> "Classical Marxist and African-American critiques suggest that individuals oppressed by class (Marxist) and/or race (African-American) have an advantageous ad more comprehensive view of reality. Because of their oppression, they have an interest in perceiving problems with the status quo and the science and knowledge produced by the dominant class and race. Simultaneously, their position requires them to understand the science and condition of the dominant group in order to survive." (ibid)

#### Essentialist feminism

Während die bisherigen Feminismen keinen grundsätzlichen (für die Wissensproduktion signifikanten) Unterschied zwischen den Geschlechtern ausmachen (das heißt üblicherweise: zwischen "Männern" und "Frauen"), und deren unterschiedliche Erkenntnis-Potentiale mehr auf die soziale Verortung zurückführen, so geht der "Essentialist feminism" davon aus

> "that women are different from men because of their biology, specifically their secondary sex characteristics and their reproductive systems. Frequently, essentialist feminism may extend to include gender differences in visuospatial and verbal ability, aggression and other behavior, and other physical and mental traits based on prenatal or pubertal hormone exposure." (ibid, p. 133)

Im 19. Jahrhundert haben so u.a. die Suffragetten biologischen Essentialismus in ihrer Argumentation für das Wahlrecht von Frauen verwendet, da diese zwar aufgrund ihrer Biologie physiologisch Männern unterlegen wären, aber in anderen Bereichen, z.B. moralisch Männern überlegen wären. In der zweiten Hälfte des 20. Jahrhunderts wurde ein gewisser sex/gender essentialism aus unterschiedlichen Richtungen (konservativ bis radikal) aufgegriffen "with a recognition that biologically based differences between the sexes might imply superiority and power for women in some arenas." (ibid)

Aufgrund dieser biologischen Unterschiede würden Frauen im frame des essentialst feminism auch entsprechend andere Wissenschaft machen als Männer. Nicht nur weil sie auf andere Probleme fokussieren, sondern weil sie auch andere Methoden etablieren würden.

Während essentialist feminists sich auch gegen den "neutral observer" der scientific method und des liberal feminism stellen, haben sie laut Rosser ein teilweise widersprüchliches Verhältnis wenn es um die soziale Konstruktion von Wissen geht. Gerade weil sie den unterschiedlichen Geschlechtern (und hier wird in der Regel von einem binären Verhältnis ausgegangen) aufgrund ihrer biologischen Voraussetzungen unterschiedliche Fähigkeiten und Möglichkeiten im wissenschaftlichen Erkenntnisprozess zusprechen, ist die soziale Verortung für die Konstruktion des Wissens nicht mehr signifikant.

#### Existentialist feminism

Ganz kurz zusammengefasst beruht der existenzielle Feminismus auf Simone de Beauvoir's Konzept der "otherness" die in unserer Gesellschaft den Frauen die Rolle der Anderen zuschreibt (im Gegensatz zu den Männern als der unmarkierten Norm). Nach Beauvoir geht es nicht um die biologischen Unterschiede per se, sondern ihrer Interpretation und der gesellschaftlichen Bewertung dieser Unterschiede. Daraus folgt für die Wissenschaft:

> "The methodological implications which flow from existentialist feminism are that a society which emphasized gender differences would produce a science which emphasized sex differences. In such a society, men and women might be expected to create very different sciences because of the social construction of both gender and science." (ibid, p. 134)

Während wir uns in dieser Konzeption also eine Gesellschaft (und damit eine Wissenschaft) ohne Gender-Differenzen vorstellen können, geht unter den gegebenen Voraussetzungen die Wahrscheinlichkeit zu einer gender-neutralen (positivistischen) Wissenschaft zu gelangen quasi gegen Null.

#### Psychoanalytic feminism

Ähnlich gelagert wie der existentielle Feminismus wird im psychoanalytischen Feminismus keine Essentialisierung biologischer Faktoren vorgenommen, allerdings die gesellschaftlich bedingte Entwicklung in den Vordergrund gestellt. Basierend auf Sigmund Freud's Theorien steht dabei die sexuelle Entwicklung der Individuen stark im Fokus, die sich wiederum durch unterschiedliche gender-basierte Interaktionen bereits im Kindesalter ergibt. Vor allem in den 1960ern und -70ern wurden die Grundsteine für eine feministische Neuinterpretation der Psychoanalyse vorgenommen, und ganz stark heruntergebrochen ist ein zentraler Fokus die Verteilung von Rollen in der Care-Arbeit, insbesondere der Verteilung der Rollen der primären Bezugspersonen im Kindesalter.

> "Psychoanalytic feminism does not necessarily imply that biological males will take a masculine approach and that biological females will take a feminine approach. In most cases, due to the resolution of the Oedipal complex with a female caretaker, this will be the case." This is seen as a reason "why so mane more males then females are attracted to science. However, many of the biological females who are currently scientists probably take a more masculine approach, due not only to resolution of psychosexual development phases but also to training by male scientists. Psychoanalytic feminism also suggests that biological males could take a 'feminine' approach." (ibid, p. 136)

Eine Hypothese des psychoanalytischen Feminismus ist, dass sich bei größerer (und ausgeglichener) Involvierung von Männern in Care-Arbeit auch weniger polarisierte Gender-Rollen etablieren, und so durch ein breiteres Angebot and Identitätskonstruktionen auch diversere Gender-Konstruktionen in der Wissenschaften und unter Wissenschafter:innen ergeben, was der Möglichkeit eine "gender-free science" den Weg ebnen könnte.

#### Radical feminism

Eine Ausgangsbasis des radikalen Feminismus ist die Annahme, dass die Unterdrückung von Frauen "the deepest, most widespread, and historically first oppression" (ibid, p. 137) ist, und es daher bislang nur wenige Gelegenheiten gab genuin feministische Theorien zu entwickeln, die sich nicht auf bereits von Männern etablierte, patriarchal durchzogene Theorien aufbauen, wie das bisher alle anderen genannten Feminismen zum Teil tun. Aus diesem Grund werden auch gängige Epistemologien und daher fast alle wissenschaftlichen Methodologien als unzureichend abgelehnt. Eine Verbindung zu Standpunkt-basierten Feminismen gibt es jedoch:

> "Radical feminism posits that it is women, not men, who can be the knowers. Because women have been oppressed, they know more then men. They must see the world from the male perspective in order to survive, but their double vision from their experience as an oppressed group allows them to see more then men." (ibid)

Allerdings gehen die methodologischen Ansätze hier stark auseinander. Eine der primären Strategien zur Vergemeinschaftung von Wissen und Erfahrung sind im radikalen Feminismus "consciousness raising groups", in denen ausgehend von der persönlichen Erfahrung von Frauen in nicht-hierarchischen Gruppen ausgelotet wird was als Wissen frei von patriarchaler Ideologie angesehen werden kann.

Aufgrund fehlender zentraler Theorien als Framing gehen die methodischen Ansätze und Annahmen wie überhaupt gewusst werden kann auch innerhalb des radikalen Feminismus teils stark auseinander.

#### Lesbian separatism

Der lesbische Separatismus wird oft als spezielle Form eines radikalen Feminismus gedeutet. Während für uns diese Zu-/Einordnung hier nicht so entscheidend ist, ist eine Ergänzung eines radikalfeministischen Zugangs durch lesbischen Separatismus bedeutend:

> "Lesbian separatists suggest that daily interaction with the patriarchal world and compulsory heterosexuality make it impossible for women to completely understand their oppression and its distortion of their experience of reality. Therefore, in order to connect with other women and nature to understand reality, women must separate themselves from men." (ibid, p. 138)

Mit dieser Separierung sind in dem Fall keine temporären safer spaces gemeint, wie wir sie auch aktuell aus unterschiedlichen feministischen und emanzipatorischen Aktionsformen kennen, sondern über einen längeren Zeitraum hinweg, der überhaupt erst eine andere Wahrnehmung von Alltag und Realität ermöglicht.

Daraus abgeleitet würden Wissenschaft und Technik, sofern die gegebenen Unterdrückungen überwunden werden können, signifikant anders ausschauen, wobei es keine klare Vorstellung davon gibt wie. Insbesondere in feministischer Science Fiction könnten hier aber Vorstellungen zu finden sein, die mögliche Varianten ganz anderer wissenschaftlicher und technischer Entwicklung skizzieren.


Zusammenfassend hält Sue V. Rosser nach diesem Überblick fest, dass es nicht sinnvoll ist von einer feministischen Methodologie per se zu sprechen, sondern dass feministische Methodologien meist eine (nicht immer widerspruchsfreie) Sammlung an Ansätzen und Interventionen sind, die verwendet werden können, um Wissenschaft und Technik selbst anders zu betreiben. Aus unserer Perspektive bedeutet das, dass wir uns auch entsprechende methodologische Praktiken aneignen können um Wissenschaft und Technik anders zu untersuchen.


## Diskurs-Sequenz

* 3min individuell (und zusätzlich mit slido open text poll unterstüzt):
	* wozu ist der feministische Blick in der Technikforschung gut, wenn STS ohnehin bereits so stark die soziale Konstruktion von Wissenschaft und Technik im Blick hat?
	* welche Parallelen gibt es hier zu den gelesenen Zeitungsartikeln?
	* in welchem der vorgestellten Feminismen würdet ihr das bisher Gelesene aus "Invisible Women" verorten? Welche Schlussfolgerung für euer eigenes Feld würdet ihr daraus ziehen?
* 5min Murmelgruppen (2er oder 3er Grüppchen):
	* Habt ihr ähnliche Gedanken zu dem Thema?
	* Sind euch ähnliche Dinge eh ganz klar oder schwer nachvollziehbar?
	* Woran meint ihr liegt das, dass ihr gleiche oder unterschiedliche Erfahrungen dazu habt?
* 20min bzw. open end: Plenumsdiskussion offener Fragen & Punkte


## Vorbereitung auf Session 5

- Lesen der Input-Sequenz von [](./session5.md) (~25min)
- Eines der folgenden beiden Papers:
  - Bauchspies, Wenda K, and María Puig de la Bellacasa. 2009. “Feminist Science and Technology Studies: A Patchwork of Moving Subjectivities. An Interview with Geoffrey Bowker, Sandra Harding, Anne Marie Mol, Susan Leigh Star and Banu Subramaniam.” Subjectivity 28 (1): 334–44. https://doi.org/10.1057/sub.2009.21. (~25min)
  - Åsberg, Cecilia, and Nina Lykke. 2010. “Feminist Technoscience Studies.” European Journal of Women’s Studies 17 (4): 299–305. https://doi.org/10.1177/1350506810377692. (~20min)
- Details im TUWEL-Kurs

## Checkout
Slido:
* Word cloud: Begriffe die mir heute im Input oder der Diskussion besonders im Kopf geblieben sind.
* Rating poll: Wie sehr hat sich mein Bild auf "Feminismus" durch die heutige Session geändert?
* Rating poll: Wie sehr ist ein feministischer Blick für mein eigenes wissenschaftliches/technisches Arbeiten relevant?
* Wordcloud: Mit welchem Gefühl gehe ich nun aus der LV und in den Abend?


## Referenzen:

Rosser, Sue V. 2001. “Are There Feminist Methodologies Appropriate for the Natural Sciences and Do They Make a Difference?” In The Gender and Science Reader, edited by Muriel Lederman and Ingrid Bartsch, 123–44. Routledge.

Sady, Wojciech. 2021. “Ludwik Fleck.” In The Stanford Encyclopedia of Philosophy, edited by Edward N. Zalta, Winter 2021. Metaphysics Research Lab, Stanford University. https://plato.stanford.edu/archives/win2021/entries/fleck/.

Schröter, Susanne. 2002. FeMale. Über Grenzverläufe Zwischen Den Geschlechtern. Frankfurt: Fischer Taschenbuch Verlag.
