# Session 2 - Wissenschafts- & Technikforschung, Part 1

> _Die begleitenden Slides gibt es hier: [slides/session2.html](https://tantemalkah.at/feministische-technikforschung/2024/slides/session2.html)_

## Aktiver Teilnahmebeitrag

Im TUWEL (Moodle) steht ein Forum zur Diskussion folgender Fragen bereit:

> Was stellt ihr euch zum aktuellen Zeitpunkt (womöglich noch ganz ohne
> weiteres Hintergrundwissen zu haben) unter den Begriffen Wissenschaftsforschung
> und Technikforschung vor? Und was könnten die zur Vorbereitung gelesenen
> Texte damit zu tun haben?

Beiträge können über den gesamten Tag, an dem die Session stattfindet,
eingebracht werden und zählen im Sinne der Punkte für die Note als aktive
Teilnahme für diese Session. Das können entweder initiale Beiträge oder
auch Antworten auf breits eingebrachte Beiträge sein. Dabei muss ein initialer
Beitrag nicht extra recherchiert und besonders "wissenschaftlich" ausgeführt
werden. Es geht um eine Momentaufnahme der eigenen Gedanken, die für die
Kolleg:innen nachvollziehbar sein sollen (ein paar Minuten über die Frage
nachdenken und ein, zwei Sätze dazu formulieren).

> Wichtig: es geht hier nicht darum, eine "richtige" Antwort zu finden,
> sondern eine niederschwellige Diskussion in Gang zu bringen. Und dazu
> nutzen wir einen digitalen Seitenkanal, weil sich das im Hörsaal in
> der Skalierung sonst nicht ganz so ausführlich ausgeht.

## Einstiegs-Sequenz

Zum Start:
* 5min 2/3er Murmelgruppen
	* Wie lief die Vorbereitung?
	* Was ist eure Erwartung an die heutige Session?
	* Was wisst ihr eigentlich schon über Wissenschafts- & Technikforschung?

Slido rating polls: wie viel haben folgende Eigenschaften/Fähigkeiten mit Wissenschaft zu tun?
* Intuition
* Logik und rationales Denken
* Empathie
* Genialität
* sozialer Zusammenhalt

## Input-Sequenz

Was ist eigentlich Technikforschung? Bzw. was ist Wissenschafts- & Technikforschung? Das ist eine nicht so leicht, und auf keinen Fall abschließend beantwortbare Frage, zumal das Feld als wissenschaftliche Disziplin eigentlich noch sehr jung ist, je nach Zeitrechnung sogar jünger als die Informatik - als gewissermaßen jüngste Technowissenschaft hier an der TU Wien. Wir wollen uns dem Feld in dieser und der folgenden Einheit also aus unterschiedlichen Richtungen nähern. Zuerst ganz allgemein, dann historisch, und dann anhand bestimmter Konzepte und Begriffe.

### Allgemeiner Zugang

Was kann also Wissenschafts- und Technikforschung bedeuten? Ganz alltagssprachlich wird sie manchmal auch einfach als Forschung in Wissenschaft und Technik verstanden. Also so in etwa wie bei Informatiker:innen oft beliebt: ich mach irgendwas mit Computer. Nur noch breiter: ich mach irgendwas mit Wissenschaft und Technik. Und in einem ganz gewissen Sinn stimmt das sogar auch. Aber besser wir nähern uns dem Begriff vom Ende her: der Forschung. Also ich betreibe Forschung. Und was forsche ich? Also ich forsche an Wissenschaft und Technik. Erforsche sie also. Ich erforsche also Wissenschaft und Technik. Und das ist nicht damit zu verwechseln, dass wir Wissenschaft und Technik betreiben. Ja, wir betreiben eine Wissenschaft, aber eben jene, die Wissenschaft und Technik erforscht.

Wie können nun Wissenschaft und Technik erforscht werden? Da gibt es unzählige Ansätze dazu, von denen wir später noch einige kennen lernen werden. Aber ohne gleich zu spezifisch zu werden können wir Wissenschaft und Technik - oder vielleicht einfacher noch: einzelne Wissenschafts- und Technikdisziplinen als (abstrakte ebenso wie physische) Orte verstehen, an denen Menschen mit anderen Menschen und Dingen interagieren um:
* neue Theorien zu erarbeiten,
* oder auch bestehende Theorien durch Experimente bestätigen,
* oder auch aufgrund bestehender Theorien oder empirischer Ergebnisse konkrete technologische Artefakte (Geräte, Tools, Applikationen, Infrastruktur, etc.) zu entwickeln,
* oder sich Gedanken dazu zu machen wie Technologie überhaupt entwickelt werden kann (hier gibt es dann auch Überschneidungen zur Technikforschung selbst),
* oder sonst halt irgendwas mit Wissenschaft und Technik tun - wir wissen ja noch gar nicht was dort alles passiert und wie es passiert, das wollen wir ja mit der Wissenschafts- und Technikforschung erst genauer erforschen

Um nun herauszufinden was davon und wie das alles passiert, können wir unter anderem folgende Strategien, Zugänge und Methoden anwenden:
* Eingrenzung des zu beforschenden Feldes auf eine (Teil-)Disziplin, eine bestimmte Einrichtung oder Organisation, z.B. eine Universität oder nur ein Institut, eine außeruniversitäre Forschungseinrichtung, oder auch eine bestimmte Firma oder einen Technologiekonzern)
* Eingrenzung auf ein bestimmtes Artefakt, z.B. ein Produkt, oder auch auf eine bestimmte Theorie oder Methode in einem Feld
* Ausgehend von einzelnen Akteur:innen oder einzelnen Artefakten können wir versuchen alle anderen Akteur:innen und Artefakte zu finden, mit denen erstere direkt oder indirekt in Beziehung steht und interagiert, um Wissenschaft/Technik zu betreiben.
* Wir können versuchen alle relevanten Publikationen zu finden und zu analysieren
* Beobachtung und Analyse von einzelnen Forschungs- und Entwicklungsprozessen
* Befragung (qualitativ oder quantativ, persönlich oder durch Umfrage-Tools) von Akteur:innen im zu beforschenden Feld
* Zusammenführen anderer Forschungsarbeiten im eigenen Wissenschafts- und Technikforschungsfeld und basierend darauf Entwicklung neuer Theorien und Methoden für die Erforschung von Wissenschaft und Technik

Das sind nun nur einige Möglichkeiten wie wir uns unserem Forschungsfeld nähern können. Alles was wir dabei tun können - und sollten - wir dabei auch beschreiben und transparent machen, da es sich dabei um unseren eigenen Forschungsprozess handelt, der wiederum von anderen Forscher:innen kritisch analysiert und im Zweifelsfall auch hinterfragt werden können soll. Letztendlich betreiben wir hier ja selbst Wissenschaft, und auch das können wir (und vor allem auch andere) aus einer Wissenschafts- und Technikforschungsperspektive heraus betrachten und Aussagen darüber machen.

Die generierten Erkenntnisse aus unserem Forschungsprozess können so als Reflexion oft auch für das beforschte Feld produktiv genutzt werden, um Prozesse zu verbessern. Genauso können diese Erkenntisse aber auch Widerstände im beforschten Feld hervorrufen, weil dadurch dominante Pradigmen in Frage gestellt werden könnten und etablierte Forschungsprozesse und -strukturen überdacht werden müssten.

Insofern könnten wir nun auch darüber nachdenken, dass es ja für Wissenschafter:innen und Techniker:innen selbst interessant sein könnte sich auch der Wissenschafts- und Technikforschung zuzuwenden, um für das eigene Feld neue Erkenntnisse und Möglichkeiten zu generieren. Und das passiert auch je nach Kontext und Phase eines Feldes mal mehr und weniger. Allerdings stellen sich hier meistens sehr schnell auch Ressourcenprobleme ein. Wir alle haben nur begrenzte zeitliche und materielle Ressourcen zur Verfügung. Und je spezialisierter ein Forschungsbereich ist, desto höher ist die Wahrscheinlichkeit, dass nur wenige Ressourcen zur Verfügung stehen um diese Art von Meta-Forschung zu betreiben. Bzw. wird die Bereitschaft Ressourcen dafür zu verwenden eher gering sein.  Auch das ist wiederum ein wichtiger Aspekt von Wissenschafts- und Technikforschung: der Blick darauf wie Ressourcen in einem Feld verwendet werden, wie entschieden wird diese Ressourcen genau so zu verwenden, und wie sich das darauf auswirkt welche Art von Wissenschaft und Technik entwickelt werden kann.

Wir haben hier nun eine erste Annäherung an die Frage versucht: was genau ist eigentlich Wissenschafts- und Technikforschung. Bevor wir uns nun historisch anschauen wie sich diese entwickelt hat, ist eine Abgrenzung am Ende noch wichtig: jene zur Wissenschaftstheorie, Wissenschaftsphilosophie sowie zur Technikphilosophie. Wie immer ist es da auch nicht ganz so einfach, und es gibt Möglichkeiten in einer Schnittmenge dieser unterschiedlichen Felder zu arbeiten oder auch kombinierte Ansätze zu verwenden. Ganz grob gesagt könnten wir anhand der bisherigen Ausführungen davon ausgehen, dass wir in der Wissenschafts- und Technikforschung auch einfach darüber nachdenken können welche Wissenschaft und Technik überhaupt möglich ist, welche Aussagen und Wahrheiten überhaupt gemacht und behauptet werden können. Also auf einer relativ abstrakten Ebene theoretische Überlegungen anstellen was Wissenschaft und Technik ausmacht. Wenn dabei dann aber ausschließlich Theoriearbeit gemacht wird, fällt das in den Bereich der Wissenschaftstheorie. Die Wissenschafts- und Technikforschung hingegen legt den Fokus auf die empirische Erforschung von Wissenschaft und Technik. Das müsste jetzt auch gar nicht so klar abgegrenzt sein, und am Ende brauchen wir immer Theorie und Empirie um die Welt vollständiger zu begreifen.

Allerdings haben sich diese unterschiedlichen Zugänge aus unterschiedlichen Forschungsfeldern entwickelt und erfordern auch unterschiedliche Methoden und Spezialisierungen. Diese Abgrenzung ist also auch historisch gewachsen und ebenso veränderlich. Zudem gibt es auch andere Kategorisierungen die "Wissenschaftsforschung" als Überbegriff für alle Teildisziplinen sieht, die sich mit Produktions- und Erkenntnisprozessen in Wissenschaft und Technik auseinandersetzen. Demnach gibt es unter dem Dach der Wissenschaftsforschung neben der Wissenschaftsphilosophie noch u.a. die Wissenschaftssoziologie, Wissenschaftsethnographie, Wissenschaftsethik, Wissenschaftsästhetik und Scientometrie. Genauso wie es auch noch eine Wissenschafts- und Technikgeschichte gibt, die sich ähnlichen Fragen eher aus einem historischen Kontext nähert und dabei Methoden der Geschichtswissenschaften anwendet.

Schauen wir uns nun die Entwicklung der Wissenschafts- und Technikforschung aus einer historischen Perspektive an und versuchen sie so etwas genauer zu verorten.

### Historische Entwicklung

Nach den meisten Einordnungen hat sich die Wissenschafts- und Technikforschung im heutigen Sinn in einer ersten Phase von den 1960ern bis in die späten 1980er Jahre hinein zu formieren begonnen. Einzelne Akteur:innen waren auch bereits davor am Erforschen verschiedener Aspekte wissenschaftlicher Theorie- und Praxisbildung. Erst durch zunehmenden Austausch dieser unterschiedlicher Akteur:innen und wissenschaftlicher Disziplinen rund um die Frage wie Wissenschaft und Technik gemacht wird, wurden dann während der 1970er und 1980er erste interdisziplinäre Programme an Universitäten geschaffen, die wir als die ersten STS Programme sehen können: also Programme in denen _Science & Technology Studies_ bzw. die _social study of science & technology_ praktiziert wird. So wurde auch im Jahr 1975 die _Society for Social Studies of Science_ (kurz: 4S) als internationale non-profit Organisation gegründet "that fosters interdisciplinary and engaged scholarship in social studies of science, technology, and medicine (a field often referred to as STS)" (Source: https://www.4sonline.org/what-is-4s/). Im Jahr 1981 wurde dann auch die _European Association for the Study of Science and Technology_ (kurz: EASST) mit ähnlichen Zielen und Zwecken, allerdings auf den europäischen Raum fokussiert gegründet. Aus ihrer Selbstbeschreibung:

>Established in 1981 it is the organization which represents academics and researchers in the fields of science and technology studies, the social analysis of innovation and related areas of knowledge. It brings together a variety of disciplines and many of its members have qualifications in both natural science/engineering and social sciences. (Source: https://www.easst.net/about-easst/)

Wir sehen also, dass dieses Feld noch ziemlich jung ist. Bevor wir uns in der kommenden Session einige relevante Akteur:innen in diesem Feld anschauen, die bereits in dessen Formierungsphase zum Thema geforscht haben, gibt es eine Person, der aus meiner Sicht aus mehreren Gründen besondere Aufmerksamkeit gewidmet werden sollte:

### Ludwik Fleck

Ludwik Fleck war ein polnisch-jüdischer Mikrobiologe und Immunologe, der von 1896 – 1961 gelebt hat. Zugleich war bzw. wurde er durch seine natur- und medizinwissenschaftliche Tätigkeit auch zum Erkenntnistheoretiker. Im medizinischen Bereich wurde er vor allem für seine Forschung zum Fleckfieber bekannt. Er hat unter anderem 1930 den ersten zuverlässigen Fleckfiebernachweis als Hauttest entwickelt. Das "Fleck" in Fleckfieber hat in diesem Fall aber nichts mit Ludwik Fleck zu tun, soweit wir das aus historischen Quellen wissen, bzw. wissen wir auch nicht ob sein Nachname für ihn eine Inspiration war, sich spezifisch diesem Thema zuzuwenden. In der englischsprachigen Literatur über Fleck wird auch primär von _epidemic typhus_ gesprochen. Fleck war also ein Typhusforscher. In seinen eigenen Publikationen wird von _Flecktyphus_ gesprochen.

Im Bereich der Wissenschafts- und Technikforschung ist Ludwik Fleck aber inzwischen vor allem für seine 1935 erschienene Monografie "Entstehung und Entwicklung einer wissenschaftlichen Tatsache. Einführung in die Lehre vom Denkstil und Denkkollektiv" bekannt (Fleck 1935).

Seine Rezeption wurde aber erst ab den 1980er Jahren und durch die Formierung der STS größer. So schreiben Lothar Schäfer und Thomas Schnelle im Vorwort zu ihrer 1980 erschienenen und kommentierten Ausgabe von Fleck's "Entstehung und Entwicklung einer wissenschaftlichen Tatsache":

> Ludwik Flecks gegenwärtig so gut wie unbekannte Schrift »Entstehung und Entwicklung einer wissenschaftlichen Tatsache« könnte unter günstigeren Umständen heute im Rang eines Klassikers der Wissenschaftstheorie stehen, vergleichbar etwa mit Poppers »Logik der Forschung« (1934). Ein Jahr nach Poppers Epoche machendem Werk erschienen, teilt es mit diesem sowohl Gegnerschaft wie Stoßrichtung: Auch Flecks Buch ist gegen die Wissenschaftsauffassung des »Wiener Kreises« geschrieben. Betonte Popper gegenüber dem statischen Theoriebegriff der logischen Empiristen den dynamischen Aspekt der Forschung, so geht Fleck jedoch entschieden weiter: er stellt den als selbstverständlich angenommenen Tatsachenbegriff selbst in Frage. Wissenschaft ist für ihn kein formales Konstrukt, sondern wesentlich eine Tätigkeit, veranstaltet von Forschergemeinschaften. (ibid, pp. VII-VIII)

Was aber ist nach Fleck nun eine Tatsache, und wie werden solche hergestellt, um Wissenschaft zu betreiben? Um seine erkenntnistheoretische Position darzulegen bzw. auch überhaupt um diese erst herzustellen, zeichnet Fleck die Entwicklung des medizinischen Begriffs der Syphilis nach. Auf dieser Basis arbeitet er die zentralen Begriffe _Denkstil_ und _Denkkollektiv_ heraus, die sich aus einer _sozialen Stimmung_ bzw. _Kollektivstimmung_ entwickeln und erst die Rahmenbedingungen für die Herstellung von Fakten bilden. Um bestimmte Erkenntnisse zu gewinnen, bedarf es demnach bestimmter Denkkollektive, um überhaupt erst bestimmte Denkweisen oder auch Interpretationen von experimentellen Ergebnissen möglich zu machen. Und um neue, innovative oder gar Paradigmen-verändernde Erkenntnisse zu gewinnen, müssen erst neue Denkkolletkive formiert werden bzw. bestehende Denkkollektive erst signifikant verändert werden. Das entscheidende daran ist, dass die nötigen Voraussetzungen nie nur individuell geschaffen werden können. Sondern eben in einem Kollektiv, einem sozialen Zusammenhang.

Nach Fleck ist ein Fakt keine objektive und universelle Warheit, sondern ein kontext-abhängiges Konzept, das in einem bestimmten sozialen Zusammenhang (dem Denkkollektiv) hergestellt wird. Die Fakten werden demnach auch nicht "entdeckt" sondern entwickelt. Um diese Fakten herstellen zu können, bedarf es nicht nur bestimmter Apperaturen und Abläufe, um Experimente durchführen zu können. Es braucht auch entsprechende kollektive Vorstellungen davon, was überhaupt erkannbar ist und erkannt werden kann. Und es braucht einen längerfristigen Austausch zwischen den Teilnehmer:innen eines Denkkollektivs, um diese Vorstellungen zu ändern und auch weiterzuentwickeln. Ein Fakt ist nicht einfach das Ergebnis einer empirischen Beobachtung oder einer logischen Schlussfolgerung. Vielmehr fließen individuelle und soziale Vorurteile und Tendenzen (vor dem sozialen Hintergrund des Denkkollektivs) - im englischen sprechen wir hier meist von _bias_ - in die Beobachtungen und Interpretationen ein, sowie auch bereits in der Herstellung von Experimenten. Demnach sind wissenschaftliche Fakten für Fleck auch unteilbar mit der Sprache (im Sinne natürlicher Sprache als auch der Fachsprache), der Symbolik und den Praktiken in einem Feld / einer wissenschaftlichen Community verbunden. Diese führen auch dazu, dass unterschiedliche Wissenschafter:innen oder auch unterschiedliche wissenschaftliche Communities die gleichen Daten in unterschiedlicher Weise interpretieren und weiterverarbeiten, und so unterschiedliche Fakten herstellen. Wobei die Unterschiede hier je nach Fokus und Größenordnung sich teilweise sehr stark unterscheiden und auch widersprechen können, oder sich aber nur in sehr kleinen Details unterscheiden, und so innerhalb eines Denkkollektivs auch in einen gemeinsamen Zusammenhang gebracht werden können und die generelle Ausrichtung des Denkstils stärken.

Fleck bezieht sich bei seiner Auseinandersetzung mit dem Denkstil neben klassischeren erkenntnistheoretischen Arbeiten auch sehr stark auf diverse neue soziologische und psychologische Arbeiten aus der ersten Hälfte des 20. Jahrhunderts - also aktuelle wissenschaftliche Erkenntnisse in anderen wissenschaftlichen Disziplinen als der klassischen Wissenschaftstheorie, die sich sonst mit der Frage des wissenschaftlich Erkennbaren auseinandersetzt. Es geht dabei auch stark darum was für uns Menschen als Individuen überhaupt denkbar ist und was als möglich oder auch unmöglich erscheint:

> Was wir als Unmöglichkeit emp­finden, ist nur Inkongruenz mit dem gewohnten Denkstil. Umwandlung der Elemente und vieles andere aus der modernen Physik, von der Wellentheorie der Materie ganz zu schweigen, galten vor unlängst als vollkommen »un­möglich«. Es existiert keine »Erfahrung an sich«, der man zugänglich oder unzugänglich sein könne. Jedes Wesen erlebt nach seiner Art und Weise. Gegenwärtige Erlebnisse verknüpfen sich mit früheren und verändern so die Bedingungen zukünftiger. Jedes Wesen macht also »Erfahrun­gen« in diesem Sinne, daß es während seines Lebens die Reaktionsweise ändert. Die spezifische wissenschaftliche Erfahrung stammt von besonderen, denkhistorisch und sozial gegebenen Bedingungen. Für sie wird nach traditionellen Mustern dressiert, aber man ist ihr nicht einfach zugänglich. (ibid, p. 66)

Etwas weiter im Text schreibt er:

> Jedes Erkennen bedeutet zunächst: bei bestimmten aktiv vorgenommenen Voraussetzungen die zwangsmäßig, pas­siv sich ergebenden Zusammenhänge festzustellen. (ibid, p. 85)

Um diese Zusammenhänge festzustellen brauchen wir aber einen bestimmten Denkstil, der im wissenschaftlichen Bereich zuerst durch eine allgemeine Einführung in die Wissenschaft erlangt wird, dessen Herausbildung aber in der wissenschaftlichen Ausbildung "bis in kleinste Einzelheiten der Fach­wissenschaften reicht" (ibid). Und weiter:

> Denkstil ist nicht nur diese oder jene Färbung der Begriffe und diese oder jene Art sie zu verbinden. Er ist bestimmter Denkzwang und noch mehr: die Gesamtheit geistiger Be­reitschaften, das Bereitsein für solches und nicht anderes Sehen und Handeln. Die Abhängigkeit der wissenschaftlichen Tatsache vom Denkstil ist evident. (ibid)

Es geht beim Denkstil um ein Bündel von (nicht-in-Frage-zu-stellenden) Vorannahmen bzw. auch Glaubenssätze, also auch Praktiken, welche die Gedankengänge und möglichen Schlussfolgerungen eines Individuums in einem bestimmten sozialen Zusammenhang leiten. Dem Individuum stehen auch nur bestimmte Sprachen, Symbole und Kommunikationsweisen zur Verfügung, um diese Gedanken innerhalb des sozialen Zusammenhangs zu formulieren und in Austausch zu bringen.

Diese sozialen Zusammenhänge werden von Fleck als "Denkkollektive" beschrieben. Denkkollektive können in ihrer Form und Größe sehr unterschiedlich sein, und wir sind meist Teil mehrerer unterschiedlicher Denkkollektive. Denkkollektive entstehen, verschwinden und verändern sich im Alltag laufend. So schreibt Fleck:

> Ein Denkkollektiv ist im­mer dann vorhanden, wenn zwei oder mehrere Menschen Gedanken austauschen. Ein schlechter Beobachter, wer nicht bemerkt, wie anregendes Gespräch zweier Personen bald den Zustand herbeiführt, daß jede von ihnen Gedan­ken äußert, die sie allein oder in anderer Gesellschaft nicht zu produzieren imstande wäre. Eine besondere Stimmung stellt sich ein, der keiner der Teilnehmer sonst habhaft wird, die aber fast immer wiederkehrt, so oft beide Perso­nen Zusammenkommen. Längere Dauer dieses Zustandes erzeugt aus gemeinsamem Verständnis und gegenseitigen Mißverständnissen ein Denkgebilde, das keinem der Zwei angehört, aber durchaus nicht sinnlos ist. Wer ist sein Träger und Verfasser? Das kleine zweipersonale Kollektiv. Kommt ein Dritter hinzu, so macht er die frühere Stim­mung verschwinden und mit ihr die besondere schöpferische Kraft des früheren Denkkollektives; ein neues ent­steht. (ibid, p. 60)

Und zur Frage der Mitgliedschaft in unterschiedlichen Denkkollektiven:

> Ein Individuum gehört eben mehreren Denkkollektiven an. Als Forscher gehört es zu einer Gemeinschaft, mit der es arbeitet und oft unbewußt Ideen und Entwicklungen heraufbeschwört, die, bald selbständig geworden, sich nicht selten gegen ihren Urheber wenden. Als Parteimit­glied, als Angehöriger eines Standes, eines Landes, einer Rasse usw. gehört es wiederum anderen Kollektiven an. Gerät es zufällig in irgendeine Gesellschaft, wird es bald ihr Mitglied und gehorcht ihrem Zwange. (ibid, p. 61)

Während im ganz Kleinen, wenn es um zwei, oder wenige Menschen geht es sich um "momentane, zufäl­lige Denkkollektive" handelt, "die jeden Augenblick entstehen und vergehen" können, so gibt es abseits dieser "zufälligen und momentanen Denkkollekti­ven [auch] stabile oder verhältnismäßig stabile" (ibid, p. 135):

> sie bilden sich besonders um organisierte soziale Gruppen. Existiert eine größere Gruppe lange genug, so fixiert sich der Denkstil und bekommt formale Struktur. Die realisierende Ausführung dominiert über die schöpferische Stimmung, die auf ein gewisses diszipliniertes, gleichmäßiges, diskretes Niveau sinkt. In dieser Situation befindet sich die gegenwärtige Wissenschaft als spezifisches, denkkollektives Gebilde. Die Denkgemeinschaft deckt sich nicht vollkommen mit der offiziellen Gemeinschaft: zum Denkkollektiv einer Religion gehören alle wirklich Gläubigen, zur offiziellen Religionsgemeinschaft die formell Aufgenommenen, unbeachtet ihrer Denkstruktur. Man kann also zum Denkkol­lektiv einer Religion gehören, ohne in die Gemeinde for­mell aufgenommen zu sein, und umgekehrt. Auch ist die innere Struktur, die Organisation des Denkkollektivs ver­schieden von der Organisation der Gemeinschaft im offizi­ellen Sinne: die geistigen Führer und die Kreise, die um sie entstehen, decken sich nicht mit der offiziellen Hierarchie und Organisation. (ibid, pp. 135-136)

Werden Denkkollektive größer, so werden auch Organisationen und Institutionen geschaffen, um diese Denkkollektive zu betreiben und weiterzuentwickeln. Wobei es hier immer ambivalente Tendenzen zwischen dem Beharren auf einem stabilen Zustand und dem Weiterentwickeln gib. Generell gilt laut Fleck: "Denkgemein­schaften pflegen, wie andere organisierte Gemeinden, eine gewisse formelle und inhaltliche Abgeschlossenheit" (ibid, p. 136). Es gibt also mit dem Wachstum eines Denkkollektives eine gewisse Verstetigungstendenz, die es schiweriger macht radikal neue Ideen innerhalb dieses Denkkollektives zu formulieren. Größere Umbrüche bedürfen daher immer einer größeren Vorbereitung, auch wenn es oft im Nachhinein so scheint, als hätte es einen sehr kurzen oder gar singulären Zeitpunkt gegeben, an dem ein wissenschaftlicher Umbruch stattgefunden hat. Denn jeglicher Beharrungstendenz zum Trotz werden Denkkollektive kontinuierlich durch Kommunikation und Kooperation von Individuen innerhalb des Denkkollektivs weiterentwickelt.

Bevor wir uns - vorerst - von Ludwik Fleck lösen um die Entwicklung der STS weiterzuverfolgen, möchte ich noch eine Passage von Fleck zitieren, die auch veranschaulicht was Fleck's eigene Ableitung der Denkkollektivtheorie für (wissenschaftliches/menschliches) Wissen und Denken ist:

> Die Fruchtbarkeit der Denkkollektivtheorie zeigt sich eben in ihrer Möglichkeit primitives, archaisches, kindli­ches und psychotisches Denken zu vergleichen und ein­heitlich zu untersuchen. Schließlich auch das Denken eines Volkes, einer Klasse, einer wie immer gearteten Gruppe. Ich halte das Postulat vom Maximum der Erfahrung für das oberste Gesetz wissenschaftlichen Denkens. Zeigt sich nun einmal die Möglichkeit vergleichender Erkenntnis­theorie, so wird sie zur Pflicht. Der alte Standpunkt, der über normative Feststellungen vom »schlechten« und »gu­ten« Denken nicht hinauskommt, ist überholt.
> Man verstehe die hier dargelegten Ansichten nicht als Skeptizismus. Wir können sicher vieles wissen. Und wenn wir nicht nach altem Rezept »Alles« wissen können, so geschieht es einfach deshalb, weil mit der Bezeichnung »Alles« hier nicht viel anzufangen ist; denn mit jeder neuen Erkenntnis erscheint wenigstens ein weiteres neues Pro­blem: das Untersuchen des Erkannten als solchen. So wird die Zahl der zu lösenden Probleme unendlich und die Bezeichnung »Alles« unsinnig.
> Ebensowenig wie es ein »Alles« gibt, gibt es ein »Allerletztes«, ein Fundamentales, aus dem sich die Erkenntnis lo­gisch aufbauen ließe. Das Wissen ruht eben auf keinem Fundamente; das Getriebe der Ideen und Wahrheiten er­hält sich nur durch fortwährende Bewegung und Wechsel­wirkung. (ibid, p. 70)

Zusammenfassend möchte ich also festhalten, dass Ludwik Fleck die Wichtigkeit von sozialen und kulturellen Faktoren für das Schaffen wissenschaftlichen Wissens hervorgehoben hat. Und das hat er ca. ein halbes Jahrhundert getan bevor sich die Science & Technolgie Studies, oder eben auch die Wissenschafts- & Technikforschung als eigenes Feld etabliert hat - ein Feld das sich um den Ansatz formiert Wissenschaft als soziale und kulturelle Praxis zu untersuchen.

Wieso aber hat Fleck bis in die 1980er kaum jemensch aus diesem Feld wahrgenommen? Während wir sicher auch darüber nachdenken können inwieweit es noch kein Denkkollektiv gab, das bereit gewesen wäre Fleck's Beiträge nachhaltig zu integrieren, und dass er hier die nötige (und undankbare) Vorarbeit geleistet hat, die es oft braucht, bis neue Erkenntnisse in größerem Rahmen denkbar werden, so gab es auch einige sehr handfeste Gründe, die mir auch besonders wichtig ercheinen hier erwähnt zu werden.

Ich hebe hier nur ein paar Rahmendaten hervor, für mehr Hintergründe zu Fleck und auch einer detaillierten Betrachtung seines Beitrags für die Wissenschaftsforschung und Erkenntnistheorie möchte ich auf den von Wojciech Sady hervorragend ausgearbeiteten Artikel zu Ludwik Fleck in der Stanford Encyclopedia of Philosophy (Sady 2021) verweisen.

Fleck wurde am 11. Juli 1896 in einer jüdischen Familie in Lemberg (heute: Lviv) geboren. Seine Eltern führten einen Malereibetrieb, und Fleck ist mit polnisch als Muttersprache aufgewachsen. Fleck hat aber ebenso fließend Deutsch gesprochen, was ein Grund dafür war, dass sein für uns zentrales Werk im deutschen Original vorliegt. 1914 begann er an der Universität Lemberg ein Medizinstudium. Dieses musste er für den Militärdienst als Arzt im 1. Weltkrieg unterbrechen. Ab 1920 war er als Assistent des Biologen und Fleckfieber-Spezialisten Rudolf Weigl tätig, erst in einem Militärlabor in Przemyśl, später dann an der Universität Lemberg. 1922 promivierte er - dann bereits im polnischen Lwów - und verließ im Jahr darauf die Universität um ein eigenes, privates bakteriologisches Laboratorium zu gründen. Er übernahm aber auch am Allgemeinen Krankenhaus die Leitung zuerst des bakteriologisch-chemischen Labors an der Inneren Medizin, später des bakteriologischen Labors an der Abteilung für Haut- und Geschlechtskrankheiten. Bis 1935 gibt es noch ein paar weitere Engagements, ab da arbeitete er aber ausschließlich in seinem eigenen privaten Labor. Er war also auch bis zur Veröffentlichung seines philiosophischen / erkenntnistheoretischen Hauptwerks primär als medizinisch-bakteriologischer Praktiker tätig.

Er war zudem immer nur peripher an akademische Communities angebunden, sowohl was den medizinischen Bereich betrifft als auch noch mehr was den wissenschaftstheoretischen Bereich betrifft. Forschung hat er vor allem im medizinischen Bereich betrieben, in dem er auch um die 40 papers veröffentlicht hat, vor allem auf polnisch und deutsch. 1927 hat er seine erste wissenschaftstheorietische Arbeit auf polnisch veröffentlicht, unter dem (übersetzten) Titel "Über einige spezifische Merkmale des ärztlichen Denkens". Zwei Jahre später veröffentlichte er auf deutsch den wissenschaftstheoretischen Aufsatz "Zur Krise der „Wirklichkeit“". Flecks Gedanken waren dabei auch gar nicht so sehr von soziologischen oder psychologischen Beiträgen geprägt, auf die er später in seinem Hauptwerk Bezug nimmt, sondern vielmehr von den zu dieser Zeit vor allem von der Quantentheorie inspirierten Debatten um Wahrheit und Wirklichkeit, und der Idee, dass es eine Verschränkung zwischen Beobachter:in und Beobachtetem gibt, welche die Vorstellung einer "absoluten Wirklichkeit" in Frage stellt.

Nach dem deutschen Überfall auf Polen im September 1939 und der anschließenden russischen Invasion im Osten wurde Fleck zum Direktor des städtischen Hygiene-Instituts. Mit der deutschen Besatzung ab 1941 verlor Fleck diese Position und wurde ins Ghetto von Lvov gezwungen. Über Flecks weitere Stationen bis zur Befreiung von Nazi-Deutschland schreibt Wojciech Sady:

> Anti-Jewish pogroms began, and Germans also started to murder university professors of Polish origin. Fleck and his wife survived, probably because Rudolf Weigl included him on the list of employees of the Lvov Institute of Research on Typhus and Viruses. In August 1941, Germans resettled Fleck to the Lvov ghetto, where he developed and produced an anti-typhus vaccine from the urine of sick people. He was then relocated with his family to the area of the pharmaceutical company Laokoon and tasked with producing an anti-typhus vaccine for German soldiers. Finally he and his wife and son were deported to the concentration camp in Auschwitz. At first he was forced to do physical labor, but later he was transferred to the camp’s hospital with the assignment to conduct bacteriological tests on prisoners. In December 1943 Fleck was transferred to the concentration camp in Buchenwald. There a group of prisoners—scientists and medical doctors of various nationalities—led by a scientifically illiterate German doctor produced an anti-typhus vaccine. (ibid)

In Buchenwald wurde er von den Nazis zur Forschung an einem Fleckfieber-Impfstoff für das Hygiene-Institut der Waffen-SS gezwungen. Dabei beteiligte sich Fleck an einer Sabotage Aktion in der bewusst unwirksamer Impfstoff an die SS geliefert, allerdings wirksamer Impfstoff für die Kontrollen von Mithäftlingen produziert wurde. Während Fleck, seine Frau und sein Sohn den Krieg und den Vernichtungswahn der Nazis überlebte, kamen alle anderen Familienangehörigen um.

Nach einem längeren Krankenhausaufenthalt ging Fleck nach Lublin um weiterzuforschen. Er wurde dort später auch Professor an der Medizinischen Universität Lublin. 1952 zog er weiter nach Warschau und wurde dort Direktor der Abteilung für Mikrobiologie und Immunologie des Mutter-und-Kind-Instituts. 1954 wurde er zum Mitglied der Polnischen Akademie der Wissenschaften gewählt. 1956 erlitt er einen Herzinfarkt und erhielt 1957 auch eine Krebsdiagnose. Daraufhin emigrierte Fleck mit seiner Frau nach Israel, wohin ihr Sohn bereits nach dem Krieg emigriert war. In Nes Ziona abreitete Fleck daraufhin im Israelischen Institut für biologische Forschung als Leiter der Abteilung für experimentelle Pathologie. 1961 starb er im Alter von 64 Jahren an einem zweiten Herzinfarkt.

Warum war es mir nun so wichtig, so ausführlich über Ludwik Fleck und seine Denkkollektive zu sprechen, wo doch die Science & Technology Studies sich erst so ab den 1980ern richtig formiert haben? Ein Grund ist, weil sich hier auch zeigt wie wissenschaftliche Entwicklung nicht linear sondern oft auch sehr kontingent verläuft, abhängig von verschiedensten Umweltfaktoren. Einerseits gab es unter Umständen in Flecks Umfeld auch einfach noch kein stabiles Denkkollektiv, das seine Idee der Denkkollektive unmittelbar weiterspinnen wollte. Andererseits war seine Verortung außerhalb spezifischer Forschungskreise in diesem Feld sicher auch ein Mitgrund. Und schließlich kommt hier auch noch seine soziale Verortung und die Gräuel des zweiten Weltkriegs sowie der Nazi-Vernichtungsideologie hinzu, die nicht nur Menschen, sondern mit ihnen auch Ideen und bestimmte wissenschaftliche Fortschritte vernichten wollte, wenn diese in der "Gefahr" waren emanzipatorische Potentiale freizusetzen. Dies wird auch an anderen Stellen im Verlauf der Lehrveranstaltung wieder sichtbar werden.

Haben wir uns in dieser Session neben einem allgemeinen Zugang zu Wissenschafts- und Technikforschung erstmal mit einem wichtigen Vordenker (oder vielleicht auch Denkkollektiv-Vorbereiter) auseinandergesetzt, werden wir uns in der kommenden Session den ersten Wegbereiter:innen in der Formierungsphase der STS sowie aktuellen Akteur:innen und Zugängen widmen.

## Diskurs-Sequenz

* 3min individuell:
	* welche Gedanken während der Input-Sequenz hatte ich, wo ich das Gefühl hatte, das ist doch ohnehin alles klar und sollte keiner weiteren Erklärung bedürfen?
	* welche Gedanken hatte ich, die mir im Gegensatz dazuso gar nicht zugänglich schienen?
	* gabs hier schon Verknüpfungen zu dem was wir in der Vorbereitung gelesen haben?
* 2min Slido:
	* Begriffe die mir in der Input-Sequenz am meisten hängen geblieben sind
* 10min bzw. open end: Plenumsdiskussion offener Fragen & Punkte

## Vorbereitung auf Session 3
- Recorded Talk: Ulrike Felt (2017): [Die Entstehung wissenschaftlicher Tatsachen](https://www.youtube.com/watch?v=XSVf3SYCmrU) (Video auf Youtube, ~42min)
  (für Details siehe weiterführendes Material unten)
- Lesen der Input-Sequenz von [](./session3.md) (~30min)

## Checkout
Slido:
* Rating poll: Wie viel war für mich heute neu?
* Rating poll: Wie nachvollziehbar fand ich die Input-Sequenz?
* Rating poll: Wie stark würde ich nun meine Einschätzung zu den relavanten Fähigkeiten für Wissenschaft vom Anfang der Session revidieren?
* Wordcloud: Mit welchem Gefühl gehe ich nun aus der LV und in den Abend?

## Referenzen

Fleck, Ludwik. 1935. Entstehung und Entwicklung einer wissenschaftlichen Tatsache: Einführung in die Lehre vom Denkstil und Denkkollektiv. Edited by Lothar Schäfer and Thomas Schnelle. 13. Auflage (2021, Erstausgabe 1980). Suhrkamp Taschenbuch Wissenschaft. Frankfurt am Main: Suhrkamp.

Sady, Wojciech. 2021. “Ludwik Fleck.” In The Stanford Encyclopedia of Philosophy, edited by Edward N. Zalta, Winter 2021. Metaphysics Research Lab, Stanford University. https://plato.stanford.edu/archives/win2021/entries/fleck/.

## Weiterführendes Material

* Ulrike Felt: [Die Entstehung wissenschaftlicher Tatsachen](https://www.youtube.com/watch?v=XSVf3SYCmrU) (Video auf Youtube, ~42min)
	* Vortrag im Rahmen des 3. Symposium der Gesellschaft für Philosophie und Medizin, 2017. Hier wird Fleck nochmal aus ganz aktueller Perspektive aufgegriffen und anhand aktueller STS Forschung im Medizinischen Bereich am Beispiel der Adipositas herausgearbeitet, was die Schwierigkeit in der Herstellung wissenschaftlicher Tatsachen ist, und wie hier unterschiedliche Denkkollektive und -stile sich ergänzen und widersprechen.
		* Ulrike Felt ist Professorin für Wissenschafts- und Technikforschung und Head of the Department of Science and Technology Studies an der Universität Wien. Sie war u.a. von 2017-2021 auch Präsidentin der EASST (European Association for the Study of Science and Technology). Entsprechend können wir hier einen state-of-the-art-Einblick in die Bedeutung von Fleck für die aktuelle STS bekommen.
