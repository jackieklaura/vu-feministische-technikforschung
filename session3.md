# Session 3 - Wissenschafts- & Technikforschung, Part 2

> _Die begleitenden Slides gibt es hier: [slides/session3.html](https://tantemalkah.at/feministische-technikforschung/2024/slides/session3.html)_

## Aktiver Teilnahmebeitrag

Im TUWEL (Moodle) steht ein Forum zur Diskussion folgender Fragen bereit:

> In welchen Bereichen in eurem eigenen Studium kommt (so etwas wie)
> Wissenschafts- & Technikforschung vor, z.B. in Form von Methodenreflexion?
> Gibts auch Bereiche wo es mehr davon bräuchte? Bzw. wenn es andererseits
> in manchen Bereichen auch sinnvoller wäre, das explizit an eine externe
> Disziplin auszulagern, wie sollte das ins eigene Fach miteinbezogen werden?

Beiträge können über den gesamten Tag, an dem die Session stattfindet,
eingebracht werden und zählen im Sinne der Punkte für die Note als aktive
Teilnahme für diese Session. Das können entweder initiale Beiträge oder
auch Antworten auf breits eingebrachte Beiträge sein. Dabei muss ein
initialer Beitrag nicht extra recherchiert und besonders “wissenschaftlich”
ausgeführt werden. Es geht um eine Momentaufnahme der eigenen Gedanken,
die für die Kolleg:innen nachvollziehbar sein sollen (ein paar Minuten
über die Fragen bzw. eine der Fragen nachdenken und ein, zwei, drei Sätze
dazu formulieren).

> Wichtig: es geht hier nicht darum, eine "richtige" Antwort zu finden,
> sondern eine niederschwellige Diskussion in Gang zu bringen. Und dazu
> nutzen wir einen digitalen Seitenkanal, weil sich das im Hörsaal in
> der Skalierung sonst nicht ganz so ausführlich ausgeht.

## Einstiegs-Sequenz

Zum Start:
- 5min 2/3er Murmelgruppen
	- Wie lief die Vorbereitung?
	- Was ist eure Erwartung an die heutige Session?
	- Was hat mir nach der letzten Session am meisten gefehlt?

Check-in Slido, um die Inhalte der vorab gelesenen Input-Sequenz zu aktivieren:
- Word cloud: Welche normativen Ansprüche an Wissenschaft gibt es für euch (nicht nur an Merton orientiert)?
- Word cloud: "Was siehst du?" anhand eines [Bildes von Joseph Jastrow](https://upload.wikimedia.org/wikipedia/commons/4/45/Duck-Rabbit_illusion.jpg), das Kuhn verwendete um die Änderung der wissenschaftlichen Wahrnehmung zu veranschaulichen.
- Word cloud: Was macht Wissensproduktion in einem Laborsetting so besonders?
- Word cloud: In welchen Aspekten unterscheidet sich Wissenschaft heute von Wissenschaft vor 50 Jahren?

Da die Input-Sequenz bereits als Vorbereitung auf diese Session gelesen wurde,
können wir in der Session an dieser Stelle direkt zur Diskurs-Sequenz springen.

## Input-Sequenz

In der letzten Einheit haben wir durch ganz allgemeine Überlegungen versucht zu beschreiben und einzugrenzen was Wissenschafts- & Technikforschung ist und macht. Dann haben wir uns mit Ludwik Fleck einen lange vergessenen Vordenker angesehen, der mit seinen Konzepten des Denkstils und der Denkkollektive eine theoretische Vorlage dafür erarbeitet haben, dass wir Wissenschaft und Technik vor allem auch als soziale Praxis verstehen müssen, die überhaupt erst die Generierung von neuen Fakten und Sichtweisen erlaubt.

In dieser Einheit wenden wir uns nun frühen Wegbereiter:innen sowie aktuellen Akteur:innen in der STS zu, und sehen uns einige inhaltlich-methodische Zugänge an, die in der STS verbreitet sind.

### Robert K. Merton

Ein sehr früher Akteur in der Formierung des STS-Feldes ist Robert K. Merton, ein US-amerikanischer Soziologe, der bereits in den 1940er Jahren wissenschaftssoziologische Studien durchführte und unter anderem 1942 den Text "The Normative Structure of Science" (Merton 1979) veröffentlichte. Darin macht er einen wissenschaftlichen Ethos aus, der sich um 4 zentrale Normen ergibt:
* "universalism"
* "communism" bzw. in späteren Versionen: "communalism"
* "disinterestedness"
* "organized skepticism"

Universalism beschreibt dabei die Haltung, dass jeglicher Wissensbeitrag unabhängig vom sozialen Status oder anderern persönlichen Charakteristika d: beitragenden Wissenschafter:in bewertet wird. Es soll also stets um eine gewisse Objektivität im Sinne interpersoneller Nachvollziehbarkeit und konstanter Reproduzierbarkeit der Ergebnisse gehen.

Communism (oder später im Zuge des us-amerikanischen Antikommunismus als "communalism" reframed) bezeichnet die Haltung, dass jegliche wissenschaftliche Erkenntnis der gesamten wissenschaftlichen Community zugänglich sein muss. Es darf also kein "ownership" über wissenschaftliche Erkenntnisse geben, wobei sehr wohl eine namentlich Anerkennung wichtig ist:
> "The scientist's claim to “his” intellectual “property” is limited to that of recognition and esteem which, if the institution functions with a modicum of efficiency, is roughly commensurate with the significance of the increments brought to the common fund of knowledge." (ibid)

Das argumentiert Merton u.a. auch mit Bezug auf Newton's Aussage "If I have seen farther it is by standing on the shoulders of giants", da jegliche Erkenntnis nur eine kleine Teilleistung in einem größeren, kollektiven wissenschaftlichen Prozess sein kann, die auch ohne diesen Rahmen gar nicht möglich wäre. Wissen soll also umfänglich geteilt werden, was in unserem Wirtschaftsmodell oft im Widerspruch zu gängiger Technologieproduktion steht:

> "The communism of the scientific ethos is incompatible with the definition of technology as “private property” in a capitalistic economy. Current writings on the “frustration of science” reflect this conflict. Patents proclaim exclusive rights of use and, often, nonuse. [...] As a defensive measure, some scientists have come to patent their work to ensure its being made available for public use. Einstein, Millikan, Compton, Langmuir have taken out patents. Scientists have been urged to become promoters of new economic enterprises. Others seek to resolve the conflict by advocating socialism. These proposals - both those which demand economic returns for scientific discoveries and those which demand a change in the social system to let science get on with the job - reflect discrepancies in the conception of intellectual property." (ibid)

Während Merton darüber in den 1940er Jahren geschrieben hat, ist die Frage wie produktiv damit umzugehen ist bist heute nicht richtig geklärt. Open Science, Open Access Journals, Open Source Software sind allesamt Phänomene die im Prinzip der selben Problematik entspringen.

Die dritte von Merton angeführte Norm: Disinterestedness. Damit meint Merton primär, dass Wissenschafter:innen - wenn sie im Rahmen der Norm arbeiten - weder von rein altruistischen noch ihren unmittelbar eigenen (womöglich egoistischen) Motiven geleitet sind, sondern vom Bedarf der wissenschaftlichen community selbst, bzw. auch von Dingen wie "passion for knowledge, idle curiosity, altruistic concern with the benefit of humanity" (ibid).

Auf verschiedene Arten mit den anderen Normen verknüpft ist schließlich Merton's vierte Norm: Organized skepticism. Auf einen individuellen Nenner heruntergebrochen könnten wir hier auch von kritischem Denken sprechen, das Vorurteile, Vorannahmen und (wissenschaftliche) Glaubenssätze (sowohl die eigenen als auch jene anderer) hinterfragt. Hier könnten wir auch einen gewissen Anti-Bias Grundsatz angelegt sehen. Wir werden allerdings in einer späteren Session noch feststellen, dass diese Norm oft nur sehr selektiv eingehalten wird.

Ein weiterer wichtiger Beitrag zum sich formierenden Feld der STS war das Konzept des Matthew-Effekt (Merton 1968). Dieses stellt fest, dass Forschungsförderung meistens nach einem Prinzip geschieht, das bereits bestehende wissenschaftliche Leistungen überdurchschnittlich anerkennt. So werden tendentiell jene besonders gefördert, die bereits auf große Forschungsleistungen (unter anderem aufgrund von früherer Förderung) zurückblicken können, während neue und unbekanntere Foscher:innen tendentiell ausgeblendet werden. Ironischerweise basierte Merton's Ausarbeitung des Konzepts auch sehr stark auf Daten die von Harriet Zuckerman, und Merton stellte erst Jahre später fest, dass sie im Sinne der wissenschaftlichen Anerkennung eigentlich auch als Ko-Autorin seines Papers aufschneinen hätte sollen. Wir werden in einer späteren Session aber auch noch den Matilda-Effekt kennenlernen, der in den 1990ern von Margaret Rossiter herausgearbeitet wurde und ähnliche Phänomene in Bezug auf Geschlecht beschreibt.

### Thomas S. Kuhn

Ein weiterer früher Wegbereiter für die STS, der aber selbst nicht direkt STS research im heutigen Sinne machte, war Thomas Samuel Kuhn, ein US-amerikanischer Wissenschaftshistoriker und -theoretiker, der ursprünglich Physik an der Harvard University studierte und dort auch 1949 seinen PhD in Physik abschloss. Währenddessen begann er dort auch als Harvard Junior Fellow einen Kurs über die history of science zu geben, und hat sich so immer mehr mit Wissenschaftsgeschichte und -theorie auseinandergesetzt, was ihn dazu bewegte das Feld zu wechseln. 1956 wurde er in Berkely Assistenzprofessor für Wissenschaftstheorie und -geschichte. Später wechselte er nach Princeton und dann ans MIT.

Besonders bekannt wurde Kuhn für sein 1962 erstmals veröffentlichtes Werk _The Structure of Scientific Revolutions_ (Kuhn 1996). Dieses war in weiterer Folge sowohl für die Wissenschaftsgeschichte und -theorie, als auch für die Wissenschaftssoziologie sehr einflussreich. Seine zentrale These ist, dass sich Wissenschaften von einer Protowissenschaft, in der relativ freies und offen experimentelles Forschen möglich ist, zu einem Stadion der Normalwissenschaft entwickeln, in der ein gewisses Paradigma etabliert ist. In dieser Phase der Normalwissenschaft ist das Feld dann entsprechend stabilisiert und die Grundsätze scheinen nicht mehr veränderlich. Erst durch eine Ansammlung immer mehr kleinerer Widersprüche formiert sich schließlich eine kritische Masse destabilisierender Beiträge (wir könnten hier auch von neu formierten Fakten sprechen), die zu einem Umbruch, oder bei Kuhn "Paradigmenwechsel" führt. Diesen beschreibt Kuhn auch als revolutionären Bruch, weil das neue bzw. neu zu etablierende Paradigma inkommensurabel mit dem Alten ist. Und erst nachdem sich der Paradigmenwechsel gesetzt hat und eine neue Phase der Normalwissenschaft etabliert ist, erscheint dieser Umbruch als logisch, linear und stringent.

Als Paradigma beschreibt er "a set of recurrent and quasi­standard illustrations of various theories in their conceptual, observational, and instrumental applications" (ibid, p.43), das von einer bestimmten scientific community geteilt wird. "By studying them and by practicing with them, the members of the corresponding community learn their trade" (ibid). Das knüpft nicht zufällig daran an, was wir in der letzten Einheit besprochen haben.

Während Kuhn im Kerntext vor allem mit wissenschaftshistorischen Mitteln arbeitet - er hat vor allem viele Physiker:innen interviewt, u.a. auch Niels Bohr einen Tag vor dessen Tod - und sich unter anderem stark auf die Gestaltpsychologie und das Gestaltwahrnehmen bezieht, ist seine Arbeit insgesamt auch sehr stark von Ludwik Fleck beeinflusst. Dieser wird von Kuhn aber nur einmal im Vorwort erwähnt:

> "I have encountered Ludwik Fleck’s almost unknown monograph, Entstehung und Entwicklung einer wissenschaftlichen Tatsache (Basel, 1935), an essay that anticipates many of my own ideas. Together with a remark from another Junior Fellow, Francis X. Sutton, Fleck’s work made me realize that those ideas might require to be set in the sociology of the scientific community. Though readers will find few references to either these works or conversations below, I am indebted to them in more ways than I can now reconstruct or evaluate." (ibid, viii-ix)

In der 50-Jahr-Jubiläumsausgabe (2012) schreibt Ian Hacking ein Introductory Essay, in dem unter anderem folgende Fußnote auf Fleck's Einfluss auf Kuhn verweist:

> "Many aspects of Kuhn’s analysis were prefigured by Ludwik Fleck (1896–1961), who published in 1935 an analysis of science perhaps more radical than Kuhn’s. [...] Kuhn’s scientific community matches Fleck’s notion of a “thought-collective,” characterized by a “thought-style,” which many readers now see as analogous to a paradigm. Kuhn acknowledged that Fleck’s essay “anticipated many of my own ideas” (Structure, xli). Kuhn was instrumental in finally getting the book translated into English. Late in life he said he was put off by Fleck’s writing in terms of “thoughts,” internal to the mind of an individual rather than communal." (Kuhn 2012)

Während Fleck also größtenteils übersehen wurde, konnte Kuhn ca. 30 Jahre später, in einem anderen Kontext, mit einer ähnlichen aber vielleicht etwas anders gelagerten Interpretation wissenschaftlichen Handelns und wissenschaftlicher Entwicklung zu einer umfassenden und einflussreichen Rezeption kommen. Erst durch Kuhn wurden die Worte Paradigma und Paradigmenwechsel so popularisiert, dass sich inzwischen alle irgend etwas darunter vorstellen können - meist nicht das was Kuhn damit meinte - und es da und dort verwenden. Kuhn selbst hat sich bereits relativ früh vom Paradigmenbegriff entfernt - u.a. aufgrund von Kritik einerseits, aber auch der diffusen popularisierten Verwendung - und sprach später von einer disziplinären Matrix anstatt vom Paradigma (auch diesen Begriff hat er aber bald wieder verworfen). Worum es ihm im Kern aber geht, beschreibt er in einem Postscriptum von 1969:

> "On the one hand, [the paradigm] stands for the entire constellation of beliefs, values, techniques, and so on shared by the members of a given community. On the other, it denotes one sort of element in that constellation, the concrete puzzle-solutions which, employed as models or examples, can replace explicit rules as a basis for the solution of the remaining puzzles of normal science." (Kuhn 1996, p. 175)

Wir sehen hier also wieder, wie aus einer wissenschaftgeschichtlichen und -theoretischen Perspektive das Feld für die Untersuchung von Wissenschaft und Technik als soziale Praxis geebnet wird.

* Diskurs-Exkurs an dieser Stelle:
	* kurze Slido-Wordcloud "Was siehst du?" anhand eines [Bildes von Joseph Jastrow](https://de.wikipedia.org/wiki/Thomas_S._Kuhn#/media/Datei:Duck-Rabbit_illusion.jpg), das Kuhn verwendete um die Änderung der wissenschaftlichen Wahrnehmung zu veranschaulichen.

Wir könnten jetzt unterschiedliche Mutmaßungen darüber anstellen, wieso Fleck nicht, Kuhn aber so breit rezipiert wurde, und so viel Einfluss entwickeln kann. Einige mögliche Gründe dafür - wieso Fleck nicht breiter rezipiert wurde - haben wir letzte Einheit schon erwähnt. Und Kuhn selbst war sich zum Zeitpunkt der Veröffentlichung von Structure of Scientific Revoultions gar nicht so sicher, wie das in unterschiedlichen Disziplinen angenommen wird. Vor allem aber auch die begriffliche Färbung (mit Begriffen wir Structure und Revolution) dürften im damaligen Zeitgeist mehr resoniert haben und haben an Begrifflichkeiten angeschlossen, die in den Humanities auch stark diskutiert und verwendet wurden - vor allem "structure" um komplexe Phänomene systematisch zu untersuchen und zu unterteilen.

Mit Fleck selbst gesprochen könnten wir hier noch mehr Evidenz dafür finden, dass sich das relevante Denkkollektiv erst jetzt zu einer kritischen Größe entwickelt hat. Oder mit Kuhn selbst gesprochen, dass so etwas wie die STS gerade erst in einem frühen protowissenschaftlichen Stadium entsteht, und dadurch aber entsprechende Resonanz gewinnt.

### Karin Knorr Cetina

Wenden wir uns nun einer unmittelbareren Mitbegründerin in der ersten Formierungsphase der STS zu, Karin Knorr Cetina (oft auch, vor allem in älteren Publikationen, Knorr-Cetina), eine österreichischen Soziologin, geboren 1944 in Graz, die sich vor allem auf die Bereiche der Wissenssoziologe und Wissenschaftssoziologie spezialisiert hat. Ihr Doktorat hat sie 1971 an der Universität Wien noch in Anthropologie abgeschlossen, hat sich dann aber zunehmen der Soziologie zugewendet. 1976-77 erhielt sie ein Fellowship am Institute for the Study of Social Change an der University of California, Berkeley. 1979-81 war sie Research Fellow beim Department of Sociology der University of Pennsylvania.

Während ihrem ersten Fellowship in Kalifornien untersuchte sie ein großes naturwissenschaftliches Forschungszentrum in den USA, und verwendete dabei neben ihrem soziologischen Zugang vor allem auch anthropologische und ethnographische Methoden, um Interaktionen der Wissenschafter:innen und ihre Prozesse zur Herstellung ihrer Ergebnisse zu untersuchen. Daraus resultierte 1981 ihr breit rezipiertes und vielfach zitiertes Werk "The Manufacture of Knowledge: An Essay on the Constructivist and Contextual Nature of Science".

Von 1981 bis 2001 wirkte sie vor allem an der Universität Bielefeld, zuerst noch als Privatdozentin, ab 1983 als Professorin für Soziologie. In diesem Rahmen entstand ihr zweites größeres Werk: "Epistemic Cultures: How the Sciences Make Knowledge" (Knorr Cetina 1999). 2001-2010 war sie, bis zur Emeritierung, Professorin für Soziologie an der Universität Konstanz. Seit 2004 ist sie auch noch Gastprofessorin an der University of Chicago. Ab 2000 hat sich ihr Fokus dann mehr auf die Soziologie globaler Finanzsysteme verlagert.

Für die STS ist ihr initialer Beitrag vor allem in der Anwendung enthnographischer Methoden bei der Beobachtung und Analyse wissenschaftlicher Forschungseinrichtungen zu sehen, der sie neben den nächsten Beiden Akteuren, Bruno Latour & Steven Woolgar, zu einer der einflussreichsten Autor:innen der Laboratory Studies machte - einem frühen Standbein der STS im Allgemeinen.

Der Fokus auf Labors als Orte wissenschaftlicher Tätigkeit ist nach Knorr Cetina unter anderem sehr spannend, weil Labors aus folgenden Gründen "epistemically advantageous for the pursuit of science" sind (ibid, p. 27, Hervorhebungen im Original):
* "it does not need to put up with an object _as it is_"
* "it does not need to accommodate the natural object _where it is_"
* "a laboratory science need not accommodate an event _when it happens_"

Das bedeutet in weiterer Folge aber auch, dass durch Laboratorien eine "enculturation" von natürlichen Objekten vorgenommen wird, in der natürliche mit sozialen Ordnungen abgeglichen werden, und dabei werden "reconfigured, workable objects in relation to agents of a given time and place" kreiert. (ibid, p. 29) Und das ist bereits die erste Voraussetzung für die soziale Bedingtheit wissenschaftlichen Wissens.

Ohne an dieser Stelle zu weit in die Tiefe gehen zu können, arbeitet Knorr Cetina noch eine Menge weiterer Faktoren heraus, aufgrund derer wissenschaftliche Erkenntnis immer kontextgebunden ist und von sozialen Konstruktionen und Kontingenz abhängig ist. Ein besonderer Faktor, den ich aber nur noch erwähnen möchte ist die opportunistische Logik von Forschung, die - stark heruntergebrochen - tendentiell immer jenen Möglichkeiten nachgeht, für die eben auch gerade die Ressourcen und sozialen Konstellationen zur Verfügung stehen (vgl. Knorr-Cetina 1981, pp. 50).

### Bruno Latour & Steve Woolgar

Bereits als zentrale Akteure der Laboratory Studies erwähnt haben wir Bruno Latour, ein französischer Philosoph/Anthropologe/Soziologe (1947-2022), und Steve Woolgar, ein britischer Soziologe (geb. 1950). Kurz bevor Karin Knorr Cetina für ihr erstes Fellowship an der University of California, Berkeley antrat, sind die beiden auch in Kalifornien gelandet um am Salk Institute in La Jolla/San Diego eine ethnographische Studie eines neuroendokrinologischen Forschungslabors vorzunehmen.

Daraus resultierte das 1979 erschienene Buch: "Laboratory Life : The Construction of Scientific Facts" (Latour and Woolgar 1986; spätere Ausgabe mit Postscriptum), in dem
anhand der Beobachtung der Interaktionen im Labor - dabei stark einzelnen Akteur:innen folgend - die teils kontingente und soziale Konstruktion der am Ende resultierenden wissenschaftlichen Fakten nachgezeichnet wurde.

Insbesondere von Bruno Latour sind auch spätere Labor-Untersuchungen breit rezipiert worden, so zum Beispiel das 1987 veröffentlichte "Science in Action: How to Follow Scientists and Engineers Through Society". (Latour 1987)

Ohne nun Pandoras Black Box (ibid) zu öffnen - auch aus Zeitgründen - sei an dieser Stelle nur erwähnt, dass vor allem Latour sehr stark einen Zugang etablierte, bei dem einzelnen Akteur:innen (wobei: meist waren es Akteure) im Labor gefolgt wird, und so alle weiteren Interaktionen analysiert und das Gesamtsystem abgebildet wird. Dies macht auch einen Unterschied zu Knorr Cetina aus, der es nicht so sehr um die individuellen "lab heroes" geht, sondern um die vermischten Strukturen von Labors und Wissenschaften, die sich um das herum formieren was wir dann als Experimente identifizieren können.

Latour etablierte auch die Begriffe _ready made science_ und _science in the making_. Während erstere die bereits abgeschlossenen wissenschaftlichen Artefakte bezeichnet, deren Herstellungsprozess für uns als black box verborgen ist, bezeichnet zweitere ein Sammelsurium an Aktivitäten, Kontroversen, Wettbewerben, Menschen bei der Arbeit und laufend sich entwickelnden Entscheidungen, welche wir sehen, wenn wir die black box öffnen - z.B. indem wir den Prozess im Labor beobachten.

### Madeleine Akrich

Angesichts der Zeit werden wir jetzt schneller und kürzer. Auf jeden Fall erwähnt werden muss aber aus meiner Sicht auch noch Madeleine Akrich, eine französische Techniksoziologin (geb. 1959), die unter anderem gemeinsam mit Bruno Latour, John Law, Michel Callon, und anderen die Actor-Network-Theory (kurz: ANT) entwickelt hat. Diese ist nicht nur, wie der Name suggeriert, eine Theorie, sondern auch ein methodischer Zugang, bzw. eine eigene sozialwissenschaftliche Schule, die sich ab den 1980ern im Rahmen der STS entwickelt hat und später auch in andere Disziplinen hineingreift.

Ganz knapp heruntergebrochen geht die ANT davon aus, dass jegliche soziotechnische und soziale Phänomene im Allgemeinen als ein Austausch in einem Netzwerk heterogener Entitäten (die Actors) erfasst werden können. Wobei die Grenzen dieser Netzwerk oft nicht stabil sind und die Actors manchmal auch als Actants bezeichnet werden, um hervorzuheben, dass damit nicht nur Menschen sondern auch technische Artefakte gemeint sein können.

Ein nennenswerter Beitrag von Akrich ist hier zum Beispiel auch ihr Beitrag "The De-Scription of Technological Objects" im 1992 von Jon Law und Wiebe Bijker herausgegebenen "Shaping Technology / Building Society. Studies in Sociotechnical Change." Darin beschreibt sie unter anderem folgende drei Komponenten bei der Untersuchung technologischer Objekte (Akrich 1992):
* "network" : Ein _network_ ist ein Set an Beziehungen zwischen _actants_ im Kontext eines technologischen Objekts. Auf diese Art kann es als Analysegegenstand verwendet werden, um soziotechnische Realitäten (oder Konstruktionen) zu untersuchen. Es wird durch alle _actants_ geformt, die irgendwie relevant für das Wirken des technologischen Artefakts sind - ob intendiert oder nicht. Das heißt, es besteht aus heterogenen Objekten und Subjekten.
* "actant" / "actor" : Ein _actant_ oder _actor_ ist ein Element innerhalb des untersuchten _network_. Es/sie/er/\* ist im Kontext des technologischen Objekts, das durch das _network_ beschrieben werden soll immer zu Teilen technisch und sozial. Durch eine _geography of responsibility_ sind _actants_ mit bestimmten Tasks und Beziehungen zu anderen _actants_ aufgeladen.
* "script" / "scenario" : Ein _script_ oder _scenario_ ist eine Art initialer Plan, den d: Designer:in einer Technolgie bzw. einem technologischen Objekt einschreibt ("inscribes"), basierend auf deren eigenen Ansichten und Intentionen.

### Helga Nowotny & Ulrike Felt

Nun gäbe es noch so viele weitere wichtige Akteur:innen in der Formierung der STS, für die wir an dieser Stelle leider keine Zeit mehr haben. Wir werden aber einige von ihnen im Laufe der folgenden Einheiten zu spezifischeren Themenfeldern noch näher kennenlernen. Zum Abschluss, und um auch wieder zurück nach Wien und ins Hier und Jetzt zu kommen, sind noch zwei Akteurinnen zu erwähnen: Helga Nowotny und Ulrike Felt, beide österreichische Soziologinnen bzw. spezifischer Wissenschafts- und Technikforscherinnen, die auch maßgeblich in Wien wirkten und wirken.

Helga Nowotny (geb. 1937) startete zuerst nach einem Doktorat in Rechtswissenschaften an der Uni Wien mit einer Assistenzprofessur am Institut für Kriminologie ihre wissenschaftliche Karriere. Über einen Umweg nach New York wechselte sie schließlich das Feld und schloß an der Columbia University in New York einen PhD in Soziologie ab - wo sie neben Paul Lazarsfeld auch den uns schon bekannten Robert Merton als Lehrer und Mentor kennenlernte. Nach einigen weiteren Stationen leitete sie zurück in Wien ab 1986 das an der Uni Wien frisch gegründete Institut für Wissenschaftstheorie und Wissenschaftsforschung, bevor sie 1995 an die ETH zu einer Professur in Wissenschaftsphilosophie und Wissenschaftsforschung berufen wurde. Neben vielen Tätigkeiten in verschiedenen Policy-Kontexten war sie unter anderem von 2001 bis 2006 Vorsitzende des European Research Council und ist auch nach wie vor in vielen europäischen Ländern in wissenschaftlichen Beratungsgremien engagiert.

Eine besonders nennenswerte Publikation von Nowotny ist das 2001 gemeinsam mit Peter Scott und Michael Gibbons herausgebrachte "Re­Thinking Science. Knowledge and the Public in an Age of Uncertainty". (Nowotny, Scott, and Gibbons 2001) Hier wird die von Nowotny bereits 1994 vorgenommene Unterscheidung zwischen einem _Mode 1_ und einem _Mode 2_ von Wissenschaft weiter ausgeführt. Während _mode 1 science_ die klassische Grundlagenforschung innerhalb einzelner Disziplinen beschreibt, wird mit _mode 2_ eine neue, inter- und manchmal transdisziplinäre Forschung beschrieben, die eher anwendungsorientiert und an spezifischen Problemlösungen orientiert ist, und in unmittelbarerem Austausch mit anderen gesellschaftlichen Prozessen steht. Dies wurde durch verschiedene Dynamiken am Ende des 20. Jahrhunderts hervorgerufen:

> "Science and technology have not been exempt from the process of democratization, at least in Western liberal democracies, which has stimulated the flow of media­-enhanced contestations and public controversies, and also articulated new demands for greater accountability and transparency in the science system." (ibid, p. 68)

Einen direkten Link zu Helga Nowotny hat nun Ulrike Felt (geb. 1957), die ursprünglich ein Doktorat in Physik an der Universität Wien absolvierte, und von 1983 bis 1988 am CERN in Genf Teil eines Forschungsprojekts war, in dem die sozialen, politischen und wissenschaftlichen Aspekte dieser ersten großen europäischen Forschungs-Infrastruktur untersucht wurden. In dieser Zeit orientierte sich Felt hin zur STS und kam so 1988 an das von Nowotny neu gegründete Institut für Wissenschaftstheorie und Wissenschaftsforschung. Seit 1999 ist sie an der Uni Wien auch als Professorin für Wissenschaftsforschung tätig und ist seit 2004 (mit Unterbrechungen) die Leiterin des damals neu gegründeten Department of Science and Technology Studies an der Uni Wien. Neben einem board membership in der 4S von 2002 bis 2004, sowie der Tätigkeit als Präsidentin der EASST von 2017 bis 2021, war sie u.a. von 2002 bis 2007 Editorin des Journals _Science, Technology, & Human Values_ (dem peer-reviewed Journal der 4S und damit einem zentralen Journal für das Feld der STS).

Während ein ursprünglicher Fokus auf die Veränderung der Modi von Wissenschaftsforschung auch in der gemeinsamen Arbeit mit Helga Nowotny begründet ist, forscht Ulrike Felt seither auch unter anderem in den Bereichen Public Engangement in Science, Partizipation in und Governance von Wissenschaft und Technik, Public Health & Health Care, Responsible Research & Innovation. Zwei besonders nennenswerte Begriffe / Konzepte, die sie geprägt hat sind _epistemic living spaces_ und _technopolitical cultures_.

Im Sinne des Zeitbudgets können wir nun auf diese Konzepte nicht im Detail eingehen, einiges davon wird uns aber im Laufe der Lehrveranstaltung begleiten. Im Sinne der full disclosure möchte ich hier auch gleich anmerken, dass ich meine MA thesis im Feld der STS bei Ulrike Felt geschrieben habe (Klaura 2014). Entsprechend dürften hier einige Einflüsse auch weiterwirken.

### Inhaltliche Zugänge und Varianten

Jetzt haben wir die Etablierung der STS entlang verschiedener Akteur:innen im Feld schematisch nachvollzogen. Klar ist, dass hier in der Kürze viele Auslassungen vorgenommen werden mussten. Einerseits gibt es noch viele weitere wichtige Akteur:innen und Wegbereiter:innen, andererseits hätten wir uns die STS auch mehr anhand ihrer unterschiedlichen Schwerpunktsetzungen und inhaltlich-methodischer Zugänge anschauen können, zum Beispiel der bereits erwähnten _Laboratory studies_ oder der diesem Umfeld entwachsenen _ANT (Actor-Network-Theory)_. Oder, um noch eine andere Schule zu nennen: _SCOT (Social Construction of Technology)_, die vor allem um Wiebe Bijker und Trevor Pinch herum entstanden ist und einen stark sozialkonstruktivistischen Zugang darstellt.

An dieser Stelle beenden wir aber unsere allgemeine Betrachtung der STS, und werden uns in der folgenden Sessions spezifisch mit feministischen Technikforschungsvarianten auseinandersetzen.  Einiges von dem was bereits angeklungen ist, wird dort auch fortgesetzt und vertieft. Anderes wiederum lassen wir hier stehen. Bei mehr Interesse zur STS allgemein möchte ich aber auch die VO von Lisa Sigl unter dem Titel "Science-Technology-Society (STS)" empfehlen, die voraussichtlich im Wintersemester wieder stattfindet.

Abschließend möchte ich noch regionale institutionelle Akteur:innen im Bereich der STS nennen. Hier gibt die Auflistung des Netzwerks STS Austria einen guten Einblick: http://www.sts-austria.org/members-institutions/ . Und als Arbeitsgruppe aus dem IFZ (Interdisziplinäres Forschungszentrum für Technik, Arbeit und Kultur) in Graz entstanden gibt es noch die Queer STS, die sich queeren Perspektiven in und auf die STS widmet: https://queersts.com . Über weitere Institute, Forschungseinrichtungen, Verbände und Studiengänge in STS im deutschsprachigen Raum und Europa gibt der deutsche Wikipedia-Artikel zur Wissenschaftsforschung auch einen ausführlicheren Überblick: https://de.wikipedia.org/wiki/Wissenschaftsforschung


## Diskurs-Sequenz

### Talking-by-walking

In Dreiergruppen begeben sich die Teilnehmer:innen auf einen kurzen Spaziergang,
wobei jede Person 5min davon in der Sprecher:innenrolle ist. Es geht dabei darum,
Fragen zu sich selbst in Verbindung zu setzen und zugleich mit anderen in Austausch
zu treten. Das Gehen dient auch dazu längere Sprechpausen zuzulassen. Die anderen
beiden, die gerade nicht in der Sprecher:innenrolle sind hören zu, können aber
auch nachfragen und so den Austausch unterstützen. Folgende Fragen sollen dabei
als Leitfragen mit auf den Weg genommen werden:

- Was hat mich am Thema "Feministische Technikforschung" interessiert, um in diese LV zu kommen?
- Wie steht das mit meinem eigenen Studien-/Forschungs-Feld in Verbindung?
- Was möchte ich aus dieser LV für mein Studium mitnehmen?

### Plenumsdiskussion

Während die Walking-Gruppen langsam zurückkommen, sammeln wir anhand eines offenen
Slidos Fragen und Aspekte, die wir diskutieren wollen. Die verbleibende Zeit der
Diskurs-Sequenz nutzen wir schließlich für eine Plenumsdiskussion der gesammelten
Fragen und Aspekte.

### Online-Exkursion

Im Anschluss an die Diskurs-Sequenz schauen wir ab 18 Uhr live und remote zum Vortrag
_"Techno-Utopien zwischen Cyborg und Care — Was Zugang mit Solidarität und Feminismus zu tun hat"_
von Katta Spiel, im Rahmen der Lecture Series der [Österreichischen Gesellschaft für Geschlechterforschung](https://www.oeggf.at).
Details dazu auch im TUWEL-Kurs.

## Vorbereitung auf Session 4

- Zeitungsartikel:
  - The Guardian (7. Feber 2023), Mark Z Jacobson: [We don’t need ‘miracle’ technologies to fix the climate. We have the tools now](https://www.theguardian.com/commentisfree/2023/feb/07/climate-crisis-miracle-technology-wind-water-solar)  (~7 min)
  - derStandard (27. Jänner 2023), Kommentar der Anderen: Georg Gratzer, Patrick Scherhaufer, Reinhard Steurer: [Klimakrise: Das überholte Mantra von einer politisch indifferenten Wissenschaft](https://www.derstandard.at/story/2000142950933/klimakrise-das-ueberholte-mantra-von-einer-politisch-indifferenten-wissenschaft) (~5 min)
- Eines der folgenden beiden Kapitel aus Invisible Women:
  - Can Snow-Clearing be Sexist? (pp. 29-46) (~30min)
  - Gender Neutral With Urinals (pp. 47-66) (~36min)
    - Trigger-Warnung für dieses Kapitel: viele Beschreibungen sexualiserter Gewalt
- Details im TUWEL-Kurs

## Bonus Challenge 1 (2 Punkte)

- Poste den Link und eine Erklärung zu einem aufschlussreichen Video, oder Podcast/Audiobeitrag oder einer kompakt geschriebenen Einführung, die einen Einblick oder Überblick in die STS oder einen ihrer Teilbereiche gibt
- Wikipedia-Artikel ausgenommen: das sollten wir bereits als Standardreferenzmaterial voraussetzen
- nicht nur Link, sondern kurze Erklärung wieso ihr es gut findet
- starts now, no deadline, aber je früher desto besser für alle
- Details im TUWEL-Kurs

## Checkout

- Ranking poll: Wie gut war der flipped classroom? Wollt ihr in Zukunft lieber eher wieder live Vortragsteile von mir, oder diese vorab lesen?
  - Antwort 1: Lieber selber vorab (mehr) lesen, und dafür in der Session mehr diskutieren.
  - Antwort 2: Lieber in der Session einen Vortragsteil und die Diskussion eher auf die zweite Semesterhälfte verlagern.
- Rating poll: Wie sehr habe ich nun das Gefühl zu verstehen, was die STS macht?
- Rating poll: Wie viele Anknüpfungsmöglichkeiten für STS-Forschung in meinem eigenen Feld sehe ich?
- Word cloud: Mit welchem Gefühl gehe ich nun aus der LV und in den Abend?

## Referenzen

Akrich, Madeleine. 1992. “The De-Scription of Technical Objects.” In Shaping Technology / Building Society: Studies in Sociotechnical Change, edited by Wiebe E. Bijker and John Law, 205–24. Brooks/Cole.

Klaura, Andrea Ida Malkah. 2014. “Computer Scientists & Their Publics. On Constructions of ‘participation’ and ‘Publics’ in Participatory Design and Research.” Master Thesis, Vienna (Austria): University of Vienna. http://othes.univie.ac.at/34851.

Knorr-Cetina, Karin. 1981. The Manufacture of Knowledge: An Essay on the Constructivist and Contextual Nature of Science. Oxford [u.a.]: Pergamon Press.

Knorr Cetina, Karin. 1999. Epistemic Cultures: How the Sciences Make Knowledge. Harvard University Press.

Kuhn, Thomas Samuel. 1996. The Structure of Scientific Revolutions. 3rd ed. The University of Chicago Press, Chicago. 1st Edition 1962.

Kuhn, Thomas S. 2012. The Structure of Scientific Revolutions. 4. ed., 50th anniversary ed. Chicago, Ill: Univ. of Chicago Press.

Latour, Bruno, and Steve Woolgar. 1986. Laboratory Life : The Construction of Scientific Facts ; with a New Postscript and Index by the Authors. New York: Princeton University Press.

Latour, Bruno. 1987. Science in Action: How to Follow Scientists and Engineers through Society. Harvard University Press.

Nowotny, Helga, Peter Scott, and Michael Gibbons. 2001. Re-Thinking Science: Knowledge and the Public in an Age of Uncertainty. Polity.
