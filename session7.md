# Session 7 - Zwischenreflexion & Diskussion

Diese Session verwenden wir vor allem um unsere Diskussionen aus den letzten Sessions fortzuführen. Außerdem wird damit ein expliziter Platz für eine Reflexion der bisherigen LV und der Auseinandersetzungen damit und darin geschaffen. Zudem wollen wir über Methoden und Methodologien sprechen. Es gibt daher keine dezidierte Input-Sequenz und keine entsprechenden Folien. Diese Seite dokumentiert den geplanten Ablauf der Einheit.

## Aktiver Teilnahmecheck

Im TUWEL (Moodle) steht ein Forum mit folgender Aufgabe bereit:

Da diese Session im Zeichen einer Zwischenreflexion über den Kurs steht, soll die Frage diesmal ein bisschen darauf abzielen die vergangenen Einheiten revue passieren zu lassen. Wir dafür auch nochmal einen Blick auf die Inhalte aller bisherigen Sessions auf der Kurs-Website: https://tantemalkah.at/feministische-technikforschung/2024/

Welche der bisherigen Sessions war für dich persönlich die beste/hilfreichste/aufschlussreichste/*. Und wieso? Was hebt die Session (in deiner konkreten Situation) von den anderen ab. Das Kriterium, das du hier heranziehst kann jetzt freilich sehr subjektiv gewählt werden. Also z.B. eher obs darum ging in welcher Session du am muntersten warst (weil sie vielleicht die interaktivste oder spannendste war) oder in welcher für dich einfach am meisten neue Inhalte vorkamen, oder nach welcher Session du noch am meisten nachzudenken hattest, etc.

Wie immer gilt, es braucht hier keine Abhandlung. Viel eher geht es um einen spontanen Gedanken, der einer Momentaufnahme entspricht (und vielleicht am näxten Tag auch schon wieder ganz anders formuliert werden würde). Es soll nur für alle anderen Lesenden grob nachvollziehbar sein, was das Relevante an dieser Session für dich war und wieso.

Bitte poste deinen Beitrag als Antwort auf die bereits bestehenden Threads zu den einzelnen Sessions.

## Stille Diskussion
> Geplante Zeit: 15 min

Während die Teilnehmer:innen zu Beginn nach und nach eintrudeln, werden auf drei
Flip-Charts im Raum zu den folgenden Themen Gedanken gesammelt:

- Grundvoraussetzung für feministische Technikforschung
- Alternativen zur feministischen Technikforschung
- kontroverseste Themen in der LV

Dabei können auch schriftliche Nachfragen oder Kommentare zu bereits anderen
niedergeschriebenen Gedanken festgehalten werden und so eine Diskussion am
Papier geführt werden.

## Kleingruppen-Arbeit: aktive Teilnahmen revue passieren lassen
> Geplante Zeit: 45 min

In 5 Kleingruppen werden die aktiven Teilnahmebeiträge aus den
vergangenen Sessions zusammengefasst, diskutiert, und die Ergebnisse auf einem
Flip Chart festgehalten und am Ende dem Plenum vorgestellt:

- S6: Your (scientific) understanding of publics
- S5: Gedanken zu feministischen Epistemologie-Texten
- S4: Einstieg feministische Technikforschung
- S3: Wo kommt so etwas wie STS im eigenen Studium vor?
- S2: Eingangs-Vorstellung zu Wissenschafts- & Technikforschung

Geplanter Zeitablauf:
- 15 Minuten Recherche/Nachlese
- 15 Minuten Diskussion & Plakatgestaltung
- 15 Minuten Vorstellen im Plenum

## Plenumsdiskussion
> Geplante Zeit: 25 min

Im Anschluss diskutieren wir die Plakate und die daraus resultierenden Fragen im Plenum.

## Vorbereitung & Checkout
> Geplante Zeit: 5 min

Kurzer Hinweis auf die ersten Schreibaufgabe, die noch vor der nächsten Session fällig ist.

Bis zur nächsten Session sind folgende zwei Texte zu lesen:

- Folgendes Kapitel aus Invisible Women: The Myth of Meritocracy (~35min)
- Endler, Rebekka. 2021. “Sprachkonstrukte.” In Das Patriarchat der Dinge: warum die Welt Frauen nicht passt, 19–39. Köln: DuMont. (~30min)
- Details im TUWEL-Kurs

Und zum Schluss - nachdem wir nun vom Hörsaal in den Seminarraum gewechselt haben -
nutzen wir diesmal den analogen Platz für ein soziogrammatisches Stimmungsbarometer
anstatt unserer klassischen Slido-Checkouts.
