# Session 1 - Intro & Vorbesprechung

> _Die begleitenden Slides gibt es hier: [slides/session1.html](https://tantemalkah.at/feministische-technikforschung/2025/slides/session1.html)_


Willkommen zur Vorbesprechung der VU Feministische Technikforschung. Was wir heute machen werden:

* Vorstellung LV-Leiter:in
* Kennenlernen der Teilnehmer:innen
* Vorstellung der LV-Themen
* Abklärung des Rahmens und der Leistungsbeurteilung
* Vorbereitung auf Session 2
* Checkout

## Vorstellung LV-Leiter:in

Also erstmal zu meiner Person und wie ich dazu komme, diese Lehrveranstaltung hier an der TU zu halten. Mein Name ist jackie. Also, mein Alltagsname. So kennen mich so gut wie alle Menschen mit denen ich auch tatsächlich schon interagiert habe. Wenn ihr mich aber im Personenverzeichnis der TU, oder generell online finden wollt, dann kommt ihr mit meinem bürgerlichen Namen eher weiter. Dieser lautet Andrea Ida Malkah Klaura. Ich mag aber auch diese Namen, zumal ich sie mir großteils selbst ausgesucht habe. Ihr könnt mich also mit jackie, gern aber auch mit Andrea, Ida oder Malkah ansprechen. Meine kognitiv-motorischen Feedbackschleifen sind allerdings auf jackie getuned. Vielleicht bin ich daher bei den anderen Namen nicht immer ganz so schnell auch auf die Ansprache zu reagieren.

Gut, Name geklärt, fängt ja schon mal kompliziert an. Dann ist da noch diese Sache mit dem Pronomen. Am liebsten, aber auch am ungewohntesten ist mir wenn ihr ze/hir verwendet, das funktioniert auch im Englischen. Wie funktioniert das? Dafür gibts zum Beispiel hier - in dem Fall fürs Englische - ein paar Beispiele:

> https://my.geeky.gay/pronouns/ze/hir/hirs/hirself

Allerdings bewegen wir uns ja oft in sehr unterschiedlichen Räumen, und im Alltag oft in einem Zweigeschlechterraum. In diesem Zweigeschlechterraum bevorzuge ich als Pronomen sie/ihr, bzw. im Englischen she/her. Das entspricht meinem legal gender, aber auch dieses ist selbst gewählt. Mensch muss halt dazusagen, dass eins in Österreich in den meisten Fällen nur aus zwei legal genders auswählen darf. (Aktuell scheint die [Genderklage](https://www.genderklage.at/) von [Venib](https://venib.at/) aber gerade einige Erfolge einzufahren) Für mich passt das aber auch ganz ok. Dennoch freue ich mich über alle, die hin und wieder auch aus dem Zweigeschlechterraum ausbrechen und die unendlichen Weiten des Genderraums erforschen wollen.

Oder wenn ihr einen spielerischen Zugang zu persönlicher Entwicklung bevorzugt, hier finde ich folgendes Zitat ganz nett, das ich im Fediverse gefunden habe:

> "That gender you got at birth? That's just a tutorial gender. You're only supposed to use it to get the hang on this world's gender system and then ditch it for a stronger one, not use it your entire life, noob"
> Source: [Mastodon: kirin@monads.online , January 14, 2021](https://monads.online/@kirin/105555650793872520)

Ok, gut, Pronomen auch geklärt. Aber wieso bin ich hier? Was ist mein scientific background um an der TU Wien eine VU zu Feministischer Technikforschung zu unterrichten? Ich fürchte, es wird nicht weniger kompliziert.

Bevor ich selbst im Jahr 2002 zu studieren begonnen habe, habe ich eine HTL für Technische Informatik & Internet Engineering in Klagenfurt besucht. Für alle, die bislang ein anderes als das österreichische Bildungssystem genießen konnten: HTL steht für Höhere Technische Lehranstalt, das ist eine 5-jährige Ausbildung in der Sekundarstufe II die mit einer Matura, dem Pendant zum Abitur, abschließt. Ich wollte danach unbedingt an die Uni, um meinen Horizont zu erweitern, und dachte mir das geht doch am Besten indem ich Philosophie inskribiere. Das ging dann so das erste Jahr mäßig gut. Es war nicht unspannend, aber auch nicht ganz das was ich mir gedacht habe. Und ich war ja mit dem Computerkram schon ganz gut, und spannend war's ja schon auch. Also hab ich dann im zweiten Jahr an der TU Wien das damals noch bakkfrische (sic!) Bachelorstudium Technische Informatik inskribiert. Das habe ich dann auch 6 Jahre später (sic!), 2009, abgeschlossen. Ich dachte mir einfach diese Mindeststudienzeit von 6 Semestern sollten einer Regelstudienzeit von 6 Jahren entsprechen. Außerdem hatte ich ja noch nebenbei mein Philosophiestudium und ne ganze Menge anderer Projekte, wie z.B. selbstorganisierte Tutorien und Lehrveranstaltungen, ganz ohne Schein, aber mit Substanz. Und obwohl ich in der glücklichen Lage war Studienbeihilfe zu beziehen, reichte das auch nicht immer, und da musste dann nebenbei noch Lohnarbeit geleistet werden.

Als ich den BSc in Technischer Informatik fertig hatte, wollte ich doch wieder ein bisschen mehr unendliche Weiten erforschen als das innerhalb der Informatik möglich ist. Und da kam mir das eben 2009 eingeführte Masterstudium Science - Technology - Society an der Uni Wien ganz gelegen. Also habe ich einfach das gemacht, und habe mir aber die Technik, und insbesondere die Informatik quasi ein bisschen von außen angeschaut. Für theoretische und praktische Handlungsanleitungen, aber auch als Grundhaltung im Zugang zu Technik und Gesellschaft habe ich mich dazu an der feministischen Technikforschung orientiert.

Nun ist das aber auch schon ein Weilchen her, meine master thesis konnte ich - nach langen Prokrastinationspausen und Phasen des Selbstzweifels - dann endlich 2014 abgeben. Danach habe ich erstmal weiter im Bereich IT Operations gelohnarbeitet, bin anderen Projekten nachgegangen und hab mich eher als academic droput gefühlt. Allerdings wird mir oft auch zu schnell wieder langweilig, darum habe ich dann 2015/16 für zwei Semester einen kurzen Ausflug zurück an die TU gemacht und einige LVs im (damaligen) Masterstudium Informatikdidaktik / Educational Technologies absolviert. Irgendwie ging sich das aber dann alles nicht so gut aus, und 2017 hat es mich dann an die FH Technikum verschlagen um dort berufsbegleitend einen Master in IT Security zu absolvieren.

Während dieser Zeit habe ich mich sonst vor allem bei Radio Orange, dem Community Radio in Wien darum gekümmert die IT-Infrastruktur am Laufen zu halten und, sofern das mit den begrenzten Mitteln überhaupt möglich war, neue Tools zu entwickeln. Das hat mich einerseits in die IT Security verschlagen, weil es einen sehr umfassenden Blick auf IT als soziotechnisches System erlaubt, und gerade Menschen (und deren Einbindung) hier auch eine große Rolle spielen. Andererseits hat es mich so etwas von IT Operations weg und zu Softwareentwicklung bzw. Web Engineering hingebracht. Was mich schließlich im Pandemieherbst 2020 an die Angewandte, die Universität für Angewandte Kunst in Wien verschlagen hat. Dort bin ich nun als Backend & DevOps Engineer angestellt und entwickle und konzipiere moderne Webanwendungen für die Angewandte und ihre Projektpartner:innen. Bzw. potentiell für alle interessierten Universitäten, da wir nach einem Open Source Prinzip entwickeln. In den letzten zwei Jahren war ich da primär mit Portfolio/Showroom, einem Open Source CRIS (Current Research Information System) für (vor allem) Kunstuniversitäten beschäftigt, nun verlagert sich der Schwerpunkt auf die Weiterentwicklung unserer Platform for Open Art Education. So hat es sich auch ergeben, dass ich dort am Coding Lab seit 2021 eine zweiteilige LV unter dem Titel "Web Coding: web-based game development" unterrichte, wo die coding basics zur Entwicklung web-basierter digitaler Spiele vermittelt werden, sowie seit 2022 eine Übung zu Machine Learning. Und seit 2023 bin ich nun also auch als externe Lektorin an der TU Wien für diese Lehrveranstaltung tätig.

Aber gut, long story short, ich bin jetzt mit vielen Umwegen irgendwie auch wieder zurück an der TU Wien gelandet. Aber wieso gerade zu diesem Thema? Da hat sich ja seit 2014 nicht mehr viel getan. Jein. Zum einen würde ich sagen, ja, ich bin ein bisschen aus der feministischen Technikforschung raus und dafür in eine feministische Technikpraxis rein. Dazu gehört unter anderem auch das Organisieren von Workshops und Meetups, wie z.B. das [feminist python meetup](https://feminist-python.diebin.at/) oder das [feminist linux meetup](https://feminist-linux.diebin.at/). Zum anderen habe ich aber zwischendurch doch auch noch relativ nahe am Thema dran an Unis unterrichtet. Mit Boka En gemeinsam haben wir die [LV Hacking Gender, Hacking Technology](https://hacking-gendertech.diebin.at/) einmal an der Alpen-Adria-Universität Klagenfurt (2019) und der Universität für Bildende Kunst Wien (2017/18) gehalten. Damit haben wir auch einen Versuch gestartet feministische Technikforschung und queere Gechlechterforschung mit praktischer Technikentwicklung in Austausch zu bringen.

Und diese vielen unterschiedlichen Verwebungen und Verflechtungen in meinem Tun haben nun dazu geführt, dass mich die Abteilung Genderkompetenz für den neuen LV-Katalog zum Zertifikat Gender- und Diversitätskompetenz angefragt hat, ob ich denn eine LV zu Feministischer Technikforschung anbieten kann. Und da habe ich erstmal sehr begeistert, und im zweiten Nachdenken auch ein bisschen mit Unsicherheit zugesagt. Nachdem ich jetzt gefühlt auch viel mehr in der technischen Praxis war, wird diese LV also auch für mich eine neue (Wieder-)Auseinandersetzung mit dem Thema. Aber ich denke, das sind ohnehin die besten Voraussetzungen um gemeinsam nicht nur Wissen zu entdecken sondern auch zu rekonfigurieren oder zu produzieren.

Nun sind wir also alle hier. Und ihr habt jetzt hoffentlich ein bisschen einen Eindruck davon welche Erfahrungshintergründe ich so in die LV mitbringe. Bevor wir uns anschauen, was die Themen der LV sind, würde ich nun gern aber auch etwas über euch wissen. Bzw. ist das vielleicht ja auch für euch spannend, wer hier sonst noch so an der LV teilnimmt.


## Kennenlernen der Teilnehmer:innen

An sich würde ich an dieser Stelle gerne mit allen gemeinsam ein paar Live-Soziogramme im Raum stellen. Da wir aber doch zu viele und in einem dafür nicht so geeigneten Raum sind - und sich die digitale Variante mit Excalidraw auch als nicht so Handy-kompatibel erwiesen hat - nähern wir diese Übung mit einem Slido-Poll mit folgenden Fragen an (die Ergebnisse stelle ich dann im TUWEL für alle zur Verfügung):

* Wie viele Semester studierst du schon?
* Wie alt bist du?
* Welches Studium studierst du?
* Wo wohnst du zur Zeit?
* In welchem Ort wurdest du geboren?
* Anzahl an Geschwistern
* Ungefähre Anzahl an Büchern im Haushalt in dem du aufgewachsen bist
* Anzahl an Freund:innen, die auch an einer Hochschule studieren oder studiert haben
* Erdnussbutter oder Marmelade?
* Wo verortest du dich im Geschlechterraum?
* Wie wohl fühlst du dich mit Gruppen-Interaktionen im Rahmen deines Studiums?
* Habe bereits die VO Männlichkeiten & Technologien besucht
* Habe bereits die VO/UE Science - Technology - Society besucht
* Anzahl an anderen bereits besuchten LVs aus dem Bereich "Gender- und Diversitätskompetenz"


## Vorstellung der LV-Themen

Das Ziel der Lehrveranstaltung ist, einen ersten Einblick und Einstieg in die feministische Technikforschung zu geben. Klarerweise kann eine einzelne VU mit 3 ECTS hier nicht alle Aspekte abdecken, die in diesen Themenbereich fallen. Immerhin ist die feministische Technikforschung ja eigentlich auf die gesamte Technikforschung an sich anzuwenden, nur eben mit der Fundierung von feministischer Theorie und Praxis. Genaugenommen müssten wir hier also nicht nur die Wissenschafts- & Technikforschung, sondern auch feministische Theorie(n) und Methoden erarbeiten.

Nun gibt es aber zum Glück auf der TU Wien bereits einige andere Lehrveranstaltungen, die einiges davon abdecken. Zum Beispiel gibt Lisa Sigl eine Vorlesung im Winteresemester (mit anschließender Übung im Sommersemester) unter dem Titel _Who cares? Soziale Verantwortung in Wissenschaft und Technologieentwicklung_, in der vor dem Hintergrund der Wissenschafts- und Technikforschung ein Überblick über Themen und Zugänge zu soziotechnischen Denkweisen erarbeitet werden. Außerdem gibt es dieses Semester auch noch die Vorlesung _Technik und Gender, Grundlagenvorlesung für IngenieurwissenschafterInnen_ von Bente Knoll, in der gender- und geschlechterbasierte Perspektiven in und auf die Technik behandelt werden und so auch grundlegende feministische Perspektiven zur Auseinandersetzung mit Technik vermittelt werden. Und dann gibt es auch noch die Vorlesung _Männlichkeiten und Technologien_ von Stefan Sulzenbacher (ebenso mit anschließender Übung im Sommersemester), in der ein sehr wichtiger Aspekt der (feministischen) Technikforschung in den Blick genommen wird: wie sich Männlichkeiten und Technologien koproduzieren, und so auch auf gesellschaftliche (Denk-)Möglichkeiten einwirken und bestimmte Normsetzung oft auch vermeintlich naturalisieren. Zum anderen werden auch Möglichkeiten aufgezeigt wie kritische Männlichkeitsrekonfigurationen einen Beitrag zu einer "inklusiveren, achtsameren und nachhaltigeren Gestaltung" von Technik beitragen können.

Und dann gibt es noch einige weitere Lehrveranstaltungen aus dem Katalog für das Zusatzzertifikat Gender- und Diversitätskompetenz, aber auch an den einzelnen Fakultäten, die den einen oder anderen Aspekt behandeln, der auch für unsere Auseinandersetzung mit der feministischen Technikforschung sehr relevant ist.

Für alle, die sich also umfassender mit diesem Themenbereich auseinandersetzen, wird es hier einige Redundanzen geben. Aber das ist auch ganz gut. Denn am Ende soll es ja nicht darum gehen noch mehr, noch genauer, noch umfassender (analog zu einem größer, schneller, stärker - bestimmte Männlichkeiten lassen hier grüßen) an einem spezifischen Detail zu arbeiten, sondern die Verknüpfungen und Verflechtungen der verschiedenen Themen und soziotechnischen Konfigurationen zu begreifen. Und das ist in seiner Komplexität nur in einem vergemeinschafteten Austausch möglich. Und dieser erfordert eben auch bestimmte Positionen immer wieder neu durchzudenken und dabei noch andere Perspektiven miteinzubeziehen.

Zum anderen kann ich auch gar nicht alle fachspezifischen Bereiche alleine so gut abdecken, da ich selbst ja auch fachlich und praktisch verortet bin. Es wird daher einige Themen geben, die auf den ersten Blick sehr viel mit meinem informatischen Hintergrund zu tun haben. Da ist bestimmt auch was dran. Zum Anderen ist ein besonderes Kennzeichen der Technowissenschaften heute, dass sie nicht mehr ohne informationstechnische Infrastrukturen betrieben werden können. Die Aspekte die wir hier an der Informatik untersuchen können schreiben sich daher auch in andere Wissenschaften ein und beeinflussen deren Produktion.

Außerdem seid ihr selber mehr Expert:innen des eigenen Felds, als ich das sein könnte. Daher werden wir vor allem auch den Übungsteil nutzen, um auch konkrete Beispiele feministischer Technikforschung in den vielen unterschiedlichen Feldern zu beleuchten, die den Fakultäten der TU Wien zugeordnet werden können.

Nun aber zu den konkreten Inhalten, wie ihr sie auch bereits im Lehrveranstaltungsverzeichnis finden konntet:

1. Wissenschafts- & Technikforschung
2. Feministische Technikforschung
3. Public Engagement, Partizipation & Demokratisierung von Technowissenschaft
4. Binary impositions on a fuzzy world
5. Diversity & Unconscious Bias
6. Methoden der feministischen Technikforschung
7. Fallbeispiele aus den Fachbereichen
8. Abschluss: Gute Technik und Schönes Leben für Alle!

Darüber hinaus gibt es noch einige Themen wie Sustainability, Barrierefreiheit, Open Science, Privacy & Netzpolitik, die wir an unterschiedlichen Stellen noch miteinflechten können (und die in vielen technofeministischen Diskursen immer auch mit eingeflochten sind). Hier gibt es in der zweiten Semesterhälfte dann noch etwas Spielraum für weitere Fokussierungen. Für die oben gelisteten Themen wird es aber auf jeden Fall bereits vorbereiteten Input geben.

## Abklärung des Rahmens und der Leistungsbeurteilung

Um kurz auf den formalen Rahmen einzugehen:

* 3 ECTS := 75h Arbeitsaufwand
* diese sind wie in der LV-Beschreibung angeführt wie folgt aufgeteilt:
	* 25h Teilnahme an LV-Sessions
	* 20h Selbststudium, Reflexion & Vertiefung der Inhalte aus den LV-Sessions
	* 10h Gruppendiskussionen und -reflexionen außerhalb der LV-Sessions
	* 20h Planung & Durchführung & Präsentation einer kleinen Fallstudie

Individuell können die tatsächlichen Arbeitsaufwände je nach Inhalt und Phase einer LV sehr stark schwanken. Auch wird der Gesamtaufwand individuell sehr unterschiedlich ausfallen. Mein Ziel ist es, dass alle mit dem kalkulierten Aufwand gut durchkommen können und auch die Chance auf eine gute Note haben. Das heißt auch, dass es für einige mit weniger Aufwand möglich sein wird eine gute Note zu bekommen. Das heißt auch, dass ihr formal nicht dafür belohnt werdet, wenn ihr euch hier extra ins Zeug legt. Ich freu mich selbstverständlich über alle, die ein grandioses Gruppenprojekt abliefern oder selbst auch noch über die im Kurs behandelte Literatur weitere Recherchen anstellen und in den Kurs einbringen. Mir ist aber wichtig, dass ihr im Hinterkopf behält, dass gerade vor dem Hintergrund der Wissenschafts- und Technikforschung so etwas wie "Exzellenz" höchst konstruiert ist, und die Frage wer "exzellente" Arbeit leisten kann sehr stark von unterschiedlichen Umweltbedingungen abhängig sind, die individuell nicht veränderbar sind. Ich setze also vor allem auch auf eure intrinsische Motivation euch mit dem Thema auseinander zu setzen und werde zugleich versuchen euch im Lauf der LV immer wieder auch daran zu erinnern auf eure study-life-balance zu achten.

Nun aber kurz dazu, was diese einzelnen Kategorien bedeuten:

**25h Teilnahme an LV-Sessions:** das ist relativ selbsterklärend, ich gehe davon aus, dass ihr an so vielen LV Sessions teilnehmt, wie es euch möglich ist. Und da wo ihr nicht teilnehmen könnt, müsst ihr euch außerhalb des Rahmens dennoch mit dem Inhalt der LV beschäftigen. Und während wir nur 12 Sessions zu 1,5h haben, gehe ich auch davon aus, dass ihr im Idealfall auch vor und nach der LV-Einheit noch unmittelbarer mit dieser Einheit und ihren Inhalten beschäftigt seid. Auch der Aufwand für diese jeweils 1,5h Dienstag abends hier teilzunehmen ist individuell unterschiedlich und geht immer über die reinen 1,5h Anwesenheitszeit hinaus.

**20h Selbststudium, Reflexion & Vertiefung der Inhalte aus den LV-Sessions:** in diese Kategorie fallen einerseits verschiedene Texte, die wir begleitend zur LV lesen werden, als auch euer Nachdenken über die LV und die Inhalte über die einzelnen Sessions hinausgehend. Zugleich werdet ihr am Ende auch einen individuellen Reflexionsbericht verfassen. Hier ist es auch sehr schwer ein "allgemeingültiges" Maß zu finden, und es wird auch sehr stark euch selbst obliegen einen Modus zu finden, indem ihr diese Aufgaben mit euren vorhandenen Zeitbudgets in Einklang bringt. Manche tun sich leichter oder schwerer beim Schreiben. Genauso beim Lesen. Texte können auch mit sehr unterschiedlichem Fokus gelesen werden. Ab und an kann auch einfach eine Zusammenfassung eines Textes schon genügend Einblicke geben um zumindest eine gute Ahnung vom eigentlichen Inhalt zu haben. Manche von euch wird der Inhalt der LV jeden Tag immer wieder einmal in den Kopf kommen, und ihr werdet Querverbindungen zu anderen Themen herstellen. Andere wiederum denken wirklich nur in vorab geplanten Timeslots über die behandelten Dinge nach. Die tatsächliche Gesamtzeit, die ihr über die Inhalte der LV reflektiert wäre aber in jedem Fall nicht so einfach quantifizierbar.

**10h Gruppendiskussionen und -reflexionen außerhalb der LV-Sessions:** vor allem auch als Vorbereitung auf euer Gruppenprojekt, aber auch um noch einmal einen anderen Reflexionsrahmen zu schaffen, werdet ihr in einer kleineren Gruppe, außerhalb der LV über die LV und ihre Inhalte nachdenken. Zum Teil werden diese Diskussionen auch Teil der kleinen Fallstudie werden, die ihr als Gruppenprojekt absolvieren werden.

**20h Planung & Durchführung & Präsentation einer kleinen Fallstudie:** hiermit ist die Zeit veranschlagt, die ihr für die Umsetzung einer kleinen Fallstudie verwenden werdet, in der ihr Inhalte und/oder Methoden der feministischen Technikforschung mit eurem Umfeld an der TU Wien in Austausch bringt. Wir werden die Details im Laufe der LV noch genauer klären. Wichtig ist mir hier zu betonen, dass dieses Projekt einen prototypischen Charakter hat.
Eine richtige Fallstudie wäre mit so einem begrenzten Zeitbudget nur sehr schwer umzusetzen. Bzw. wäre es eine besondere Schwierigkeit eine so gut eingegrenzte Forschungsfrage zu finden und zugleich schon so viel methodische Erfahrung mitzubringen, um in diesem Rahmen wirklich eine Fallstudie zu erarbeiten, deren Ergebnisse ihr auch allenorts gerne veröffentlichen würdet. Wenn sich so etwas ergibt, dann ist das ganz großartig. Das Ziel für die Lehrveranstaltung ist aber gewissermaßen nur ein erster Entwurf und Testlauf für eine solche potentielle Fallstudie. Ein Prototype eben, anhand dem ihr ausprobieren könnt ob eure Ideen und Methoden sinnvoll weitergesponnen werden können.
Entsprechend wird auch die Präsentation nicht unbedingt die Präsentation von vermeintlich finalen "Ergebnissen" sein. Das Ergebnis, das ihr präsentiert ist vielmehr euer Thema, eure Herangehensweise und eure Idee, was ihr mit den Ergebnissen - sofern ihr denn irgendwann so eine Fallstudie wirklich auch mit gebührenden Ressourcen im Hintergrund durchführen könnt - machen würdet. Ich nenne das eine Art Disseminationskonzept. Wie würdet ihr das, was ihr beforscht habt, bzw. eingehender beforschen wollen würdet, in Umlauf bringen, und vor allem wo. Im besten Fall ist euer Ergebnis also ein Template, mit dem ihr später nochmal ausführlicher an dieses (oder auch ein ähnliches) Thema herangehen und es beforschen könnt.

Jetzt haben wir erstmal grob skizziert, was wir in dieser LV tun werden und wie viel Aufwand das grob im Schnitt geschätzt sein soll. Jetzt gibt der formale Rahmen dieser VU an der TU Wien aber auch vor, dass am Ende ein Note vergeben werden muss. Das widerstrebt mir aus vielen Gründen. Siehe die bereits erwähnte Konstruktion von Exzellenz. Ebenso werden wir im Lauf der LV viele Themen anschneiden, die sichtbar machen wie Leistung und wissenschftliche oder technische Entwicklung auf individueller Ebene nicht klar quantifizierbar und determinierbar sind. Ja, es gibt allerlei Maßstäbe die hier angewandt werden können, aber keinen eindeutigen Referenzrahmen. Und in jedem Fall wird sich der angewandte Maßstab auf die sichtbare Leistung aller Teilnehmenden unterschiedlich auswirken - aufgrund vieler umweltbedingter Faktoren, die weder in der Verantwortung noch im Einflussbereich der individuellen Teilnehmenden liegt.

Mir geht es primär darum, dass ihr euch ernsthaft mit dem Thema auseinandersetzt und am Ende eine Entwicklung vollzogen habt - also inhaltlich nicht genau an dem Punkt steht wo ihr vorher wart. Das Ergebnis dessen wo ihr dann steht wird aber für alle sehr unterschiedlich sein. Und auch das Ausmaß der Entwicklung die euch möglich ist, kann nicht einheitlich bewertet werden. Mir wäre es also an sich lieber, wenn ich am Ende nur sagen müsste "hat positiv abgeschlossen" oder eben nicht. Nun muss ich aber ein Schema finden, in dem ihr möglichst transparent zu einer Note von 1 bis 5 kommen könnt. Und dabei möchte ich den Einfluss meiner eigenen Bewertung dessen, was eine gute Entwicklung ist, möglichst minimieren. Klar, ich werde bestimmte Favorit:innen im Kurs haben, und es wird andere geben, bei denen ich nicht ganz so überzeugt von ihren Ansätzen bin. Aber das ist mein individueller Maßstab, den ich jetzt gar nicht erst versuchen möchte objektiv oder fair zu reden. Klar ist auch, wenn am Ende eine Person "reflektiert", dass das hier "einfach alles nur Bullshit war", weil es doch ganz klar wäre, dass es nur diesen einen Maßstab geben kann, dann muss ich den Anteil meiner eigenen Bewertung rein aus fachlicher Perspektive vergrößern. Weil dann kann ich nicht davon ausgehen, dass die Person sich "ernsthaft mit dem Thema auseinandersetzt", obwohl sie vielleicht sogar eine Entwicklung vollzogen hat.

Mein Willkür-minimierender Vorschlag für ein entsprechendes Schema, das die oben angeführten Arbeitsaufwände am Ende in eine Note von 1 bis 5 überführt schaut nun also wie in der LV-Beschreibung bereits aufgelistet so aus:

Zur Erlangung eines Leistungsnachweises wird ein Punktesystem angewandt, in dem Teilnehmer:innen durch folgende Aktivitäten bis zu 100 Punkte und weitere 10 Bonuspunkte sammeln können:

* 25 durch aktive Teilnahme an LV-Sessions
* 15 durch Beantwortung offener Fragen in Form drei kleinerer Schreibaufgaben
* 30 durch individuellen Reflexionsbericht am Ende/nach der LV
* 30 durch Gruppenprojekt / kleine Fallstudie
* 10 mögliche Bonuspunkte durch Beisteuern von frei zugänglichen themenrelevanten Ressourcen

Die finale Note errechnet sich anhand des finalen Punktestandes wie folgt:

-   über 80 Punkte: 1
-   70-80 Punkte: 2
-   60-70 Punkte: 3
-   50-60 Punkte: 4
-   0-50 Punkte: 5

Alle Abgaben sowie auch das Tracking der Punkte wird dabei über den TUWEL-Kurs erledigt. In den meisten Fällen werde ich versuchen keine individuelle, qualitative Bewertung anzustellen. Das wäre auch rein organisatorisch und aufwandsmäßig für mich nicht machbar, nachdem ich auch nur ein begrenztes Zeitbudget zur Verfügung habe. Im Fall des Gruppenprojekts und der individuellen Reflexionsarbeit ist das ausschlaggebende Kriterium daher auch primär, ob die Arbeit gemacht wurde oder nicht. Dafür gibt es dann bereits jeweils 10 der 30 Punkte. Weitere 10 Punkte gibt es dann dafür, dass die dargelegten Ideen/Überlegungen konsistent und gut nachvollziehbar sind. Die finalen 10 Punkte gibt es im Fall der individuellen Reflexionsarbeit für das Herausarbeiten von spezifischen Querverbindungen zum eigenen Feld, sowie im Fall der Gruppenarbeit für gut dargelegte Disseminationskonzepte, die über klassisches Paper-Publishing & Conference-Talking hinausgehen.

## Vorbereitung auf Session 2

-   Zeitungsartikel:
	-  derStandard (10. Juni 2022), Kommentar der Anderen: Judith Kohlenberger: [Die engagierte Wissenschaft](https://www.derstandard.at/story/2000136429353/die-engagierte-wissenschaft) (~ 6min)
-   Buchkapitel:
	- Introduction. In: Criado Perez, Caroline. 2020. Invisible Women: Exposing Data Bias in a World Designed for Men. London: Vintage. (~45 min)
-   Details und Materialien finden sich im TUWEL-Kurs

## Checkout

Slido:
* Rating poll: Wie wahrscheinlich ist es nun, dass ich diese LV weiter besuche:
	* sicher nicht
	* wahrscheinlich eher nicht
	* ich bin mir noch unsicher
	* eher ja
	* fix oida! (sicher)
* Ranking poll: Welchen Aspekt verkörpert die Lehrperson für dich zur Zeit in welchem Ausmaß? (Mehrfachauswahl und Ranking möglich):
	* Wissenschafter:in
	* Techniker:in
	* Lehrperson
	* Sozialwissenschafter:in
	* Wissenschaftsforscher:in
	* Feminist:in
* Wordcloud: Mit welchem Gefühl gehe ich nun aus der LV und in den Abend?
