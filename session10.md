# Session 10 - Methoden & Projektthemen

## Aktiver Teilnahmecheck

Die aktive Teilnahme in Session 10 gestaltet sich nun etwas anders auf Basis
der Beiträge aus Session 9. Eure Aufgabe ist es nun einen der bestehenden
Posts von euren Kolleg:innen zu nehmen und darauf ein gutes Gegenargument
zu formulieren. Dabei kann sich dieses Gegenargument dann auch auf ein anderes
Gegenargument beziehen, dass schon jemensch anders gepostet hat.

Das muss jetzt auch nicht unbedingt eurer eigenen Perspektive entsprechen,
sondern kann ein Beitrag sein, wie bestimmte Methoden und Perspektiven
wiederum kritsich betrachtet werden können. Also ihr könnt hier quasi auch
eine:n super-postitivistische:n advocat: diaboli spielen, wenn euch das
leichter fällt. Oder ihr habt eh auch so einen guten Einwand.

Achtet nur in allen Fällen darauf, dabei nicht gegen d: Poster:in selbst,
sondern gegen den die Perspektive zu argumentieren - also von einer anderen
Perspektive, und anerkennend, dass es eben es diese Perspektive auch geben
kann, nur dass halt etwas daran auch kritisch zu sehen ist.

## Ankommen & Methodensammeln
> geplante Zeit: 15min

Während alle Teilnehmer:innen langsam im Seminarraum eintrudeln, sammeln und
clustern wir mit Post-Its alle möglichen relevanten Methoden, die uns bisher
untergekommen sind - auch mit Fokus darauf, was wir selbst in unseren eigenen
Projekten anwenden können.

## Projektteam-Soziogramm
> geplante Zeit: 5min

Anhand einer Aufstellung im Raum versuchen wir erstmal noch ganz unabhängig
vom möglichen Thema festzustellen welche fixen Teams es für das Abschlussprojekt
bereitsn gibt, wo sich eventuell schon mögliche Teams abzeichnen, und wer
noch gar keine Team-Preferenzen hat.

## Projektteam- & -themenfindung
> geplante Zeit: 25min

Jene fixen Teams, die sich im Soziogramm bereits abegzeichnet haben, nutzen
diese 25min um bei einem Talking-by-Walking, oder an einem anderen Ort
außerhalb des Seminarraums über ihr Thema bzw. mögliche Themen zu plaudern.
Das Ziel soll sein, danach zumindest eine erste vage Projektidee im Plenum
vorstellen zu können.

Alle anderen Teilnehmer:innen erstellen im Seminarraum eine Art
Themensoziogramm (5-10min), woraufhin wir danach vorläufige Kleingruppen
formulieren, die sich über mögliche Projektideen austauschen (15-20min).
Auch hier soll das Ziel sein, im Anschluss dem Plenum erste (vorläufige)
Projektideen vorzustellen (noch unabhängig davon, ob sich dazu auch schon
ein Team gefunden hat).

## Vorstellen der Projektgruppen & -ideen
> geplante Zeit: 20min

Die im vorigen Schritt erarbeiteten Ideen werden nun im Plenum vorgestellt.

## Sprachdebatte
> geplante Zeit: 30min

Spätestens wenn es um die Umsetzung eigener Forschungsprojekte geht, müssen wir
auch unsere Sprach- & Schreibgewohnheiten reflektieren. Wen wollen und sollen wir
dabei wie adressieren und welche Auswirkungen hat das auf Forschungs-Teilnehmende
(z.B. in partizipativen Prozessen, aber auch im eigenen Forschungsteam).

In einer der vergangen Einheiten haben wir bereits einen Text zu Sprachkonstrukten
gelesen, und es ist relativ klar, dass wir mit den bisher eingelernten Sprachvarianten
nicht immer die gewünschten Effekte erzielen wollen und nicht immer die eigentlich
intendierten Forschungssubjekte identifizieren (und womöglich auch affizieren) können.

Nun soll daher darüber diskutiert werden, wo es im eigenen Forschungsvorhaben wichtig
ist, auf solche Sprachkonstrukte zu achten, und wann/ob/wie eine ungegenderte oder
gegenderte (z.B. generisches Maskulinum oder inklusivere Varianten) sinnvoll oder gar
kontraproduktiv sind.

Die Diskussion wird in diesem Fall als _Ins & Outs Diskussion_ geführt. Die
Teilnehmer:innen sitzen dabei in einem Sesselkreis. Jede:r Teilnehmer:in hat dabei
(vorerst) nur zwei Wortmeldungen zur Verfügung. Nach der ersten Wortmeldung steht
d: Teilnehmer:in auf stellt sich hinter den eigenen Sessel. Nach der zweiten
Wortmeldung setzt sich d: Teilnehmer:in dann auf einen der Tische an der Wand des
Seminarraums. Erst wenn sich keine Teilnehmer:innen mehr im/am Sesselkreis befinden,
und noch genügend Diskussionszeit vorhanden ist, können alle wieder ihre Plätze
einnehmen und der Modus beginnt von vorne.

## Bonus Challenge 3

Um 1 Punkt für diese Challenge zu bekommen, poste einen Comic (mit Angabe der Quelle,
oder einfach einen Link zu einem Comic), der einen Aspekt der feministischen
Technikforschung (im weitesten Sinn) darstellt. Es kann also auch ein Comic sein,
der etwas thematisiert, das auch in der feministischen Technikforschung Thema ist.

Extra points: wer stattdessen einen eigenen Comic zum Thema erstellt (oder solch
einen bereits erstellt hat und diesen postet), erhält stattdessen 4 Punkte.

## Vorbereitung auf Session 11

Für die kommende Session, in der wir uns mit biolog(ist)ischen Geschlechtskonstruktionen auseinandersetzen
werden, ist folgender Text zu lesen (PDF wie üblich auch im TUWEL Kurs verfügbar):

* Fausto-Sterling, Anne. 2000. “The Five Sexes, Revisited.” The Sciences 40 (4): 18–23. https://doi.org/10.1002/j.2326-1951.2000.tb03504.x.
