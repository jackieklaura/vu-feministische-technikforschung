# Session 9 - Methodologie & Methodendiskussion

> _Diese Session ist krankheitsbedingt ausgefallen. Anbei findet sich die Dokumentation
> dessen, was ursprünglich geplant gewesen wäre. Die Texte zur Vorbereitung und die aktive Teilnahme
> werden allerdings weiterhin gelesen und geschrieben._

Anschließend an die Reflexion & Diskussion in der letzten Session, wollen wir auch diese Einheit vor allem diskursiv nutzen. Es geht nun aber hin zu methodologischen Ableitungen sowie das Studienumfeld im Speziellen. Dazu besprechen wir auch die Ergebnisse der ersten Moodle-Aufgabe, in der bereits einige Beispiel aus den unterschiedlichen Studienrichtungen anklingen, wo und wie feministische Technikforschung miteinbezogen werden kann.

## Aktiver Teilnahmecheck

Diesmal geht es darum, einige Kerngedanken zu den methodischen Ansätzen in den Vorbereitungstexten festzuhalten.
Bitte antwortet daher auf einen der drei Forumsthreads im TUWEL mit ein paar Sätzen dazu, welche Methode oder
welches Kernkonzept in dem gelesenen Text euch am relevantesten erscheint. Gerne kann stattdessen auch
auf andere bereits formulierte Gedanken geantwortet werden - im Sinne von Ergänzung, Erweiterung
oder auch Widerspruch.

Wir werden dieses Forum dann auch für die kommende Session nochmals weiterverwenden,
Gedanken können dann auch über einen längeren Zeitraum noch weitergesponnen werden.

## Fachkleingruppenaustausch
Wir finden uns in Kleingruppen zu 4 Personen (+/- 1) zusammen, in denen alle im selben technowissenschaftlichen Feld tätig sind (z.B. Studienrichtung). Jede Gruppe erhält einen A3-Zettel und notiert ihr(e) Fachgebiet(e). In einem 15-minütigen Austausch versuchen wir dann jeweils mind. 3 Beispiele zu finden für:

* Forschungs-/Entwicklungs-Tätigkeiten, bei denen Gender oder Geschlecht fix **keine** Rolle spielt
* Forschungs-/Entwicklungs-Tätigkeiten, bei denen Gender oder Geschlecht fix **eine** Rolle spielt
* Forschungs-/Entwicklungs-Tätigkeiten, bei denen sich die Gruppe unsicher ist, bzw. wo es unterschiedliche Positionen gibt, ob Gender oder Geschlecht eine Rolle spielt

Im Anschluss stellt jede Gruppe kurz ihr Plakat im Plenum vor.

## Methodendiskussion & Lehr/Lern-Reflexion
In einer Mischung aus Plenumsdiskussion und Brainstorming/Sammlung per Post-Its auf einem Pinboard
diskutieren wir die Frage von Methodenwahl und -gestaltung und auch ob bzw. wie relevant diese bereits
im Lehrkontext ist. Es sollen dabei Verbindungen zwischen feministischen Standpunkttheorien,
Epistemologien und unserem methodischen Handeln nachgezeichnet werden.

## Vorbereitung auf Session 10

Für diese Session ist ein weiterer der drei Texte aus der Vorbereitung der letzten Session zu lesen, sowie folgender Zeitungsartikel:

* derStandard (1. Mai 2024): Karin Krichmayr:
  [Warum wir raus aus unserer Bubble müssen](https://www.derstandard.at/story/3000000215910/warum-wir-raus-aus-unserer-bubble-muessen).
  Die Komplexitätsforscherin Fariba Karimi deckt auf, wie soziale Netzwerke Vorurteile
  festigen – und was das damit zu tun hat, dass es so wenig Frauen in der Physik gibt (~10min)
