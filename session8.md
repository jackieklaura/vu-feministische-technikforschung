# Session 8 - Zwischenreflexion II - Unis und die Meritokratie vs Sprache & Inklusion

Auch in dieser Session wollen wir die Reflexion der bisher erarbeiteten und
diskutierten Themen vertiefen. Zugleich gehen wir dabei noch näher auf den
unmittelbaren eigenen Kontext ein: die Hochschule und ihre Leistungs- und
Lehrformen. Dabei loten wir auch Spielräume dieser und anderer
Lehrveranstaltungen aus.

## Aktiver Teilnahmecheck

Ist diesmal als Feedback an die LV-Leitung gedacht und daher als
Frage-Antwort-Forum im TUWEL mit folgender Frage gestaltet:

* Was war für dich bisher der verzichtbarste Teil dieser LV, und was
  würdest du bis zum Ende der LV auf jeden Fall noch gern (anders) haben?
  Das kann sich jetzt sowohl auf Inputs von mir als auch auf die Gestaltung
  der Interaktion beziehen.

## Check-in
> geplante Zeit: 15 min

Während die Teilnehmer:innen eintrudeln, können sie über an den Wänden
befestigte Flipcharts mittels Klebepunkte ihre aktuelle Stimmung
und Verortung in Bezug auf die LV-Themen und -Methoden kundtun.

Zusätzlich können im Rahmen der LV bereits entstandene analoge und
digitale (aber ausgedrucke) Artefakte an einer Art Wandgalerie nochmal
betrachtet werden.

## Soziogramm
> geplante Zeit: 20 min

Durch eine Aufstellung im Raum beantworten und visualisieren wir als
Gruppe folgende Fragen:

* Den Namen wie vieler Leute im Raum kenne ich eigentlich jetzt?
* Wie viele Leute kannte ich zu Beginn der LV beim Namen?
* Wie viel (un)wohler fühle ich mich hier im interaktiveren Seminarraum-Setting
  als zuvor im statischer strukturierten Hörsaal-Setting?
* Wie viel Zeit habe ich bisher in diese LV investiert, im Vergleich
  - zu dem was ich dachte, ich müsste hier investieren
  - zu den 3 ECTS, die für die LV vorgesehen sind
  - zu anderen LVs mit 3 ECTS
* Auf einer Skala von 0 bis 100%, wie gut fand ich bisher:
  - das schlummrige Lauschen der vorgetranen Inputs?
  - das Lesen der Vorbereitungstexte?
  - das Schreiben an der Textaufgabe und den aktiven Teilnahmebeiträgen?
  - die Diskussion mit anderen Teilnehmer:innen
* Wie stark haben mich diesmal die Vorbereitungstexte auch nach dem Lesen
  noch beschäftigt?

## Fishbowl-Diskussion zu den Vorbereitungstexten
> geplante Zeit: 30 min

Anhand der [Fishbowl-Methode](https://en.wikipedia.org/wiki/Fishbowl_(conversation))
diskutieren wir die beiden Texte, wobei die LV-Leitung moderierend tätig ist.

## Plenumsdiskussion
> geplante Zeit: 30 min

Fokus:
- wie wirkt eigentlich diese LV auf uns? Was davon wäre in anderen
  Fächern auch anwendbar, und was nicht?
- Inwiefern prägen Lehrformate und die Didaktik unabhängig vom
  eigentlichen Thema auch die Zugänge zu einem Thema?
- wo bzw. für wen ist es überhaupt möglich sich so viel Raum und
  Gestaltungsfreiheit in der Didaktik herauszunehmen?

## Checkout & Vorbereitung auf Session 9
> geplante Zeit: 5 min

Für die folgende Session ist einer der folgenden drei Texte zu lesen
(macht euch dabei ein paar Notizen über die Kernaussagen, da dies in
der kommenden aktiven Teilnahmefrage und der folgenden Session
diskutiert wird):

- Wigginton, Britta, and Michelle N Lafrance. 2019.
  “Learning Critical Feminist Research: A Brief Introduction to Feminist
  Epistemologies and Methodologies.” Feminism & Psychology 0 (0): 1–17.
  https://doi.org/10.1177/0959353519866058. (~30 min)
- Ernst, Waltraud. 2019. “Technikverhältnisse: Methoden feministischer
  Technikforschung.” In Handbuch interdisziplinäre Geschlechterforschung,
  edited by Beate Kortendiek, Birgit Riegraf, and Katja Sabisch, 447–55.
  Band 1. Wiesbaden: Springer VS. (~25 min)
- Cojocaru, Eugenia, Waltraud Ernst, Peter Hehenberger, Helmut J. Holl,
  and Ilona Horwath. 2014. “Design for Gender: Bedienungsgerechte
  Maschinenentwicklung durch Expertise von MaschinenbedienerInnen.”
  In Agenda Gute Arbeit: geschlechtergerecht!, edited by Marianne Weg,
  Brigitte Stolz-Willig, and Reiner Hoffmann. Hamburg: VSA. (~45 min)

Wex hier gleich mehrere Texte lesen will, kann damit gleich vorarbeiten.
Die Vorbereitung für die kommende Session wird daraus bestehen noch
einen weiteren dieser drei Texte zu lesen.

Die Details und PDFs zu den Texten gibt es wie immer im TUWEL-Kurs.
