# Session 12 - Binary impositions on a fuzzy world - Part 2

## Aktiver Teilnahmecheck

Diesmal sammeln wir für Session 11 & 12 gemeinsam über eine Woche Antworten
auf folgende 4 Fragen:

* Welchen der Punkte aus den "Suggested Guidelines for Non-Intersex Individuals
  Writing about Intersexuality and Intersex People" findet ihr besonders gut
  geeignet um ihn auch für andere Kontexte und Themen zu adaptieren?
* Welchen der Punkte aus den "Suggested Rules for Non-Transsexuals Writing
  about Transsexuals, Transsexuality, Transsexualism, or Trans \_\_\_\_" findet
  ihr besonders gut geeignet um ihn auch für andere Kontexte und Themen zu
  adaptieren?
* Welche Begrifflichkeiten in diesem Themenfeld sind euch schon mal
  untergekommen, haben aber im Text noch gefehlt?
* Welche der Begrifflichkeiten im Text findet ihr (inzwischen) nicht mehr
  so adäquat, bzw. zu wenig kontextualisiert? Wozu bzw. in welchen Kontexten
  würde der Begriff noch Sinn machen, bzw. was wäre ein besser Begriff dafür?

## Input-Sequenz

Die Input-Sequenz in dieser Session fällt zugrunsten der Diskurs-Sequenz aus,
da als Vorbereitung auf die heutige Session der gesamte Input-Sequenz-Text
von [Session 11](session11.md) gelesen wurde.

## Diskurs-Sequenz

Während der Ankommphase sammeln wir analog zur Methodensammlung in der
Session 10 spezifische Methoden zur Diskussionsgestaltung bzw. Moderation
von Diskussionen.

Danach entscheiden wir uns für eine oder zwei dieser Methoden und gestalten
damit für ca. eine Stunde eine Diskussion zu den Themen von Session 11 & 12.

Zum Abschluss gibt es wieder einen knapp halbstündigen Block für die
bestehenden Projektgruppen, um an ihrem Thema zu arbeiten, sowie für jene
ohne Projektgruppe, um sich zu Themen zusammenzufinden.

## Bonus Challenge 4

In der begleitenden Schreibaufgabe 2: Schwerpunktreflexion, habt ihr euch
einen Text selbst ausgesucht und diesen zusammengefasst. Im Forum für diese
Bonuschallenge könnt ihr diese Zusammenfassung noch mit allen anderen
Kursteilnehmer:innen teilen und damit weitere 2 Bonuspunkte einsammeln.
Dazu nehmt ihr einfach den Text und macht einen Forums-Post daraus.
Wichtig: der Titel des Forums-Post soll der Titel des gelesenen
Papers/Kapitels sein.

In diesem Fall macht es auch nichts, wenn es zu diesem Text bereits eine
Zusammenfassung gibt. Dann stellt ihr eure Zusammenfassung einfach als
Antwort bei der schon vorhandenen Zusammenfassung zu diesem Text hinein.
Die Punkte gibts dann auf jeden Fall. Es kann auch spannend und
erkenntnisreich sein, wie ein einziger Text aus unterschiedlichen
Perspektiven zusammengefasst wird.

## Vorbereitung auf Session 13

Diesmal gibt es keinen fixen Vorbereitungs-Task, allerdings sollen jene
Projektgruppen, die bereits eine konkretere Idee von ihrem Projekt haben
im Offenen Kursforum unter dem neuen Thread "Projektthemen" ihr Thema
posten, mit der/den zentralen Fragestellung(en) und welche Methode(n)
sie gern ausprobeiern möchten.
