# Session 6 - Partizipation und Public Engagement with Science & Technology

> _Die begleitenden Slides gibt es hier: [slides/session6.html](https://tantemalkah.at/feministische-technikforschung/2024/slides/session6.html)_

## Aktiver Teilnahmebeitrag

Im TUWEL (Moodle) steht ein Forum mit folgender Aufgabe bereit:

In Sheila Jasanoff's Text klang bereits an, dass ein Problem an (vermeintlicher) Wissenschaftsskepsis nicht (nur) das öffentliche Verständnis von Wissenschaft, sondern vielmehr (auch) das wissenschaftliche Verständnis von Öffentlichkeit ist.

Aber wie schaut dieses Verständnis eigentlich an der TU und in den einzelnen Fachbereichen aus? Das wird in unterschiedlichen Studienrichtungen sicher sehr unterschiedlich (von sehr zentral bis gar nicht) diskutiert und dargestellt. Wie nehmt ihr das selber war? Ist das ein Thema, oder fehlt das? Welche Öffentlichkeit(en) nehmt ihr selbst als besonders relevant wahr?

Bitte macht einen Thread mit dem Titel eurer Studienrichtung auf, falls noch keiner da ist, und andernfalls postet in einen bereits bestehenden Thread. So können wir die unterschiedlichen Perspektiven versuchen ein bisschen zu bündeln.

## Einstiegs-Sequenz

Zum Start:
- 5min 2/3er Murmelgruppen
	- Wie lief die Vorbereitung?
	- Was ist eure Erwartung an die heutige Session?
	- Was hat mir nach der letzten Session am meisten gefehlt?

Währrenddessen auch Slido mit offener Q & A vom letzten mal fortführen:
welche Fragen möchtet ihr heute diskutieren, nachdem wir jetzt sowohl die
Wissenschafts- & Technikforschung als auch feministische Perspektiven
darauf überblicksartig betrachtet haben?

## Input-Sequenz

Bisher haben wir uns vor allem mit unterschiedlichen Feminismen und feministischen Epistemologien befasst, und diskutiert, was diese für ein Forschen in und über Wissenschaft & Technik bedeuten. Nun knüpfen wir an die Frage an, wer denn überhaupt an technowissenschaftlichen Unternehmungen beteiligt ist oder beteiligt sein sollte, und wie Akteur:innen außerhalb des Feldes mit einbezogen werden können.

Eine zentrale Frage, die uns in der Auseinandersetzung mit feministischer Technikforschung bis jetzt schon begleitet hat ist jene wer denn die aktiven Akteur:innen in der technowissenschaftlichen Praxis sind, und was das für ihr Erkenntnispotential bedeutet. Eine weitere Frage, die bisher vielleicht eher implizit mitschwang ist jene, wie denn darüber hinaus unterschiedliche Akteur:innen in die technowissenschaftliche Praxis eingebunden sind. So haben sich in den (grob) letzten 50 Jahren in unterschiedlichen Disziplinen auf unterschiedlichen Ebenen Praktiken etabliert um Nicht-Wissenschafter:innen an wissenschaftlicher Produktion zu beteiligen. Auf der einen Seite haben sich diverse partizipative Forschungsmethoden entwickelt, auf der anderen Seite hat sich vor allem auf einer Policy-Ebene immer mehr die Frage gestellt wie Öffentlichkeit in Wissenschaft und Technik eingebunden ist und werden soll.

In der STS gibt es dafür auch den Begriff des "participatory turn". Dieser beschreibt eine Veränderung des Verhältnisses von Technowissenschaft und Öffentlichkeit(en), die vor allem seit den 1980er/90ern verstärkt beobachtet wurde.

Laut Martin Lengwiler wurde der Begriff _participatory turn_ im STS-Kontext vor allem von Sheila Jasanoff geprägt, und zwar in einer Veröffentlichung von 2003 (cf. Lengwiler 2008, p. 187, citing Jasanoff 2003). Jasanoff, für die leider keine Zeit mehr in unseren STS-Intro-Sessions übrig war, aber von der ihr nun zumindest einen Text gelesen habt, ist Professor of Science and Technology Studies in Harvard und hat besonders für die Bereiche des science policy making und der science governance zentrale Beiträge für die STS geliefert. Auf zwei ihrer Konzepte, _technologies of humility_ und _civic epistemologies_ werden wir später noch zurückkommen. Aber zuerst nochmal zurück zum _participatory turn_.

Da es gar nicht so einfach ist diesen participatory turn zu beschreiben (geht es mehr um den participatory turn in STS selbst, oder auch einen participatory turn in science & technology oder deren governance, und welche Art von participation ist gemeint), und die STS hier zwar eigene Geschichten dazu hat (auf die wir später noch detaillierter eingehen), gibt es inzwischen ja die Möglichkeit eine statistische Zusammenfassung aller im Internet vorhandenen Informationen abzufragen, z.B. mit OpenAI's ChatGPT. Und für einen Einstieg ist die Antwort auf die Frage "What is the 'participatory turn' in science & technology studies?" durchaus hilfreich:

> "Traditionally, scientists and researchers have been seen as the sole authorities on scientific knowledge, with the public viewed as passive recipients of this knowledge. However, the participatory turn challenges this view by emphasizing the importance of including diverse perspectives and experiences in the research process. This approach acknowledges that scientific research can have significant impacts on society, and that those who will be affected by scientific developments should have a say in how research is conducted and how its results are used." (ChatGPT Mar 23, asked 2023-05-30: What is the "participatory turn" in science & technology studies?)

Es ist also irgendetwas passiert, das uns als Gesellschaft an einen Punkt gebracht hat, an dem wir nicht einfach jede wissenschaftliche Forschung so hinnehmen wollen. Während wir bereits in den Weltkriegen des 20. Jahrhunderts, im pseudo-verwissenschaftlichten Vernichtungswahn der Nazis, oder auch in der Erfindung der Atombombe verschiedene solche Anlassfälle sehen könnten, hat sich aber erst am Ende des 20. Jahrhunderts sowohl innerhalb der Wissenschaften als auch innerhalb der Zivilgesellschaft eine kritische Dynamik etabliert, die zu diesem participatory turn geführt hat. Aber was hat uns dieser participatory turn seither gebracht? ChatGPT gibt uns weiter Auskunft:

> "The participatory turn has led to the development of a range of participatory research methods, such as citizen science, participatory action research, and co-design. These methods involve collaborating with non-experts to identify research questions, design studies, collect data, and analyze results. This approach can lead to more socially relevant and responsible research, as well as greater public engagement with science and technology." (ChatGPT Mar 23, asked 2023-05-30: What is the "participatory turn" in science & technology studies?)

Es geht also um die Beteiligung von vormals nicht beteiligten Stakeholder:innen in technowissenschaftlichen Forschungs- und Entwicklungsprozessen. Dabei kann die Beteiligung einerseits eher auf einer Policy-Ebene stattfinden, also im Bereich der Entscheidung welche Projekte und Forschungen überhaupt gefördert und betrieben werden sollen, oder aber auf der konkreten methodischen Ebene einzelner Disziplinen. Die STS hat sich dabei aus meiner Sicht vor allem mit der Policy-Ebene befasst, was im framing des _public engagement_  in und mit Wissenschaft und Technik Ausdruck findet.

Brian Wynne, Professor Emeritus of Science Studies an der Lancaster University, für den in unserer STS-Intro-Einheit leider auch keine Zeit mehr übrig war, hat sehr viel im Bereich _Public Understanding of Science (PUS)_  bzw. _Public Engagement with Science & Technology (PEST)_ und zum Verhältnis von Expert:innen- und Lai:innen-Wissen zur technowissenschaftlichen Politikgestaltung geforscht. In der Gründungsausgabe des Journal "East Asian Science, Technology and Society" 2007 merkt er in einem Artikel in einer Fußnote an, dass wir eigentlich von einem "participatory return" sprechen müssten:

> "It may be more appropriate to call this a ‘participatory return’, since a similar mushrooming of such initiatives took place in the 1970s in Western countries, albeit with some significant differences." (Wynne 2007, p. 100)

Was meint er aber mit dem "mushrooming of such initiatives"? Dazu müssen wir erst die Ausgangssituation beleuchten, die er wie folgt einleitet:

> "One of the pervasive problems in the endlessly escalating and globalising arenas of public reactions to what is called ‘science’ (or often its de-facto synonym, ‘risk’), is the dearth of serious questioning of what is this object—‘science’? ‘risk’? ‘uncertainty’?—which has usually been just taken for granted as the origin of meaning for all the actors involved." (ibid)

Hier wird ein Verhältnis von Öffentlichkeit zur Wissenschaft angesprochen, bzw. eine Darstellung davon, die Wynne in weiterer Folge kritisiert:

1. es wird ein reaktives Verhältnis angenommen: Wissenschaft tut etwas, und die Öffentlichkeit reagiert darauf, wobei hier vor allem auf Formen von Dissens, Protest oder Ablehnung fokussiert wird
2. während es viele Auseinandersetzungen damit gibt wer oder was denn die relevanten Öffentlichkeiten für technowissenschaftliche Forschung sind, wird kaum hinterfragt was denn "science" im Kontext dieses Verhältnisses überhaupt bedeutet, und was vor allem die relevanten Öffentlichkeiten darunter verstehen

Während das Thema laut Wynne seit rund 10 Jahren vor der Veröffentlichung des Artikels in und um die STS als sehr relevant erachtet wurde und entsprechend viele Konzepte und Methoden für das _public engagement with science_ entwickelt wurden, bleibt unter den vorhin genannten Aspekten ein zentrales Problem bestehen:

> "This also imposes a hugely problematic, unacknowledged normative commitment as to the subject-identities of those publics. The mushrooming academic field which claims to clarify these issues over ‘public engagement with science’ often appears uncritically to reproduce and reinforce those same actors’ categories." (ibid)

Ein bloßes Anerkennen, dass öffentliche Befragungen, Beteiligungen, oder gar tatsächliche Partizipation im Forschungsprozess wichtig sind, reicht demnach nicht, um die Forschung und ihre Ergebnisse solider und robuster zu machen. Hier gibt es einen aus meiner Sichtig wichtigen Schnittpunkt zur feministischen Technikforschung. Letztere bringt nämlich einen expliziteren bzw. strukturell machtkritischen Anspruch in diese Frage hinein. Es ist nicht nur die Frage ob, sondern auch wann und wie Öffentlichkeiten, oder Betroffene partizipieren. Also nicht nur wie "Partizipation" oder "Beteiligung" geframed werden, sondern auch wer sie wann initiieren kann sind entscheidende Fragen. Und durch intersektionale Analysen können auch unterschiedliche Betroffenengruppen identifiziert werden, die womöglich auch widersprüchliche Interessen in den Prozess einbringen, aber allesamt Gruppen von relevanten Stakeholder:innen darstellen.

Ein weiterer Aspekt der oben anklingt ist, dass eine kritische öffentliche Debatte und Einbindung in Wissenschaft und Technik aufgrund von Risiken der Forschung und Entwicklung relevant wird. Auch hier kann Kritik formuliert werden, dass es eben nicht nur im Fall potentiell riskanter Forschung und Entwicklung notwendig ist, einen gesellschaftlichen Diskurs über Notwendigkeit und Ausrichtung von Wissenschaft und Forschung zu führen. Hier kommen wir auch wieder zu Sheila Jasanoff und ihren _technologies of humility_ zurück.

In ihrem Artikel "Technologies of Humility: Citizen Participation in Governing Science." von 2003 bezieht sie sich unter anderem auf Charles Perrow, der im Bereich der Soziologie großer Organisationen breit rezipiert wurde und 1984 ein Buch mit dem Titel "Normal accidents : living with high-risk technologies" herausbrachte. Darin analysiert er die sozialen und organisatorischen Komponenten technologischen Risikos und argumentiert, dass konventionelle Zugänge zu safety engineering aufgrund von hoher Komplexität und enger Kopplung unweigerlich zu Fehlern und Katastrophen führen. Alleine in den Jahren unmittelbar nach dieser Veröffentlichung ereigneten sich dann noch in dieses Schema fallende und weltweit bekannt gewordene (sozio)technische Katastrophen: die Chemie- und Umweltkatastrophe in Bhopal 1984, die Challenger-Katastrophe und die Tschernobyl-Katastrophe.

Für Jasanoff ergibt sich aus einer Serie von solchen 'normal accidents' ein zunehmendes öffentliches Bewusstsein darüber, dass bisherige Ideen der Verwaltbarkeit und Kontrollierbarkeit von komplexen technologischen Systemen überdacht, und auch entsprechende Technopolitiken (technical policy-making) neu gedacht werden müssen. Während die bestehenden Modelle vor allem an bestehende Machtverhältnisse gebunden sind, müsste eine umfassendere Idee von Verantwortlichkeit (accountability) alle relevante Stakeholder:innen miteinbeziehen. Insofern müssen auch die Rollen von Expert:innen, Bürger:innen und Entscheidungsträger:innen überdacht werden.

Und während eine Verschiebung hin zu partizipativeren Formen der Technikfolgenabschätzung stattfindet, wird aus Jasanoff's Perspektive auch sichtbar, dass Partizipation allein nicht zur Demokratisierung technischer Gesellschaften führt. Eine offene und oft zu ungenau gestellte Frage ist, wer eigentlich in diese Prozesse integriert werden soll und was mit den Ergebnissen passieren soll, wie diese am Ende in die eigentliche technische Entwicklung einbezogen werden sollen. Um diese Fragen sinnvoll zu klären müssen für Jasanoff erst neue "institutionalized habits of thought" (Jasanoff 2003, p. 227) entwickelt werden, die dann als Framework für die Anwendung partizipativer Methoden dienen.

Sie beschreibt dabei das bisher etablierte Set von Techniken und institutionaliserten Prozessen der Technikfolgenabschätzung (z.B. risk assessment, climate modelling, cost-benefit analysis) als _predictive methods_ deren Ziel es ist Technologien kontrollierbar und verwaltbar (managable) zu machen. Diese würden implementiert um die Öffentlichkeit zu beschwichtigen (reassure) und die Angst vor bereits vorgestellten Risiken zu nehmen. Allerdings werden dabei jene Risiken übersehen, die im öffentlichen Diskurs (oder auch im Forschungsprozess) bisher noch nicht erwähnt wurden. Dabei beschreibt sie drei zentrale Mängel dieser Praktiken: 1) "peripheral blindness" gegenüber Unsicherheit und Ambiguität, 2) Vorwegnahme (preemption) einer politischen Diskussion, und 3) dass Gefahren außerhalb der _framing assumptions_ gar nicht gesehen werden. (ibid, p. 239). Dieses Set von Techniken und institutionalisierten Prozessen mit all ihren Mängeln nennt Jasanoff _technologies of hubris_.

Dem gegenüber stellt sie die noch zu bzw. sich bereits entwickelnden _technologies of humility_, die sie als Set von Techniken und institutionalisierten Prozessen beschreibt, die nicht versuchen Vorhersagen zu machen und Sicherheiten zu schaffen, sondern ergänzend zu den _technologies of hubris_ dafür sorgen sollen, dass unvorhergesehene Konsequenzen entdeckt und bedacht werden. Normatives soll dabei explizit gemacht werden, und _plural viewpoints_ sowie _collective learning_ sollen dabei im Prozess etabliert werden. Die zentralen Fragen die dabei beantwortet werden sollen sind: "what is the purpose; who will be hurt; who benefits; and how can we know?" (p. 240).

Durch die gleichzeitigen Anwendung dieser beiden "social technologies" kommen wir dann zu besseren Analysen der Implikationen neuer Technologien und technowissenschaftlicher Forschungsvorhaben. Jasanoff nimmt dabei auch auf das _Mode-2_ Konzept von Nowotny et al. Bezug, das wir in den STS-Intro Sessions kurz erwähnt haben. Dieser Transformation technowissenschaftlicher Prozesse hin zu Anwendbarkeit, Transdisziplinarität, Dezentralisierung und Reflexivität stellt Jasanoff 4 Fokuspunkte zur Seite: _framing_, _learning_, _distribution_ and _vulnerability_. Diese müssen als Basis für alle weiteren Fragestellungen im Forschungsprozess dienen.

Generell hat sich in und durch die STS um die Jahrtausendwende auch eine Verschiebung aufgetan, weg von einem _deficit model_ des _public understanding of science_, nach dem öffentliche Akzeptanz von Technowissenschaft primär durch Aufklärungsarbeit hergestellt werden sollte, hin zu einer Perspektive des _public engagement with science & technology_, also einem produktiven Austausch mit Öffentlichkeiten. Insgesamt besteht dabei aber ein starker Fokus auf die Policy-Ebene, und es gibt wenige Ansätze einer unmittelbaren Einbindung von Öffentlichkeiten in konkrete Methoden der Forschung und Entwicklung.

Meine eigene Forschung hat sich hier vor allem der Frage angenommen welche Öffentlichkeiten sich im Kontext technowissenschaftlicher Praxis formieren können, wie wir uns als Forschende selbst teil von trans:disziplinären Forschungs- und Öffentlichkeitskollektiven verstehen sollten, und wie Transdisziplinarität auch auf methodischer Ebene gelebt werden kann. Daher möchte ich an dieser Stelle für weitere Einblicke nur auf diese Arbeit verweisen: (Klaura 2014, 2016). Im Journal-Beitrag von 2016, der begleitend zur Changing Worlds Conference entstand fasse ich einerseits die zentralen Einsichten meiner Thesis von 2014 zusammen, in der es um konkrete partizipative Prozesse im Participatory Design als transdisziplinärem Feld innerhalb der Informatik einerseits geht, und die Konstruktion breiter gefasster Öffentlichkeiten im Wechsel von Policy- zu konkreter Forschungsebene. Andererseits versuche ich auch einen Anstoß zu geben wie wir uns selbst als Forschende beforschen können.

Bevor wir nun wieder in die Diskussion gehen ist mir hier aber noch ein Aspekt besonders wichtig: die Art bzw. der Grad der Partizipation der in unterschiedlichen Settings und mit unterschiedlichen Methoden erreicht wird. Und als Schablone zum Einordnen konkreter Prozesse eignet sich hier meines Erachtens nach wie vor die _Ladder of Citizen Participation_ wie sie von Sherry Arnstein 1969 im Journal of the American Planning Association beschrieben wurde (Arnstein 1969), und deren Visualisierung 2004 von Duncan Lithgow online reproduziert wurde und nun in den Wikimedia Commons zu finden ist:

![](ladder_of_citizen_participation.png)<br>
Quelle: [Wikimedia Commons: Ladder of citizen participation, Sherry Arnstein.tiff](https://commons.wikimedia.org/wiki/File:Ladder_of_citizen_participation,_Sherry_Arnstein.tiff)

In diesem Model stellt Arnstein 8 Stufen von (vermeintlicher) Partizipation dar. Diese Teilen sich wiederum in drei breitere Kategorien: “Citizen Power”, “Tokenism” und “Nonparticipation”, wobei eben nur die erste bzw. höchste davon (Citizen Power) einem tatsächlichen partizipativen Anspruch genügt, während die anderen beiden oft vorgefunden Formen von Pseudo-Partizipation sind, die eher einen beschwichtigenden Charakter haben als tatsächlich das Ziel von öffentlicher Beteiligung an der Gestaltung von Prozessen und Vorhaben zu verfolgen. Diese Leiter können wir nicht nur bei der Beobachtung und Analyse von Prozessen anderer Akteur:innen als Analyseinstrument verwenden, sondern wir können sie auch als Maßstab beim Design unserer eigenen partizipativen Forschungsprozesse verwenden.

## Diskurs-Sequenz

Nach einer ersten offenen Frage- und Antwortrunde zum Input greifen wir
die Beiträge aus der Einstiegs-Sequenz auf und diskutieren diese im Plenum.

## Bonus Challenge 2 (2 Punkte)

Um die 2 Bonuspunkte für diese Challenge zu bekommen, poste im entsprechend TUWEL Forum den Link und eine kurze eigene Erläuterung zu:

- einem Artikel, Paper, Tutorial, Video oder Podcast/Audiobeitrag der eine bestimmte Methode beschreibt, die sich gut für die feministische Technikforschung eignet (kann auch eine Methode aus dem eigenen Fach sein, die in einer feministischen Art angewandt wird).
- Regeln für die Gültigkeit des Beitrags:
  - Wikipedia-Artikel sind ausgenommen: diese sollten wir bereits als Standardreferenzmaterial voraussetzen können
  - nicht nur den Link posten, sondern auch eine kurze Erklärung wieso du das jeweilige Material besonders gut oder hilfreich findest
  - für Materialien die schon von jemensch anders gepostet wurden, gibt es keine weiteren Punkte mehr

## Checkout & Vorbereitung auf Session 7

Diesmal ist keine extra Vorbereitung auf die nächste Session nötig. Nützt die Lücke
aber gegebenenfalls für die erste Schreibaufgabe.

Und ganz am Ende unser üblicher Checkout-Slido mit einer einzigen Frage:
* Word cloud: Mit welchem Gefühl oder Gedanken gehe ich nun aus der LV und in den Abend?

## Referenzen

Arnstein, Sherry R. 1969. “A Ladder of Citizen Participation.” Journal of the American Planning Association 35 (4): 216–24.

Jasanoff, Sheila. 2003. “Technologies of Humility: Citizen Participation in Governing Science.” Minerva 41 (3): 223–44.

Klaura, Andrea* Ida Malkah. 2016. “Changing the World for Whom? Some Thoughts about Trans*disciplinarity, Feminist Epistemologies and Participatory Design.” Graduate Journal of Social Science 12 (2): 108–26. http://gjss.org/12/02

Klaura, Andrea Ida Malkah. 2014. “Computer Scientists & Their Publics. On Constructions of ‘participation’ and ‘Publics’ in Participatory Design and Research.” Master Thesis, Vienna (Austria): University of Vienna. http://othes.univie.ac.at/34851.

Lengwiler, Martin. 2008. “Participatory Approaches in Science and Technology.” Science, Technology & Human Values 33 (2): 186–200. https://doi.org/10.1177/0162243907311262.

Wynne, Brian. 2007. “Public Participation in Science and Technology: Performing and Obscuring a Political–Conceptual Category Mistake.” East Asian Science, Technology and Society: An International Journal 1 (1): 99–110. https://doi.org/10.1007/s12280-007-9004-7.
