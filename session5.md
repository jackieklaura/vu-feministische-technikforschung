# Session 5 - Feministische Epistemologien

## Aktiver Teilnahmebeitrag

Im TUWEL (Moodle) steht ein Forum mit folgender Aufgabe bereit:

Gedanken zu einem der Vorbereitungstexte. Bitte formuliert zumindest zu einem der Themen/Texte aus der Vorbereitung einen Gedanken den ihr beim Lesen/Nachdenken hattet. Das kann sich aber auch auf bereits von anderen formulierte Gedanken beziehen.

Ich stelle dazu im Forum ein paar initiale Threads ein, mit der Idee, dass ihr innerhalb dieser Threads eure Beiträge oder Antworten auf andere Beiträge formuliert. Wenn jemensch aber findet, dass hier ein ganz wesentliches Thema aus den zu lesenden Texten als Initial-Thread fehlt, könnt ihr gern auch einen neuen einstellen. Bitte aber nur, wenn das nicht schon irgendwie in einem anderen Thread behandelt wird.

Initiale Themen:
- Feminist Technoscience Studies
- Feminist Science and Technology Studies: A Patchwork of Moving Subjectivities
- Donna Haraway’s Situated Knowledges
- Sandra Harding’s Strong Objectivity

## Einstiegs-Sequenz

Zum Start:
- 5min 2/3er Murmelgruppen
	- Wie lief die Vorbereitung?
	- Was ist eure Erwartung an die heutige Session?
	- Was hat mir nach der letzten Session am meisten gefehlt?

Slido mit offener Q & A: welche Fragen möchtet ihr heute diskutieren, nachdem wir jetzt sowohl die Wissenschafts- & Technikforschung als auch feministische Perspektiven darauf überblicksartig betrachtet haben?

Da die Input-Sequenz bereits als Vorbereitung auf diese Session gelesen wurde,
können wir in der Session an dieser Stelle direkt zur Diskurs-Sequenz springen.

## Input-Sequenz

In der letzten Einheit haben wir uns grob den unterschiedlichen feministischen Strömungen und ihrer Bedeutung für Wissenschaft und Technik angenähert. Darin ist bereits angeklungen, dass dabei unterschiedliche Epistemologien entwickelt und herangezogen werden, um bestehende Ansätze weiterzuentwickeln oder gar neue Ansätze wissenschaftlicher und technischer Praxis zu etablieren.

In dieser Einheit wollen wir uns diesen - oder zumindest manchen - feministischen Epistemologien weiter nähern und ihre Bedeutung für unsere eigene technowissenschaftliche Praxis diskutieren.

### Feministische Epistemologien

Aber was ist überhaupt Epistemologie? Im Großen und Ganzen sprechen wir dabei synonym von Erkenntnistheorie.  Die _epistéme_ aus dem Altgriechischen bezeichnen Erkenntnis, Wissen, oder auch Wissenschaft, und das Präfix _-logie_ zielt auf die Beschreibung oder Lehre ebendieser ab.

Es geht dabei also vor allem um die Frage was und wie wir Menschen erkennen können, und was wir als gültige Erkenntnisse betrachten können. Neben einem klassischen rationalen und logischen Denken, geht es dabei also auch um Wahrnehmung, Erfahrung, und Sprache, die alle dazu beitragen, wie wir unsere Überzeugungen formen und am Ende zu gefestigtem Wissen gelangen. Ein Ziel klassischer Epistemologien ist es, ein systematisches und grundlegendes Gedankengebäude zu schaffen, anhand von dem wir Aussagen darüber machen können was als wahr oder falsch gelten kann. Im Sinn des Wortes selbst soll also eine Theorie der Erkenntnis entwickelt werden.

Aus unserer Auseinandersetzung mit verschiedenen Feminismen erahnen wir aber bereits, dass es vielleicht gar nicht so einfach, und vielleicht auch gar nicht zielführend ist genau EINE solche Theorie zu haben. Bzw. könnte die Arbeit, die wir in das erstellen einer unified theory of knowledge über alle wichtigen Epistemologien hinweg, zielführender für andere wissenschaftliche Projekte verwendet werden. Das legen uns sowohl die Wissenschafts- und Technikforschung als auch feministische Auseinandersetzungen mit Wissenschaft und Technik nahe. In welche Richtung nun aber zielführend zu Arbeiten wäre hängt wieder sehr stark damit zusammen wo wir uns inhaltlich und theoretisch bzw. eben auch epistemologisch verorten.

Aber zurück zu unseren unterschiedlichen feministischen Ansätzen. In der letzten Einheit sind schon Ansätze von unterschiedlichen Epistemologien angeklungen, die sich ergeben, je nachdem ob eher aus einem liberalen Feminismus, einem sozialistischen oder schwarzen Feminismus, aus einem radikalen oder lesbischen Feminismus, oder einer anderen feministischen Richtung gedacht wird, insbesondere wenn wir das im unmittelbaren Kontext von Wissenschaft und Technik tun.

Wenn wir uns nun an den liberalen Feminismus zurückerinnern, so besteht hier kein besonderer Bedarf an einer eigenen, oder neuen Epistemologie. Hier wird auf Basis eines klassischen Empirismus vorgeschlagen noch gründlicher im Sinne des eigentlichen Empirismus zu arbeiten, um am Ende zu einer gender-freien bzw. gender-neutralen Wissenschaft zu gelangen, die um jeglichen bias bereinigt ist. Das ist jetzt klarerweise etwas simplifiziert, um auf den Punkt zu bringen, dass an der grundsätzlichen Art wie Erkenntnis im Kontext technowissenschaftlicher Forschung gedacht wird nichts zu ändern ist - und das blendet hier um diesen Punkt zu machen auch aus, dass es inzwischen ja auch technowissenschaftliche Zugänge, die eben auf einer anderen Epistemologie beruhen.

Wir haben aber in der letzten Einheit auch gesehen, dass sich insbesondere im letzten Drittel des 20. Jahrhunderts abseits des liberalen Feminismus andere feministische Kritiken an und Zugänge zu Wissenschaft und Forschung etabliert haben. Während manche die gesamte Idee von Wissenschaft und Technik als inhärent patriarchal und in diesem Sinne nicht änderbar verstehen, sind für uns an dieser Stelle vor allem jene Zugänge interessant die Wissenschaft und Technik nicht an sich negieren, sondern auf eine bessere, adäquatere oder allen Menschen gerechtere Wissenschaft und Technik hinwirken wollen, und dazu Probleme im Wurzelwerk dieser gesellschaftlichen Vorhaben suchen, nämlich in der Epistemologie.

### Sandra Harding's Strong Objectivity

Sandra Harding ist eine der prägendsten Stimmen, wenn es um feministische Standpunkt-Epistemologie geht. 1935 geboren, und 1973 an der New York University ein Doktorat in Philosophie abgeschlossen, war sie von 1976 bis 1996 an der University of Delaware tätig, bevor sie an die UCLA wechselte, wo sie bis 2000 das Center for the Study of Women leitete und weiter als Professor of Education and Gender Studies tätig war, als die sie inzwischen emeritiert ist.

Harding hat auf Basis von Standpunkt-theoretischen Ansätzen das Konzept der "strong objectivity" erarbeitet, das als qualitativer Gradmesser für wissenschaftliche Erkenntnisse herangezogen werden sollt, weil wie sie schreibt:

> "[t]he problem with the conventional conception of objectivity is not that it is too rigorous or too “objectifying,” as some have argued, but that it is _not rigorous or objectifying enough_; it is too weak to accomplish even the goals for which it has been designed, let alone the more difficult projects called for by feminisms and the other new social movements." (Harding 1993, pp. 50-51)

Um dieses Problem zu lösen schlägt sie vor stärkere Standards für wissenschaftliche Methoden und Methodologien vor, die stark objektive Aussagen generieren sollen, die nicht von einem vermeintlich "neutralen", "wissenschaftlichen" Standpunkt ausgehen, sondern die ihre Analyse vom Standpunkt unterdrückter Gruppen und Positionen aus starten - also im Sinne der Standpunkt-Theorie gewissermaßen universeller sind als Aussagen die nur von einer privilegierten Gruppe getroffen werden. Daher eben auch _strong objectivity_ im Gegensatz zu einer _weak objectivity_, die von einem "neutralen" Standpunkt aus erzielt wird:

> "Knowledge claims are always socially situated, and the failure by dominant groups critically and systematically to interrogate on their beliefs leaves their social situation a scientifically and epistemologically disadvantaged one for generating knowledge." (ibid, p. 54)

Dies ist bei Harding auch eine gezielte Kritik am feministischen Empirismus, den wir im Rahmen des liberal feminism bereits kennengelernt haben. Während feministischer Empirismus nach Harding versucht "to purify science of all [...] bad politics by adherence to what it takes to be rigorous methods [...] this is far too weak a strategy to maximize the objectivity of the results of research" (ibid, p. 56)

Feministische Standpunkt-Theorie fordert es daher ein, dass Forschung sich immer erst von Fragen marginalisierter Standpunkte aus startet. Praktisch gesehen verknüpft sie das mit demokratischen Ansprüchen und der strukturellen Verknüpfung wissenschaftlicher Forschung mit gesellschaftlichen Anliegen:

> "Thus, strong objectivity requires that scientists and their communities be integrated into democracy-advancing projects for scientific and epistemological reasons as well as moral and political ones." (ibid, p. 69)

Das bedeutet, dass wir marginalisierte Gruppen in unsere technowissenschaftlichen Kontexte einbinden müssen. Einerseits passiert dies durch eine diversere Zusammensetzung von Forschungsteams. Andererseits ist es aber auch notwendig integrative und partizipative Methoden und Methodologien für die jeweiligen Forschungsrichtungen zu entwickeln, in denen bislang eher von einen "neutralen" Standpunkt ausgegangen wurde.

Dass allein die Forderung nach mehr Frauen\* in der Technik, wie sie auch vom liberal feminism vertreten wird, nicht zu den gewünschten Ergebnissen führt, wird zwar leider noch immer zu wenig, aber immerhin schon an manchen Beispielen sichtbar. So hat zum Beispiel Vivian Anette Lagesen die Situation in Malaysia analysiert, wo es unter Informatiker:innen an den Universitäten mehr als 50% Frauen gibt, auch in führenden Positionen. Ein cyberfeministisches Utopia ist daraus aber dennoch nicht entstanden, und die Studienwahl ist nach wie vor durch kulturelle Zuschreibungen entgegen manch inidividuellem Interesse geformt, was sich auch an den Geschlechterverhältnissein innerhalb unterschiedlicher Teilbereiche in der Informatik zeigt (Lagesen 2008).

### Donna Haraway's Situated Knowledges

Eine weitere wichtige und einflussreiche Akteurin in diesem Diskurs ist Donna Haraway. 1944 geboren, hat sie ursprünglich als Zoologie am Colorado College studiert und 1972 ihren PhD in Biologie in Yale abgeschlossen. Nach ersten Forschungsaufenthalten an der University of Hawaii und der Johns Hopkins University kam sie 1980 an die University of California, Santa Cruz, wo sie die erste ordentliche Professorin der USA für Feministische Theorie wurde und seither lehrte und forschte.

Ihr womöglich bekanntester Text - weil er auch weit über akademische Kreise hinaus rezipiert wurde - ist das 1985 in der _Socialist Review_ erschienene _Manifesto for Cyborgs: Science, Technology, and Socialist-Feminism in the 1980s_. Eine aktualisierte Version davon erschien in ihrem 1991 veröffentlichten Buch _Simians, Cyborgs and Women: The Reinvention of Nature_ als Kapitel mit dem leicht geänderten Titel _A Cyborg Manifesto: Science, Technology, and Socialist-Feminism in the Late Twentieth Century_.

Während Haraway generell einen sehr metaphorischen und assoziativen Schreibstil prägt, war dieses Manifest bewusst noch stärker metaphorisch geprägt und auch provokativer geschrieben als ihre anderen wissenschaftlichen Texte (zum Beispiel im gleichen Buch von 1991). Das hat unter anderem auch damit zu tun, dass Haraway sich mit diesem Essay nicht nur gegen klassische Technowissenschaften im Dienste des Kapitalismus richtete, sondern auch gegen verschiedene feministische Zugänge, die aus ihrer Sicht in Identitätspolitiken und Essentialismen feststeckten, und so kein wirkliches Potential für die (Um)Gestaltung technowissenschaftlicher Theorie und Praxis bieten. Das Manifest beginnt auch mit der Feststellung "This essay is an effort to build an ironic political myth faithful to feminism, socialism, and materialsm." (Haraway 1985, p. 65) Es geht hier also nicht so stark um die rein wissenschaftliche Auseinandersetzung, die sie in anderen Texten führt, als um ein Wachrütteln, Aufrufen und Motivieren, um einen politischen Kommentar. Wobei wir inzwischen auch wissen, dass die "rein wissenschaftliche Auseinandersetzung" nicht immer so klar von letztem getrennt werden kann - oder sollte.

Die Figur der:des Cyborg verwendet Haraway, da sie einerseits erst aus Technowissenschaften und High-Tech entstand, durch etliche science-fiction Szenarios umschrieben, aber klassische Dichotomien sprengt, die uns daran hindern die Welt vollständiger wahrzunehmen. Dabei geht Haraway von "three crucial boundary breakdowns" aus, die sich im späten 20. Jahrhundert vollziehen: jene zwischen Mensch und Tier, jene zwischen "animal-human (organism)" und Maschine, und schließlich jene zwischen physisch und nicht-physisch (ibid, pp. 68). Sie greift diese Entwicklung auf um problematische westlich-patriarchale Dualismen zu hinterfragen wie "self/other", "culture/nature", "male/female", "civilized/primitive", "right/wrong", "truth/illusion", "total/partial", "God/man". Sie stellt damit nicht nur in Aussicht, dass wir neue Epistemologien brauchen, sondern dass wir auch an feministischen Ontologien arbeiten müssen, also an neuen Klassifikationen von dem was ist.

Haraway hat den von ihr beschriebenen Zusammenbruch bestehender Dichotomien beschrieben als "epistemological electro-shock therapy, which far from ushering us into the high stakes tables of the game of contesting public truths, lays us out on the table with self-induced multiple personality disorder", was dazu geführt hat, dass "[s]ome of us tried to stay sane in these disassembled and dissembling times by holding out for a feminist version of objectivity." (Haraway 1991, p. 186). In einem 1988 in _Feminist Studies_ publizierten Text "Situated Knowledges: The Science Question in Feminism and the Privilege of Partial Perspectives", beschreibt sie die grundlegende Ausgangsproblematik wie folgt:

> "how to have _simultaneously_ an account of radical historical contingency for all knowledge claims and knowing subjects, a critical practice for recognizing our own ‘semiotic technologies’ for making meanings, _and_ a no-nonsense commitment to faithful accounts of a ‘real’ world, one that can be partially shared and friendly to earth-wide projects of finite freedom, adequate material abundance, modest meaning in suffering, and limited happiness." (Haraway 1988, p. 579, Hervorhebung im Original)

Und während Haraway und Harding aus ähnlichen Positionen argumentieren und auf Standpunkttheorien Bezug nehmen, fokussiert Harding mehr darauf, wie wir diese _feminist objectivity_ (bei ihr eben _strong objectivity_) erlangen können. Haraway hingegen fokussiert mehr auf die relationalen Verknüpfungen aller beteiligten Entitäten und beschreibt das mit dem Begriff der _situated knowledges_. Egal von welchem Standpunkt aus wir _meaning-making_ betreiben, sind wir immer in einem bestimmten sozialen, ökonomischen, kulturellen, ökologischen, vergeschlechtlichten und aus rassifzierten Kontext situiert. Die Herausforderung ist dabei einerseits diesen Kontext zu nutzen und ihn andererseits durch Allianzen mit unterschiedlichsten Akteur:innen zu erweitern, um so zu "faithful accounts of a
‘real’ world" zu gelangen, oder wie sie an anderer Stelle schreibt: "The goal is better accounts of the world, that is, 'science'." (ibid, p. 590)

Als Wissenschafter:innen und Techniker:innen ist es daher unter anderem auch unsere Verantwortung unseren Standpunkt bzw. Situierung explizit/transparent zu machen, weil unserer Forschung nicht davon trennbar ist. Selbst wenn wir in einem eher feministische-empiristischen Paradigma arbeiten, müsste demnach diese Tatsache in die Forschung integriert werden, weil ohne diese Verortung - wie das im Fall der:des neutralen Beobachter:in der Fall ist - eine intersubjektive Nachprüfbarkeit gar nicht erst vollständig möglich ist.

Wir werden diesen Ansatz der eigenen Situierung auch in der folgenden Einheit, wenn es um konkrete Methoden und Methodologien für die feministische Technikforschung geht, nochmal aufgreifen.

Und während das Framing der "Situiertheit" im Ansatz bereits mehr Möglichkeiten von Fluidität und Überwindung der eigenen Verortung nahelegt (bzw. vielleicht mehr das Darüber hinaus gehen (hier setzt auch mein persönliches Verständnis von Trans\*disziplinarität an)), so stellt sich für viele bei den feministischen Standpunkttheorien noch stärker die Frage, ob denn nun von privilegierten Standpunkten überhaupt pro-feministische, emanzipatorische Zugänge erarbeitet werden können. Da sich dieser Aspekt auch sehr gut für unsere spätere Diskussion anbietet, möchte ich an dieser Stelle nur einen Beitrag von Sandra Harding im 1998 erschienenen und von Tom Digby herausgegebenen "Men Doing Feminism" erwähnen, mit dem Titel "Can Men Be Subjects of Feminist Thought?" (Harding 1998, pp. 171-196). Darin arbeitet sie diese Frage anhand einer ähnlichen Einteilung feministischer Strömungen ab, wie wir sie in der letzten Einheit bei Sue V. Rosser vorgefunden haben. Für eine weitere Auseinandersetzung dazu möchte ich euch den Artikel empfehlen, daher findet er sich auch in unserem digitalen Handapparat. An dieser Stelle nur ein kleiner Spoiler: ja, es gibt verschiedene Möglichkeiten sich auch mit verschiedenen unterdrückten Standpunkten zu verknüpfen und von diesen ausgehend zu denken, unabhängig von der eigenen gesellschaftlichen Verortung. Und während diese Frage hier vor allem in Bezug auf Männer und Feminismus gestellt wird, wird zugleich auf offengelegt wie dieser Prozess auch auf andere soziale Dimensionen angewandt werden kann und wie Allianzen über identitäre Verortungen hinweg funktionieren können.


Jetzt können wir das Thema feministischer Epistemologien nicht allein an den bisher erwähnten Akteur:innen festmachen. Allerdings wollte ich insbesondere Donna Haraway und Sandra Harding mit ihren Ideen zu Standpunktepistemologien und zu Situated Knowledges hervorheben, weil diese sich sehr stark mit der sozialen Verortung auseinandersetzen und wie diese produktiv auch in feministische und emanzipatorische Auseinandersetzungen einbezogen werden kann, unabhängig vom eigenen Status.

Wir gehen nun einerseits mit dieser Frage, und andererseits mit dem Überblick über die Feminist STS, den ihr euch über die Vorbereitungs-Literatur erarbeitet habt, in die heutige Diskussion.


## Diskurs-Sequenz

Wir gruppieren uns zuerst nach Studienrichtungen und formen dann 3er-Teams
(eventuell mit ein oder zwei 2er-Teams), mit denen wir dann - wie in Session 3,
nur jetzt fachlich gruppiert - ein Talking by Walking machen:

In Dreiergruppen begeben sich die Teilnehmer:innen auf einen kurzen Spaziergang,
wobei jede Person 5min davon in der Sprecher:innenrolle ist. Es geht dabei darum,
Fragen zu sich selbst in Verbindung zu setzen und zugleich mit anderen in Austausch
zu treten. Das Gehen dient auch dazu längere Sprechpausen zuzulassen. Die anderen
beiden, die gerade nicht in der Sprecher:innenrolle sind hören zu, können aber
auch nachfragen und so den Austausch unterstützen. Folgende Frage soll dabei
als Leitfrage mit auf den Weg genommen werden:

- Wie(so) kann/will ich feministische Technikforschung in meinem eigenen
  technowissenschaftlichen Umfeld (nicht) integrieren?

Nachdem die Gruppen zurück sind, gehen jeweils zwei Walking-Gruppen zusammen,
wenn möglich ebenso aus dem selben Fach oder fachnah. Die so entstandenen
Kleingruppen diskutieren dann für 15 Minuten ihre Erfahrungen vom Walking
und versuchen eine temporäre Konklusio auf die Leitfrage zu formulieren und
dabei zentrale Gedanken auf auch einem A3 Blatt festzuhalten (das wir später
digitalisieren und im TUWEL teilen können). Diese wird dann von jeder
Kleingruppe kurz im Plenum vorgestellt und diskutiert.

Als abschließende Sequenz erhalten alle Gruppen noch 1 bis 2 Diskussionsschnipsel
von den vergangen Aktiven Teilnahmeforen, und diskutieren diese ebenso für 10
Minuten, um's dann im Plenum kurz zusammenzufassen.

Allfällige verbleibende Zeit (unwahrscheinlich) verwenden wir für eine offene Plenumsdikussion.

## Vorbereitung auf Session 6

- Jasanoff, Sheila. 2014. “A Mirror for Science.” Public Understanding of Science 23 (1): 21–26. https://doi.org/10.1177/0963662513505509. (~15 min)
- Details im TUWEL-Kurs

## Begleitende Schreib-Aufgabe

Part 1 (5 Punkte): Brauchbarkeit feministischer Technikforschung für die eigene Praxis

- Welche Aspekte der bisher in der Lehrveranstaltung behandelten Themen sind für dich am meisten bzw. am ehesten für das eigene Fach anwendbar, und wieso? Bzw. falls gar nichts anwendbar ist, wieso nicht?
- Beschreibe dazu auch in einigen Sätzen dein Fach und deinen eigenen spezifischen Fokus bzw. deine Vertiefung, und welche Querverbindungen du zur feministischen Technikforschung siehst.
- Details im TUWEL-Kurs
  - ~ 3 Wochen Zeit
  - Deine Antwort sollte weder überbordend ausfallen, noch sich auf einen einfachen Stehsatz beziehen. Sie sollte so in etwa aus 250 bis 500 Wörtern bestehen. Es ist aber keine besondere wissenschaftliche Form nötig. Du kannst quasi auch frei von der Leber schreiben. Zitate oder Referenzen dürfen bei Bedarf gern trotzdem eingefügt werden (auch wenn’s damit dann über die 500 Wörter hinausgehen sollte). Je nachdem wie du die Aufgabe hier anlegst, kannst du sie am Ende auch gleich als Basis für die Abschlussreflexion verwenden.

## Referenzen

Haraway, Donna. 1985. “A Manifesto for Cyborgs: Science, Technology and Socialist Feminism in the 1980s.” Socialist Review 80.

Haraway, Donna. 1988. “Situated Knowledges: The Science Question in Feminism and the Privilege of Partial Perspectives.” Feminist Studies 14 (3): 575–99.

Haraway, Donna. 1991. Simians, Cyborgs and Women: The Reinvention of Nature. New York: Routledge.

Harding, Sandra. 1993. “Rethinking Standpoint Epistemology: What Is ‘Strong Objectivity’?” In Feminist Epistemologies, edited by Linda Alcoff and Elizabeth Potter, 49–82. Routledge.

Harding, Sandra. 1998. “Can Men Be Subjects of Feminist Thought?” In Men Doing Feminism, edited by Tom Digby, 171–96. New York: Routledge.

Lagesen, Vivian Anette. 2008. “A Cyberfeminist Utopia?: Perceptions of Gender and Computer Science among Malaysian Women Computer Science Students and Faculty.” Science Technology Human Values 33 (1): 5–27. https://doi.org/10.1177/0162243907306192.
