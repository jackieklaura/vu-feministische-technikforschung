# Session 13 - Gastvortrag Elka Xharo: Women in Science & Technology

In dieser Session gibt uns Elka Xharo (a.k.a. [The Sciency Feminist](https://www.instagram.com/thesciencyfeminist/)) einige
Einblicke in Statistiken, Hintergründe und Frauenbiografien in Natur- und Technikwissenschaften.

Weiterführende Ressourcen dazu:
* die Folien zum Talk sind im TUWEL-Kurs hinterlegt
* [The Science Feminist linktr.ee](https://linktr.ee/thesciencyfeminist) - weitere Artikel, Podcasts und andere
  Ressourcen von und/oder mit Elka Xharo
* für auditive Personen inbesondere zum heutigen Thema passend: [Cosmic Latte Podcast - Episode 12: Frauen in der Wissenschaft](https://cosmiclatte.podigee.io/12-new-episode)
* [Lost Women of Science Podcast](https://www.lostwomenofscience.org/) : Passend zu oft unsichtbaren oder vergessenen
  Frauenbiografien in Wissenschaft und Technik, stellt dieser Podcast einige sehr spannende Biografien vor und gibt
  detailreich Einblicke in Leben und Werken von großartigen Frauen, über die die meisten von uns bislang gar nichts wussten.
  Gerade für den Technik-Bereich finde ich insbesondere Seasons 2 & 3 zu Klára Dán von Neumann und Yvonne Y. Clark besonders
  aufschlussreich - erstere vor allem im Bereich Informatik, zweitere im Bereich Mechanical Engineering. Vor allem die Season
  zu YY Clark finde ich darüber hinaus auch sehr spannend in Hinblick auf feministische Technikforschung und wie
  Wissenschafts-/Technikdisziplinen als (nicht besonders innovative, jedenfalls auschließende) soziale Systeme
  funktionieren und was das auch für die inhaltliche Ausformung bedeutet.
