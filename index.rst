Willkommen zur Doku für die VU Feministische Technikforschung
=============================================================

Diese Website dient der laufenden Dokumentation für die
`VU Feministische Technikforschung <https://tiss.tuwien.ac.at/course/courseDetails.xhtml?courseNr=034016&semester=2025S>`_
im **Sommersemester 2025** an der TU Wien. Vergangene Instanzen dieser
Lehrveranstaltungen finden sich unter https://tantemalkah.at/feministische-technikforschung

Die Inhalte zu den einzelnen Sessions werden im Laufe des Semesters ergänzt.
Zugleich finden sich im Abschnitt zu den Referenzen und Ressourcen einerseits
die in der Lehrveranstaltung verwendeten Materialien, wie auch weiterführende
Literatur.

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   session1

Diese Dokumentation wurde mit `Sphinx <https://www.sphinx-doc.org>`_, dem *Python Documentation Generator* erstellt.
Sphinx ist selbst in Python geschrieben, und kann einfach per ``pip install sphinx`` installiert werden. Wir verwenden
darüber hinaus noch den `MyST Parser <https://myst-parser.readthedocs.io>`_ um Files für die einzelnen Abschnitte auch
in `Markdown <https://commonmark.org/>`_ schreiben zu können.
`linkify-it-py <https://myst-parser.readthedocs.io/en/latest/syntax/optional.html#linkify>`_ wird verwendet um einfache
URLs auch automagisch in Links umzuwandeln. Als Theme haben wir auf das verbreitete ``sphinx_rtd_theme`` zurückgegriffen,
das auf `readthedocs.io <https://readthedocs.io>`_ von vielen Open Source Softwareprojekten zur Dokumentation verwendet wird. Als Erweiterung
haben wir aber auch noch ``sphinx_rtd_dark_mode`` hinzugefügt, damit Leser*innen in der Ansicht zwischen light und dark
mode wechseln können (siehe rechts unten den kreisrunden Button mit einem Mond/Sonne Symbol). Die Theme-bezogenen Links
finden sich im Footer.

Der Source Text dieser Dokumentation, inklusive der Sphinx config, um diesen Output zu generieren, befinden sich auf
https://gitlab.com/jackieklaura/vu-feministische-technikforschung und wurde von `Andrea Ida Malkah Klaura <https://tantemalkah.at>`_
unter einer `Creative Commons Attribution-ShareAlike 4.0 International Lizenz <https://creativecommons.org/licenses/by-sa/4.0/>`_
erstellt.
