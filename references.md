# Referenzen und Ressourcen

## Bücher

Die Liste der Bücher, die an Themen dieser LV anschließen und sie weiterführen,
wie sie auch in der LV-Beschreibung im TISS zu finden ist:

* Barad, Karen Michelle. 2007. Meeting the Universe Halfway: Quantum Physics and the Entanglement of Matter and Meaning. Duke University Press.
* Criado Perez, Caroline. 2020. Invisible Women: Exposing Data Bias in a World Designed for Men. London: Vintage.
* Digby, Tom. 2013. Men Doing Feminism. Routledge.
* Dunbar-Hester, Christina. 2019. Hacking Diversity: The Politics of Inclusion in Open Technology Cultures. Princeton University Press.
* Fleck, Ludwik. 1935. Genesis and Development of a Scientific Fact. Chicago: University of Chicago Press (originally published in 1935).
    * bzw. in der Originalsprache:
      Fleck, Ludwik. 1935. Entstehung und Entwicklung einer wissenschaftlichen Tatsache: Einführung in die Lehre vom Denkstil und Denkkollektiv.
      Edited by Lothar Schäfer and Thomas Schnelle. 13. Auflage (2021, Erstausgabe 1980). Suhrkamp Taschenbuch Wissenschaft. Frankfurt am Main: Suhrkamp.
* Green, Eileen. 2001. Virtual Gender: Technology, Consumption, and Identity. Routledge.
* Haraway, Donna. 1991. Simians, Cyborgs and Women: The Reinvention of Nature. New York: Routledge.
* Haraway, Donna J. 2016. Staying with the Trouble: Making Kin in the Chthulucene. Duke University Press.
* Kleinman, Daniel Lee. 2000. Science, Technology, and Democracy. SUNY Press.
* Knoll, Bente, and Brigitte Ratzer. 2010. Gender Studies in Den Ingenieurwissenschaften. 1., Auflage. Facultas Universitätsverlag.
* Kuhn, Thomas Samuel. 1996. The Structure of Scientific Revolutions. 3rd ed. The University of Chicago Press, Chicago. 1st Edition 1962.
* Lederman, Muriel, and Ingrid Bartsch. 2001. The Gender and Science Reader. Routledge.
* MacKenzie, Donald A., and Judy Wajcman. 1985. The Social Shaping of Technology: How the Refrigerator Got Its Hum. Open University Press.
* O’Neil, Cathy. 2016. Weapons of Math Destruction: How Big Data Increases Inequality and Threatens Democracy. Crown.
* Oudshoorn, Nelly, and Trevor Pinch. 2005. How Users Matter: The Co-Construction of Users And Technology. Mit Press.
* Schmidt, Francesca. 2021. Netzpolitik: eine feministische Einführung. Verlag Barbara Budrich.
* Wajcman, Judy. 1991. Feminism Confronts Technology. Pennsylvania State University Press.
* Wajcman, Judy. 2004. TechnoFeminism. Cambridge: Polity Press.
* Washington, Harriet A. 2008. Medical Apartheid: The Dark History of Medical Experimentation on Black Americans from Colonial Times to the Present. Anchor Books.
* Weizenbaum, Joseph. 1976. Computer Power and Human Reason: From Judgment to Calculation. San Francisco: W. H. Freeman.
    * bzw. in deutscher Fassung: Weizenbaum, Joseph. 2000. Die Macht Der Computer Und Die Ohnmacht Der Vernunft. Suhrkamp.

## Podcasts

* Caroline Criado Perez: [Visible Women](https://www.tortoisemedia.com/listen/visible-women/)
* [Lost Women of Science](https://www.lostwomenofscience.org/)
